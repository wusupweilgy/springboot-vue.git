package com.wusuowei.miniouploadfile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.miniouploadfile.model.po.Files;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface FilesMapper extends BaseMapper<Files> {

}
