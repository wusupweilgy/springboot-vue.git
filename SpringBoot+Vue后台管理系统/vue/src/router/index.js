import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
// 在index.js 里面写入下面的代码即可解决
const originalReplace = VueRouter.prototype.replace;
VueRouter.prototype.replace = function replace(location) {
    return originalReplace.call(this, location).catch(err => err);
};

const routes = [
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('../views/Register.vue')
    },
    {
        path: '/front',
        name: 'Front',
        component: () => import('../views/front/Front'),
        children: [
            {
                path: 'home',
                name: 'FrontHome',
                component: () => import('../views/front/Home.vue')
            },
            {
                path: 'item1',
                name: 'Item1',
                component: () => import('../views/front/Item1.vue')
            },
            {
                path: 'person',
                name: 'FrontPerson',
                component: () => import('../views/front/Person')
            },
            {
                path: 'password',
                name: 'FrontPassword',
                component: () => import('../views/front/Password')
            },
            {
                path: 'video',
                name: 'Video',
                component: () => import('../views/front/Video')
            },
            {
                path: 'videoDetail',
                name: 'VideoDetail',
                component: () => import('../views/front/VideoDetail')
            },
            {
                path: 'article',
                name: 'FrontArticle',
                component: () => import('../views/front/Article')
            },
            {
                path: 'articleDetail',
                name: 'ArticleDetail',
                component: () => import('../views/front/ArticleDetail')
            },
        ]
    },
    {
        path: '/404',
        name: '404',
        component: () => import('../views/404.vue')
    },
]
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

// 提供一个重置路由的方法
export const resetRouter = () => {
    router.matcher = new VueRouter({
        mode: 'history',
        base: process.env.BASE_URL,
        routes
    })
}

export const  setRoutes = () => {
    const storeMenus = localStorage.getItem("menus")
    if (storeMenus) {
        // 拼装动态路由
        const manageRoute = {
            path: '/', name: 'Manage', component: () => import('../views/Manage.vue'), redirect: "/home", children: [
                {path: 'person', name: '个人信息', component: () => import('../views/Person.vue')},
                {path: 'password', name: '修改密码', component: () => import('../views/Password.vue')}
            ]
        }
        const menus = JSON.parse(storeMenus)
        menus.forEach(item => {
            if (item.path) {  // 当且仅当path不为空的时候才去设置路由
                let itemMenu = {
                    path: item.path.replace("/", ""),
                    name: item.name,
                    component: () => import('../views/' + item.pagePath + '.vue')
                }
                manageRoute.children.push(itemMenu)
            } else if (item.children.length) {
                item.children.forEach(item => {
                    if (item.path) {
                        let itemMenu = {
                            path: item.path.replace("/", ""),
                            name: item.name,
                            component: () => import('../views/' + item.pagePath + '.vue')
                        }
                        manageRoute.children.push(itemMenu)
                    }
                })
            }
        })
        // 获取当前的路由对象名称数组
        const currentRouteNames = router.getRoutes().map(v => v.name)
        if (!currentRouteNames.includes('Manage')) {
            // 动态添加到现在的路由对象中去
            router.addRoute(manageRoute)
        }
    }
}

// 刷新重置我就再set一次路由
setRoutes()

router.beforeEach( (to, from, next) => {
    //localStorage.setItem("currentPathName", to.name)  // 设置当前的路由名称
    //store.commit("setPath")
    // 未找到路由的情况
    console.log(to.path)
    if (!to.matched.length) {
        const storeMenus = localStorage.getItem("menus")
        const token = localStorage.getItem("token")
        if (storeMenus&&token) {
            next("/front")
        } else if(storeMenus&&!token){
            next("/404")
        }else{
            // 跳回登录页面
            next("/login")
        }
    }
    // 其他的情况都放行
    next()

})

export default router
