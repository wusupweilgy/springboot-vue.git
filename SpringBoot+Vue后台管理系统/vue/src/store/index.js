import Vue from 'vue'
import Vuex from 'vuex'
import router, {resetRouter, setRoutes} from "@/router";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        uploadHeaders: localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")) : '',
        avatarUrl: localStorage.getItem("userInfo") === null ? "" : JSON.parse(localStorage.getItem("userInfo" || "[]")).avatarUrl
    },
    getters: {},
    mutations: {
        changeAvatarUrl() {
            let user = JSON.parse(localStorage.getItem("userInfo"))
            console.log(this.state.avatarUrl)
            console.log(user.avatarUrl)
            this.state.avatarUrl = user.avatarUrl
        },
        logout() {
            //清空缓存
            localStorage.clear()
            router.replace("/login")

            // 重置路由
            resetRouter()
        },
        awaitSetRoutes() {
            setRoutes()
        }

    },
    actions: {},
    modules: {}
})
