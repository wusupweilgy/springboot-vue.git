package com.wusuowei.springboot;

import com.wusuowei.springboot.model.po.Menu;
import com.wusuowei.springboot.service.MenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringbootApplicationTests {

    @Autowired
    MenuService menuService;

    @Test
    void contextLoads() {
        List<Menu> userMenus = menuService.getUserMenus(1L);
        System.out.println(userMenus);
    }

}
