create table article
(
    id          int auto_increment comment 'id'
        primary key,
    name        varchar(255)         null comment '标题',
    content     varchar(255)         null comment '内容',
    user        varchar(255)         null comment '发布人',
    time        varchar(255)         null comment '发布时间',
    is_delete   tinyint(1) default 0 null comment '是否删除',
    create_time datetime             null comment '创建时间',
    update_time datetime             null comment '更新时间'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table course
(
    id          int auto_increment comment 'id'
        primary key,
    name        varchar(255)         null comment '课程名称',
    score       int                  null comment '学分',
    times       datetime             null comment '上课时间',
    state       tinyint(1)           null comment '是否开课',
    teacher_id  int                  null comment '授课老师id',
    is_delete   tinyint(1) default 0 null comment '是否删除',
    create_time datetime             null comment '创建时间',
    update_time datetime             null comment '更新时间'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table files
(
    id          int auto_increment comment 'id'
        primary key,
    name        varchar(255)         null comment '文件名称',
    type        varchar(255)         null comment '文件类型',
    size        bigint               null comment '文件大小(kb)',
    url         varchar(255)         null comment '下载链接',
    md5         varchar(255)         null comment '文件md5',
    is_delete   tinyint(1) default 0 null comment '是否删除',
    enable      tinyint(1) default 1 null comment '是否禁用链接',
    create_time datetime             null comment '创建时间',
    update_time datetime             null comment '更新时间'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table sys_dict
(
    name  varchar(255) null comment '名称',
    value varchar(255) null comment '内容',
    type  varchar(255) null comment '类型'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table sys_menu
(
    id          int auto_increment comment 'id'
        primary key,
    name        varchar(255)      null comment '名称',
    path        varchar(255)      null comment '路径',
    icon        varchar(255)      null comment '图标',
    description varchar(255)      null comment '描述',
    permission  varchar(100)      null comment '权限标识',
    pid         int               null comment '父级id',
    page_path   varchar(255)      null comment '页面路径',
    sort_num    int               null comment '排序',
    create_time datetime          null comment '创建时间',
    update_time datetime          null comment '更新时间',
    is_delete   tinyint default 0 null comment '是否删除'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table sys_role
(
    id          int auto_increment comment 'id'
        primary key,
    role_key    varchar(255)      null comment '唯一标识',
    name        varchar(32)       null comment '名称',
    description varchar(255)      null comment '描述',
    create_time datetime          null comment '创建时间',
    update_time datetime          null comment '更新时间',
    is_delete   tinyint default 0 null comment '是否删除'
)
    comment '角色表' row_format = DYNAMIC;

create table sys_role_menu
(
    role_id     varchar(32) not null comment '角色id',
    menu_id     varchar(32) not null comment '菜单id',
    create_time datetime    null comment '创建时间',
    update_time datetime    null comment '更新时间',
    primary key (role_id, menu_id)
)
    comment '角色-菜单-关联表' row_format = DYNAMIC;

create table sys_user
(
    id          int auto_increment comment 'id'
        primary key,
    username    varchar(50)                                                                    null comment '用户名',
    password    varchar(50)                                                                    null comment '密码',
    nickname    varchar(50)                                                                    null comment '昵称',
    email       varchar(50)                                                                    null comment '邮箱',
    phonenumber varchar(20)                                                                    null comment '电话',
    address     varchar(255)                                                                   null comment '地址',
    avatar_url  varchar(255) default 'http://localhost:9090/files/20230409082108000000936.jpg' null comment '头像',
    is_delete   tinyint      default 0                                                         null comment '是否删除',
    create_time datetime     default CURRENT_TIMESTAMP                                         null comment '创建时间',
    update_time datetime                                                                       null comment '更新时间'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

create table sys_user_role
(
    id          int         not null,
    user_id     varchar(32) not null comment '用户id',
    role_id     varchar(32) not null comment '角色id',
    create_time datetime    null comment '创建时间',
    update_time datetime    null comment '更新时间',
    primary key (user_id, role_id, id)
)
    comment '用户-角色关联表' row_format = DYNAMIC;

create table t_comment
(
    id          int auto_increment comment 'id'
        primary key,
    content     varchar(255)         null comment '内容',
    user_id     bigint               null comment '评论人id',
    pid         int                  null comment '父id',
    origin_id   int                  null comment '最上级评论id',
    article_id  int                  null comment '关联文章的id',
    p_nickname  varchar(255)         null comment '父节点的用户昵称',
    p_userId    bigint               null comment '父节点的用户id',
    nickname    varchar(255)         null,
    is_delete   tinyint(1) default 0 null comment '是否删除',
    create_time datetime             null comment '创建时间',
    update_time datetime             null comment '更新时间'
)
    collate = utf8mb4_unicode_ci
    row_format = DYNAMIC;

