package com.wusuowei.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wusuowei.springboot.model.dto.UserPasswordDTO;
import com.wusuowei.springboot.model.po.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lgy
 */
public interface UserMapper extends BaseMapper<User> {

    int updatePassword(UserPasswordDTO userPasswordDTO);

    Page<User> findPage(Page<User> page, String username, String email, String address);
}
