package com.wusuowei.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.springboot.model.po.Dict;

public interface DictMapper extends BaseMapper<Dict> {
}
