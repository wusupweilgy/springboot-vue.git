package com.wusuowei.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.springboot.model.po.Role;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 青哥哥
 * @since 2022-02-10
 */
public interface RoleService extends IService<Role> {

    void setRoleMenu(Integer roleId, List<Integer> menuIds);

    List<Integer> getRoleMenu(Integer roleId);
}
