package com.wusuowei.springboot.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.springboot.model.po.Course;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-08
 */
public interface CourseService extends IService<Course> {
    Page<Course> findPage(Page<Course> page, String name);

    void setStudentCourse(Integer courseId, Integer studentId);
}
