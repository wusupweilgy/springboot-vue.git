package com.wusuowei.springboot.service;

import com.wusuowei.springboot.model.po.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-11
 */
public interface ArticleService extends IService<Article> {

}
