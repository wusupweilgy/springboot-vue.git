package com.wusuowei.springboot.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lgy
 */
@Data
@TableName("sys_user")
@ApiModel(value="User", description="用户实体类")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @ApiModelProperty(value = "用户名")
    @TableField("username")
    private String username;

    @JsonIgnore
    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "用户邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "手机号码")
    @TableField("phonenumber")
    private String phonenumber;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "昵称")
    @TableField("nickname")
    private String nickname = "";

    @ApiModelProperty(value = "地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "头像")
    @TableField("avatar_url")
    private String avatarUrl;

    @ApiModelProperty("角色")
    @TableField(exist = false)
    private String role;

    @ApiModelProperty("选的课程")
    @TableField(exist = false)
    private List<Course> courses;

    @ApiModelProperty("教的课程")
    @TableField(exist = false)
    private List<Course> stuCourses;
}
