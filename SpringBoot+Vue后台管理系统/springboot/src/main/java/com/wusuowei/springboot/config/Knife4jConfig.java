package com.wusuowei.springboot.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableKnife4j
public class Knife4jConfig {

    @Value("${server.port}")
    private String port;

    @Value("${server.servlet.context-path:}")
    private String path;


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.wusuowei.springboot"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        if("/".equals(path)){
            System.out.println("http://localhost:" + port + "/doc.html");
        }else{
            System.out.println("http://localhost:" + port + path +  "/doc.html");
        }

        return new ApiInfoBuilder()
                .description("后台管理系统在线API接口文档")
                .contact(new Contact("LGY", "https://blog.csdn.net/weixin_51603038?type=blog", "2673152463@qq.com"))
                .version("v3.0.0")
                .title("后台管理系统在线API接口文档")
                .build();
    }
}