package com.wusuowei.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.springboot.model.po.Comment;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-11
 */
public interface CommentService extends IService<Comment> {
    List<Comment> findCommentDetail(Integer articleId);
    List<Comment> getTree(Integer id);
}
