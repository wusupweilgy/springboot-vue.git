package com.wusuowei.springboot.service;

import com.wusuowei.springboot.model.po.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-08
 */
public interface UserRoleService extends IService<UserRole> {

}
