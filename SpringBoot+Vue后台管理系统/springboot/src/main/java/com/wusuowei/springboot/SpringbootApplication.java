package com.wusuowei.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;


@SpringBootApplication
public class SpringbootApplication {
    private static final Logger logger = LoggerFactory.getLogger(SpringbootApplication.class);
    public static void main(String[] args) throws IOException {
        SpringApplication.run(SpringbootApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
