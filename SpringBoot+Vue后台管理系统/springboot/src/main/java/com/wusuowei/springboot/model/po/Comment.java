package com.wusuowei.springboot.model.po;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("t_comment")
@ApiModel(value="Comment", description="")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "评论人id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty(value = "评论时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "父id")
    @TableField("pid")
    private Integer pid;

    @ApiModelProperty(value = "最上级评论id")
    @TableField("origin_id")
    private Integer originId;

    @ApiModelProperty(value = "关联文章的id")
    @TableField("article_id")
    private Integer articleId;

    @TableField(exist = false)
    private String pNickname;  // 父节点的用户昵称

    @TableField(exist = false)
    private Long pUserId;  // 父节点的用户id

    @TableField(exist = false)
    private String nickname;

    @TableField(exist = false)
    private String avatarUrl;

    @TableField(exist = false)
    private List<Comment> children;

}
