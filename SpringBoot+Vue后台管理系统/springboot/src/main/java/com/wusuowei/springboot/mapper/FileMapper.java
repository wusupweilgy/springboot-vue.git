package com.wusuowei.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.springboot.model.po.Files;

public interface FileMapper extends BaseMapper<Files> {
}