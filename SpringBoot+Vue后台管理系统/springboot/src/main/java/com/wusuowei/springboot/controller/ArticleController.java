package com.wusuowei.springboot.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wusuowei.springboot.model.po.Article;
import com.wusuowei.springboot.service.ArticleService;
import com.wusuowei.springboot.utils.R;
import com.wusuowei.springboot.utils.TokenUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LGY
 */
@Slf4j
@RestController
@Api(value = "管理接口",tags = "管理接口")
@RequestMapping("article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    // 新增或者更新
    @PostMapping
    public R save(@RequestBody Article article) {
        if (article.getId() == null) { // 新增
            article.setTime(DateUtil.now());  // new Date()
            article.setUser(TokenUtils.getCurrentUser().getNickname());
        }
        articleService.saveOrUpdate(article);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    public R delete(@PathVariable Integer id) {
        articleService.removeById(id);
        return R.ok();
    }

    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        articleService.removeByIds(ids);
        return R.ok();
    }

    @GetMapping
    public R findAll(@RequestParam(required = false) String start, @RequestParam(required = false) String end) {
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();

        if (StrUtil.isNotBlank(start)) {
            // where time >= start
            queryWrapper.ge("time", start);
        }
        if (StrUtil.isNotBlank(end)) {
            // where time <= end
            queryWrapper.le("time", end);
        }
        return R.ok().setData(articleService.list(queryWrapper));
    }

    @GetMapping("/{id}")
    public R findOne(@PathVariable Integer id) {
        return R.ok().setData(articleService.getById(id));
    }

    @GetMapping("/page")
    public R findPage(@RequestParam String name,
                           @RequestParam Integer pageNum,
                           @RequestParam Integer pageSize) {
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        if (StrUtil.isNotBlank(name)) {
            queryWrapper.like("name", name);
        }
        return R.ok().setData(articleService.page(new Page<>(pageNum, pageSize), queryWrapper));
    }



}
