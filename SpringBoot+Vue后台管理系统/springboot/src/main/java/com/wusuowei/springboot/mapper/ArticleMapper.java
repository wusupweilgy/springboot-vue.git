package com.wusuowei.springboot.mapper;

import com.wusuowei.springboot.model.po.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
