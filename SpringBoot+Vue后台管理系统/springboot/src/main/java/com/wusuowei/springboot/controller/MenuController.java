package com.wusuowei.springboot.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wusuowei.springboot.constant.Constants;
import com.wusuowei.springboot.mapper.DictMapper;
import com.wusuowei.springboot.model.po.Dict;
import com.wusuowei.springboot.model.po.Menu;
import com.wusuowei.springboot.service.MenuService;
import com.wusuowei.springboot.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 青哥哥
 * @since 2022-02-10
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    private MenuService menuService;

    @Resource
    private DictMapper dictMapper;

    // 新增或者更新
    @PostMapping
    public R save(@RequestBody Menu menu) {
        menuService.saveOrUpdate(menu);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    public R delete(@PathVariable Integer id) {
        menuService.removeById(id);
        return R.ok();
    }

    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        menuService.removeByIds(ids);
        return R.ok();
    }

    @GetMapping("/ids")
    public R findAllIds() {
        return R.ok().setData(menuService.list().stream().map(Menu::getId));
    }

    @GetMapping
    public R findAll(@RequestParam(defaultValue = "") String name) {
        return R.ok().setData(menuService.findMenus(name));
    }

    @GetMapping("/{id}")
    public R findOne(@PathVariable Integer id) {
        return R.ok().setData(menuService.getById(id));
    }

    @GetMapping("/page")
    public R findPage(@RequestParam String name,
                           @RequestParam Integer pageNum,
                           @RequestParam Integer pageSize) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", name);
        queryWrapper.orderByDesc("id");
        return R.ok().setData(menuService.page(new Page<>(pageNum, pageSize), queryWrapper));
    }

    @GetMapping("/icons")
    public R getIcons() {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", Constants.DICT_TYPE_ICON);
        return R.ok().setData(dictMapper.selectList(queryWrapper));
    }

}

