package com.wusuowei.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wusuowei.springboot.model.po.Course;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface CourseMapper extends BaseMapper<Course> {

    void deleteStudentCourse(Integer courseId, Integer studentId);

    void setStudentCourse(Integer courseId, Integer studentId);

    Page<Course> findPage(Page<Course> page, String name);
}
