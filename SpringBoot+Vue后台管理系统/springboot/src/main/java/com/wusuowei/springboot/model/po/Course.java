package com.wusuowei.springboot.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("course")
@ApiModel(value="Course", description="")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "学分")
    @TableField("score")
    private Integer score;

    @ApiModelProperty(value = "上课时间")
    @TableField("times")
    private String times;

    @ApiModelProperty(value = "是否开课")
    @TableField("state")
    private Boolean state;

    @ApiModelProperty(value = "授课老师id")
    @TableField("teacher_id")
    private Integer teacherId;

    @TableField(exist = false)
    private String teacher;
}
