package com.wusuowei.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.springboot.model.po.Menu;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 青哥哥
 * @since 2022-02-10
 */
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("SELECT * FROM sys_menu WHERE id in (SELECT menu_id FROM sys_role_menu WHERE role_id in(SELECT role_id FROM sys_user_role where user_id = #{id})) ORDER BY sort_num ASC")
    List<Menu> getUserMenus(Long id);

}
