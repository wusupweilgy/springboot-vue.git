package com.wusuowei.springboot.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wusuowei.springboot.model.po.Course;
import com.wusuowei.springboot.service.CourseService;
import com.wusuowei.springboot.service.UserService;
import com.wusuowei.springboot.utils.R;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LGY
 */
@Slf4j
@RestController
@Api(value = "管理接口",tags = "管理接口")
@RequestMapping("course")
public class CourseController {

    @Resource
    private CourseService courseService;

    @Resource
    private UserService userService;

    // 新增或者更新
    @PostMapping
    public R save(@RequestBody Course course) {
        courseService.saveOrUpdate(course);
        return R.ok();
    }

    @PostMapping("/studentCourse/{courseId}/{studentId}")
    public R studentCourse(@PathVariable Integer courseId, @PathVariable Integer studentId) {
        courseService.setStudentCourse(courseId, studentId);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    public R delete(@PathVariable Integer id) {
        courseService.removeById(id);
        return R.ok();
    }

    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        courseService.removeByIds(ids);
        return R.ok();
    }

    @GetMapping
    public R findAll() {
        return R.ok().setData(courseService.list());
    }

    @GetMapping("/{id}")
    public R findOne(@PathVariable Integer id) {
        return R.ok().setData(courseService.getById(id));
    }

    @GetMapping("/page")
    public R findPage(@RequestParam String name,
                           @RequestParam Integer pageNum,
                           @RequestParam Integer pageSize) {
//        QueryWrapper<Course> queryWrapper = new QueryWrapper<>();
//        queryWrapper.orderByDesc("id");
//        Page<Course> page = courseService.page(new Page<>(pageNum, pageSize), queryWrapper);
//        List<Course> records = page.getRecords();
//        for (Course record : records) {
//            User user = userService.getById(record.getTeacherId());
//            if(user != null) {
//                record.setTeacher(user.getNickname());
//            }
//
//        }
        Page<Course> page = courseService.findPage(new Page<>(pageNum, pageSize), name);
        return R.ok().setData(page);
    }


}
