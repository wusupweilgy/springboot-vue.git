package com.wusuowei.shiro_jwt;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.wusuowei.shiro_jwt.mapper")
@SpringBootApplication
public class ShiroJwtApplication {
    private static final Logger logger = LoggerFactory.getLogger(ShiroJwtApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(ShiroJwtApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
