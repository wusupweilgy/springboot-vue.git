package com.wusuowei.shiro_jwt.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户-角色关联表
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_user_role")

public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("user_id")
    private String userId;


    @TableField("role_id")
    private String roleId;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}
