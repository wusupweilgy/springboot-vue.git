import Vue from 'vue'
import VueRouter from 'vue-router'
import FileUpload from "@/views/FileUpload";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: FileUpload
  },
]

const router = new VueRouter({
  routes
})

export default router
