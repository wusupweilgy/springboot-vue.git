export const admin = [
    {
        "icon": "el-icon-user-solid",
        "orderNum": 1,
        "updateTime": 1656917791000,
        "remark": "系统管理目录",
        "parentId": 0,
        "path": "/sys",
        "component": "",
        "children": [
            {
                "icon": "el-icon-user",
                "orderNum": 1,
                "updateTime": 1656919253000,
                "remark": "用户管理菜单",
                "parentId": 1,
                "path": "/sys/user",
                "component": "sys/user/index",
                "children": [
                    {
                        "icon": "#",
                        "orderNum": 1,
                        "updateTime": 1656919486000,
                        "remark": "用户查询按钮",
                        "parentId": 3,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "用户查询",
                        "menuType": "F",
                        "perms": "system:user:query",
                        "id": 20
                    },
                    {
                        "icon": "#",
                        "orderNum": 2,
                        "updateTime": 1656919486000,
                        "remark": "添加用户按钮",
                        "parentId": 3,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "用户新增",
                        "menuType": "F",
                        "perms": "system:user:add",
                        "id": 8
                    },
                    {
                        "icon": "#",
                        "orderNum": 3,
                        "updateTime": 1656919486000,
                        "remark": "修改用户按钮",
                        "parentId": 3,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "用户修改",
                        "menuType": "F",
                        "perms": "system:user:edit",
                        "id": 9
                    },
                    {
                        "icon": "#",
                        "orderNum": 4,
                        "updateTime": 1656919486000,
                        "remark": "删除用户按钮",
                        "parentId": 3,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "用户删除",
                        "menuType": "F",
                        "perms": "system:user:delete",
                        "id": 10
                    },
                    {
                        "icon": "#",
                        "orderNum": 5,
                        "updateTime": 1656919486000,
                        "remark": "分配角色按钮",
                        "parentId": 3,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "分配角色",
                        "menuType": "F",
                        "perms": "system:user:role",
                        "id": 11
                    },
                    {
                        "icon": "#",
                        "orderNum": 6,
                        "updateTime": 1656919486000,
                        "remark": "重置密码按钮",
                        "parentId": 3,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "重置密码",
                        "menuType": "F",
                        "perms": "system:user:resetPwd",
                        "id": 12
                    }
                ],
                "createTime": 1656919251000,
                "name": "用户管理",
                "menuType": "C",
                "perms": "system:user:list",
                "id": 3
            },
            {
                "icon": "el-icon-coin",
                "orderNum": 2,
                "updateTime": 1656919419000,
                "remark": "角色管理菜单",
                "parentId": 1,
                "path": "/sys/role",
                "component": "sys/role/index",
                "children": [
                    {
                        "icon": "#",
                        "orderNum": 1,
                        "updateTime": 1656919486000,
                        "remark": "角色查询按钮",
                        "parentId": 4,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "角色查询",
                        "menuType": "F",
                        "perms": "system:role:query",
                        "id": 21
                    },
                    {
                        "icon": "#",
                        "orderNum": 2,
                        "updateTime": 1656919486000,
                        "remark": "添加用户按钮",
                        "parentId": 4,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "角色新增",
                        "menuType": "F",
                        "perms": "system:role:add",
                        "id": 13
                    },
                    {
                        "icon": "#",
                        "orderNum": 3,
                        "updateTime": 1656919486000,
                        "remark": "修改用户按钮",
                        "parentId": 4,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "角色修改",
                        "menuType": "F",
                        "perms": "system:role:edit",
                        "id": 14
                    },
                    {
                        "icon": "#",
                        "orderNum": 4,
                        "updateTime": 1656919486000,
                        "remark": "删除用户按钮",
                        "parentId": 4,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "角色删除",
                        "menuType": "F",
                        "perms": "system:role:delete",
                        "id": 15
                    },
                    {
                        "icon": "#",
                        "orderNum": 5,
                        "updateTime": 1656919486000,
                        "remark": "分配权限按钮",
                        "parentId": 4,
                        "path": "",
                        "component": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "分配权限",
                        "menuType": "F",
                        "perms": "system:role:menu",
                        "id": 16
                    }
                ],
                "createTime": 1656919415000,
                "name": "角色管理",
                "menuType": "C",
                "perms": "system:role:list",
                "id": 4
            },
            {
                "icon": "el-icon-menu",
                "orderNum": 3,
                "updateTime": 1656919423000,
                "remark": "菜单管理菜单",
                "parentId": 1,
                "path": "/sys/menu",
                "component": "sys/menu/index",
                "children": [
                    {
                        "icon": "#",
                        "orderNum": 1,
                        "updateTime": 1656919486000,
                        "remark": "菜单查询按钮",
                        "parentId": 5,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "菜单查询",
                        "menuType": "F",
                        "perms": "system:menu:query",
                        "id": 22
                    },
                    {
                        "icon": "#",
                        "orderNum": 2,
                        "updateTime": 1656919486000,
                        "remark": "添加菜单按钮",
                        "parentId": 5,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "菜单新增",
                        "menuType": "F",
                        "perms": "system:menu:add",
                        "id": 17
                    },
                    {
                        "icon": "#",
                        "orderNum": 3,
                        "updateTime": 1656919486000,
                        "remark": "修改菜单按钮",
                        "parentId": 5,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "菜单修改",
                        "menuType": "F",
                        "perms": "system:menu:edit",
                        "id": 18
                    },
                    {
                        "icon": "#",
                        "orderNum": 4,
                        "updateTime": 1656919486000,
                        "remark": "删除菜单按钮",
                        "parentId": 5,
                        "path": "",
                        "children": [

                        ],
                        "createTime": 1656919482000,
                        "name": "菜单删除",
                        "menuType": "F",
                        "perms": "system:menu:delete",
                        "id": 19
                    }
                ],
                "createTime": 1656919421000,
                "name": "菜单管理",
                "menuType": "C",
                "perms": "system:menu:list",
                "id": 5
            }
        ],
        "createTime": 1656917789000,
        "name": "系统管理",
        "menuType": "M",
        "perms": "",
        "id": 1
    },
    {
        "icon": "el-icon-s-finance",
        "orderNum": 2,
        "updateTime": 1656917985000,
        "remark": "业务管理目录",
        "parentId": 0,
        "path": "/bsns",
        "component": "",
        "children": [
            {
                "icon": "el-icon-turn-off",
                "orderNum": 1,
                "updateTime": 1656919484000,
                "remark": "部门管理菜单",
                "parentId": 2,
                "path": "/bsns/department",
                "component": "bsns/Department",
                "children": [

                ],
                "createTime": 1656919480000,
                "name": "部门管理",
                "menuType": "C",
                "perms": "",
                "id": 6
            },
            {
                "icon": "el-icon-price-tag",
                "orderNum": 2,
                "updateTime": 1656919486000,
                "remark": "岗位管理菜单",
                "parentId": 2,
                "path": "/bsns/post",
                "component": "bsns/Post",
                "children": [

                ],
                "createTime": 1656919482000,
                "name": "岗位管理",
                "menuType": "C",
                "perms": "",
                "id": 7
            }
        ],
        "createTime": 1656917983000,
        "name": "业务管理",
        "menuType": "M",
        "perms": "",
        "id": 2
    }
]
export const user = [
    {
        "icon": "el-icon-s-finance",
        "orderNum": 2,
        "updateTime": 1656917985000,
        "remark": "业务管理目录",
        "parentId": 0,
        "path": "/bsns",
        "component": "",
        "children": [
            {
                "icon": "el-icon-turn-off",
                "orderNum": 1,
                "updateTime": 1656919484000,
                "remark": "部门管理菜单",
                "parentId": 2,
                "path": "/bsns/department",
                "component": "bsns/Department",
                "children": [

                ],
                "createTime": 1656919480000,
                "name": "部门管理",
                "menuType": "C",
                "perms": "",
                "id": 6
            },
            {
                "icon": "el-icon-price-tag",
                "orderNum": 2,
                "updateTime": 1656919486000,
                "remark": "岗位管理菜单",
                "parentId": 2,
                "path": "/bsns/post",
                "component": "bsns/Post",
                "children": [

                ],
                "createTime": 1656919482000,
                "name": "岗位管理",
                "menuType": "C",
                "perms": "",
                "id": 7
            }
        ],
        "createTime": 1656917983000,
        "name": "业务管理",
        "menuType": "M",
        "perms": "",
        "id": 2
    }
]
