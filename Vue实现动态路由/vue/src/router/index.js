import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const constantRoutes = [
    {
        path: '/',
        name: 'Home',
        component: HomeView,
        redirect: '/index',
        children: [
            {
                path: 'index',
                name: 'Index',
                component: () => import("@/views/Index")
            },
            {
                path: 'user',
                name: 'user',
                component: () => import("@/views/sys/user")
            },
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import("@/views/Login.vue")
    },
    {
        path: '/404',
        component: () =>import("@/views/404Error.vue")
    }
]

const createRouter = () => new VueRouter({
    mode: 'history',
    routes: constantRoutes
})

const router = createRouter()


// 重置路由
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher
}

export default router
