import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:'',
    menuList:[],
    isLoadRouters:false
  },
  getters: {
    GET_TOKEN:state => {
      state.token = sessionStorage.getItem("token")
      return state.token
    },
    GET_MENULIST:state => {
      state.menuList = JSON.parse(sessionStorage.getItem("menuList")||'[]')
      return state.menuList
    },
    GET_ISLOADROUTERS:state=>{
      return state.isLoadRouters
    }
  },
  mutations: {
    SET_TOKEN:(state,token)=>{
      state.token = token
      sessionStorage.setItem("token",token);
    },
    SET_MENULIST:(state,menuList)=>{
      state.menuList = menuList
      sessionStorage.setItem("menuList",JSON.stringify(menuList));
    },
    SET_ISLOADROUTERS:(state,isLoadRouters)=>{
      state.isLoadRouters = isLoadRouters
    }
  },
  actions: {
  },
  modules: {
  }
})
