import router from '../router'
import store from '@/store'

//动态添加路由
export function generateRoutes() {
    const _asyncRoutes = store.getters.GET_MENULIST
    if(_asyncRoutes==null)
        return
    _asyncRoutes.forEach(menu => {
        if (menu.children) {
            menu.children.forEach(m => {
                let route = menuToRoute(m, menu.name);
                if (route) {
                    router.addRoute("Home",route)
                }
            })
        }
    })
}

//将菜单转换成router可以识别的路由
const menuToRoute = (menu, parentName) => {
    if (!menu.component) {
        return null;
    } else {
        let route = {
            name: menu.name,
            path: menu.path,
            meta: {
                parentName: parentName
            }
        }
        route.component = () => import('@/views/' + menu.component + '.vue');
        return route;
    }
}
