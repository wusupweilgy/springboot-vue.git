package com.wusuowei.security.controller;

import com.wusuowei.security.model.po.SysUser;
import com.wusuowei.security.service.SysUserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lgy
 */
@Slf4j
@RestController
@Api(value = "用户管理接口",tags = "用户管理接口")
@RequestMapping("sysUser")
public class SysUserController {

    @Autowired
    private SysUserService  sysUserService;

    @PostMapping("/test")
    public SysUser test(@RequestBody SysUser sysUser){
        return sysUser;
    }
}
