package com.wusuowei.security.mapper;

import com.wusuowei.security.model.po.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lgy
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
