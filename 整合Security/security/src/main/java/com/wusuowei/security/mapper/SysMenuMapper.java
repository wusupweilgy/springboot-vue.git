package com.wusuowei.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.security.model.po.SysMenu;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lgy
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
