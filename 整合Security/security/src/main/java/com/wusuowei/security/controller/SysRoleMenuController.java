package com.wusuowei.security.controller;

import com.wusuowei.security.service.SysRoleMenuService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lgy
 */
@Slf4j
@RestController
@Api(value = "角色菜单管理接口",tags = "角色菜单管理接口")
@RequestMapping("sysRoleMenu")
public class SysRoleMenuController {

    @Autowired
    private SysRoleMenuService  sysRoleMenuService;
}
