package com.wusuowei.security.service;

import com.wusuowei.security.model.po.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lgy
 * @since 2023-03-14
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
