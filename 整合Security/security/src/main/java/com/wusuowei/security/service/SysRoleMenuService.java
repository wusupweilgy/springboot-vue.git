package com.wusuowei.security.service;

import com.wusuowei.security.model.po.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lgy
 * @since 2023-03-14
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
