package com.wusuowei.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @description security配置
 * @author LGY
 * @date 2023/03/14 20:09
 * @version 1.0.0
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//    @Autowired
//    LoginFailureHandler loginFailureHandler;
//
//    @Autowired
//    LoginSuccessHandler loginSuccessHandler;
//
//    @Autowired
//    private MyUserDetailsServiceImpl myUserDetailsService;
//
//    @Autowired
//    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
//
//
//    @Autowired
//    private JwtLogoutSuccessHandler jwtLogoutSuccessHandler;
//
//
//    private static final String URL_WHITELIST[] = {
//            "doc.html",
//            "/login",
//            "/logout",
//            "/captcha",
//            "/password",
//            "/image/**",
//            "/test/**"
//    };
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(myUserDetailsService);
//    }
//
//    @Bean
//    BCryptPasswordEncoder bCryptPasswordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
//        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager());
//        return jwtAuthenticationFilter;
//    }
//
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 开启跨域 以及csrf攻击 关闭
        http
                .cors()
                .and()
                .csrf()
                .disable();

//                // 登录登出配置
//                .formLogin()
//                .successHandler(loginSuccessHandler)
//                .failureHandler(loginFailureHandler)
//                .and()
//                .logout()
//                .logoutSuccessHandler(jwtLogoutSuccessHandler)
//
//                // session禁用配置
//                .and()
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)  // 无状态
//
//                // 拦截规则配置
//                .and()
//                .authorizeRequests()
//                .antMatchers(URL_WHITELIST).permitAll()  // 白名单 放行
//                .anyRequest().authenticated()
//
//
//                // 异常处理配置
//                .and()
//                .exceptionHandling()
//                .authenticationEntryPoint(jwtAuthenticationEntryPoint)
//                // 自定义过滤器配置
//
//                .and()
//                .addFilter(jwtAuthenticationFilter());
    }
}
