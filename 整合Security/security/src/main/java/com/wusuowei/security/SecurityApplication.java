package com.wusuowei.security;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.wusuowei.security.mapper")
@SpringBootApplication
public class SecurityApplication {
    private static final Logger logger = LoggerFactory.getLogger(SecurityApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
