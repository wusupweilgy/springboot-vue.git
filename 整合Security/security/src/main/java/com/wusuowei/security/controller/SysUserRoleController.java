package com.wusuowei.security.controller;

import com.wusuowei.security.service.SysUserRoleService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lgy
 */
@Slf4j
@RestController
@Api(value = "用户角色关系管理接口",tags = "用户角色关系管理接口")
@RequestMapping("sysUserRole")
public class SysUserRoleController {

    @Autowired
    private SysUserRoleService  sysUserRoleService;
}
