package com.wusuowei.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.security.mapper.SysMenuMapper;
import com.wusuowei.security.model.po.SysMenu;
import com.wusuowei.security.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lgy
 */
@Slf4j
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
