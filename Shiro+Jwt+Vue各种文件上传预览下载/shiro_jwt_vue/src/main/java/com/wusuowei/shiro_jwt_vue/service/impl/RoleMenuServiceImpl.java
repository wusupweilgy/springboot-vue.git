package com.wusuowei.shiro_jwt_vue.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.shiro_jwt_vue.mapper.RoleMenuMapper;
import com.wusuowei.shiro_jwt_vue.model.po.RoleMenu;
import com.wusuowei.shiro_jwt_vue.service.RoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-菜单-关联表 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
