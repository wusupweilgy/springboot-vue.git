package com.wusuowei.shiro_jwt_vue.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.shiro_jwt_vue.model.po.Dict;

public interface DictMapper extends BaseMapper<Dict> {
}
