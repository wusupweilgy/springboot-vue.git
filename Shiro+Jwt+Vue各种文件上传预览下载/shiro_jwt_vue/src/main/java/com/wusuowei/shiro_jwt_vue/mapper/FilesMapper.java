package com.wusuowei.shiro_jwt_vue.mapper;

import com.wusuowei.shiro_jwt_vue.model.po.Files;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface FilesMapper extends BaseMapper<Files> {

}
