package com.wusuowei.shiro_jwt_vue.model.vo;

import lombok.Data;

@Data
public class UserVo {

    private String username;
    private String password;
}
