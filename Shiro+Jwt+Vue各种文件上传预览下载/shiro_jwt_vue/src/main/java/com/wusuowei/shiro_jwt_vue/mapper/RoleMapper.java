package com.wusuowei.shiro_jwt_vue.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.shiro_jwt_vue.model.po.Role;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface RoleMapper extends BaseMapper<Role> {

}
