package com.wusuowei.shiro_jwt_vue.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wusuowei.shiro_jwt_vue.mapper.DictMapper;
import com.wusuowei.shiro_jwt_vue.model.po.Dict;
import com.wusuowei.shiro_jwt_vue.model.po.Menu;
import com.wusuowei.shiro_jwt_vue.service.MenuService;
import com.wusuowei.shiro_jwt_vue.service.UserService;
import com.wusuowei.shiro_jwt_vue.utils.R;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lgy
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private UserService userService;

    // 新增或者更新
    @RequiresRoles(value = {"admin","vip"},logical = Logical.OR)
    @PostMapping
    public R save(@RequestBody Menu menu) {
        menuService.saveOrUpdate(menu);
        return R.ok();
    }

    @RequiresRoles("admin")
    @DeleteMapping("/{id}")
    public R delete(@PathVariable Integer id) {

        menuService.removeById(id);
        return R.ok();
    }

    @RequiresRoles("admin")
    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        menuService.removeByIds(ids);
        return R.ok();
    }

    @RequiresAuthentication
    @GetMapping("/ids")
    public R findAllIds() {
        return R.ok().setData(menuService.list().stream().map(Menu::getId).collect(Collectors.toList()));
    }

    @RequiresAuthentication
    @GetMapping
    public R findAll(@RequestParam(defaultValue = "") String name) {
        return R.ok().setData(menuService.findMenus(name));
    }

    @RequiresAuthentication
    @GetMapping("/{id}")
    public R findOne(@PathVariable Integer id) {
        return R.ok().setData(menuService.getById(id));
    }

    @RequiresAuthentication
    @GetMapping("/icons")
    public R getIcons() {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", "icon");
        return R.ok().setData(dictMapper.selectList(queryWrapper));
    }

}

