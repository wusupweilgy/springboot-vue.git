package com.wusuowei.shiro_jwt_vue.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.shiro_jwt_vue.model.po.RoleMenu;

/**
 * <p>
 * 角色-菜单-关联表 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-17
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
