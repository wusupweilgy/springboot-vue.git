package com.wusuowei.shiro_jwt_vue.model.vo;

import lombok.Data;

@Data
public class UserPasswordVo {
    private Integer id;
    private String username;
    private String password;
    private String newPassword;
}
