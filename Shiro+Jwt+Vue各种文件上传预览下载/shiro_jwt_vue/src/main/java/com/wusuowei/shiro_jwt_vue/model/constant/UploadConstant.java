package com.wusuowei.shiro_jwt_vue.model.constant;

public class UploadConstant {

    public static final String URLLIST = "urlList";
    public static final String UPLOADID = "uploadId";
}
