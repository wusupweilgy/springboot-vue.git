package com.wusuowei.shiro_jwt_vue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.shiro_jwt_vue.mapper.FilesMapper;
import com.wusuowei.shiro_jwt_vue.model.po.Files;
import com.wusuowei.shiro_jwt_vue.service.FilesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class FilesServiceImpl extends ServiceImpl<FilesMapper, Files> implements FilesService {

}
