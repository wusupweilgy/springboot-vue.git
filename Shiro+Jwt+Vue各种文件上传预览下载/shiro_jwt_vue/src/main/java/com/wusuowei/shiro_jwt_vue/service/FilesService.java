package com.wusuowei.shiro_jwt_vue.service;

import com.wusuowei.shiro_jwt_vue.model.po.Files;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-27
 */
public interface FilesService extends IService<Files> {

}
