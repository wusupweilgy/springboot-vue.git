package com.wusuowei.shiro_jwt_vue.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.shiro_jwt_vue.model.po.UserRole;

/**
 * <p>
 * 用户-角色关联表 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
