import request from "@/util/request";
import qs from "qs";

//获取token
export const getToken = usernamejson => {
    localStorage.clear()
    return request({
        method: "post",
        url: "/auth/oauth/token?" + qs.stringify(usernamejson),
    })
}

export const getUser = () => {
    return request({
        method: "get",
        //url: "/auth/user/getUser"
        url: "/product/list"
    })
}
/*获取图片验证码*/
export const getCheckCodeSubmit = () => {
    return request({
        method:"post",
        //url:"http://localhost:4000/checkcode/pic"
        url:"/checkcode/pic"
    });
}

/*获取邮箱验证码*/
export const getEmailCodeSubmit = (emailReceiver) => {
    return request.get("/checkcode/email/sendEmail/"+emailReceiver)
}
