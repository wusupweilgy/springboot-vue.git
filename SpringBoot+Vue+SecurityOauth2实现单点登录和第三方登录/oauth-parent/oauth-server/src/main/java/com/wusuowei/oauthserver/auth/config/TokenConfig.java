package com.wusuowei.oauthserver.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;


/**
 * @description jwt令牌存储配置
 * @author LGY
 * @date 2023/03/31 00:24
 * @version 1.0.0
 */
@Configuration
public class TokenConfig {
    @Bean
    public TokenStore jwtTokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();

//        //将解析后的token转换成我们自定义的对象，存入Authentication中
//        DefaultAccessTokenConverter defaultAccessTokenConverter = new DefaultAccessTokenConverter();
//        defaultAccessTokenConverter.setUserTokenConverter(new UserAuthenticationConverter());
//        // 赋予新的Token转换器。
//        accessTokenConverter.setAccessTokenConverter(defaultAccessTokenConverter);
        //配置JWT使用的秘钥
        accessTokenConverter.setSigningKey("test_key");

        return accessTokenConverter;
    }

    //jwt增强器
//    @Bean
//    public JwtTokenEnhancer jwtTokenEnhancer(){
//        return new JwtTokenEnhancer();
//    }
}
