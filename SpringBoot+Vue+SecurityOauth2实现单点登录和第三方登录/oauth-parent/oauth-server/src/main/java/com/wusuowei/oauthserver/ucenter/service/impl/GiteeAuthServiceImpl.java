package com.wusuowei.oauthserver.ucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wusuowei.oauthserver.ucenter.mapper.UserMapper;
import com.wusuowei.oauthserver.ucenter.model.dto.UserExt;
import com.wusuowei.oauthserver.ucenter.model.po.User;
import com.wusuowei.oauthserver.ucenter.model.vo.AuthParamsVo;
import com.wusuowei.oauthserver.ucenter.service.AuthService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description gitee认证
 * @author LGY
 * @date 2023/04/11 18:57
 * @version 1.0.0
 */
@Service("gitee_authservice")
public class GiteeAuthServiceImpl implements AuthService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    GiteeAuthServiceImpl currentProxy;

    //微信认证方法
    @Override
    public UserExt execute(AuthParamsVo authParamsVo) {
        //获取账号
        String unionId = authParamsVo.getUnionId();
        User user = userMapper.selectOne(new LambdaQueryWrapper<User>().eq(User::getUnionid,unionId));
        if(user==null){
            throw new RuntimeException("用户不存在");
        }
        UserExt userExt = new UserExt();
        BeanUtils.copyProperties(user, userExt);

        return userExt;
    }
}
