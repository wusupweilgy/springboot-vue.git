package com.wusuowei.oauthserver.ucenter.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.oauthserver.ucenter.model.po.Menu;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("SELECT * FROM sys_menu WHERE id in (SELECT menu_id FROM sys_role_menu WHERE role_id in ( SELECT role_id FROM sys_user_role WHERE user_id = #{id})) and perms!=''")
    List<Menu> selectPermissionByUserId(Long id);
}
