package com.wusuowei.oauthserver.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.oauthserver.ucenter.model.po.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author lgy
 */
public interface UserMapper extends BaseMapper<User> {

}
