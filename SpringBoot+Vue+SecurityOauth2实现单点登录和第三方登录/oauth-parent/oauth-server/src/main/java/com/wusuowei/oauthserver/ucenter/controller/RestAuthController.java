package com.wusuowei.oauthserver.ucenter.controller;


import com.wusuowei.oauthserver.ucenter.mapper.UserMapper;
import com.wusuowei.oauthserver.ucenter.model.po.User;
import com.wusuowei.oauthserver.ucenter.service.impl.ThreeUserServiceImpl;
import com.wusuowei.oauthserver.ucenter.service.impl.UserServiceImpl;
import com.xkcoding.justauth.AuthRequestFactory;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @description 第三方授权登录
 * @author LGY
 * @date 2023/04/11 18:57
 * @version 1.0.0
 */
@Controller
@RequestMapping("/oauth")
public class RestAuthController {

    @Autowired
    UserMapper userMapper;

    @Autowired
    AuthRequestFactory factory;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    ThreeUserServiceImpl threeUserService;

    @GetMapping
    public List<String> list() {
        return factory.oauthList();
    }

    @GetMapping("/login/{type}")
    public void login(@PathVariable String type, HttpServletResponse response) throws IOException {
        AuthRequest authRequest = factory.get(type);
        response.sendRedirect(authRequest.authorize(AuthStateUtils.createState()));
    }

    @GetMapping("/{type}/callback")
    public String login(@PathVariable String type, AuthCallback callback) {
        AuthRequest authRequest = factory.get(type);
        AuthResponse<AuthUser> response = authRequest.login(callback);
        User user = threeUserService.addUser(response.getData());
        if (user == null) {
            //重定向到一个错误页面
            return "redirect:http://localhost:8080/login/?error="+"登录失败";
        } else {
            String unionId = user.getUnionid();
            //重定向到登录页面，自动登录
            return "redirect:http://localhost:8080/login?unionId=" + unionId + "&authType="+type;
        }
    }

}