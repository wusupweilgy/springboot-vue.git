package com.wusuowei.oauthserver.ucenter.model.dto;

import com.wusuowei.oauthserver.ucenter.model.po.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 用户扩展类
 * @author LGY
 * @date 2023/03/31 17:58
 * @version 1.0.0
 */
@Data
public class UserExt extends User {
    //用户权限
    List<String> permissions = new ArrayList<>();
}
