package com.wusuowei.oauthserver.auth.config;

import com.wusuowei.oauthserver.auth.exception.MyWebResponseExceptionTranslator;
import com.wusuowei.oauthserver.ucenter.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;


/**
 * @description 授权服务器配置
 * @author LGY
 * @date 2023/03/31 00:23
 * @version 1.0.0
 */
@Configuration
@EnableAuthorizationServer//开启授权服务器
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    // @Qualifier("jwtTokenStore")
    private TokenStore tokenStore;

    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

//    @Autowired
//    private JwtTokenEnhancer jwtTokenEnhancer;


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

        //设置jwt增强内容
//        TokenEnhancerChain chain = new TokenEnhancerChain();
//        List<TokenEnhancer> delegates = new ArrayList<>();
//        delegates.add(jwtTokenEnhancer);
//        delegates.add(jwtAccessTokenConverter);
//        chain.setTokenEnhancers(delegates);

        //默认除了密码模式都实现了，这里配置密码模式
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userService)
                //accessToken转成JWTtoken
                .tokenStore(tokenStore)
                .accessTokenConverter(jwtAccessTokenConverter)
                .exceptionTranslator(new MyWebResponseExceptionTranslator());
        //.tokenEnhancer(chain)//扩展jwt存储的内容

        ;
    }

    /**
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("client")// appid
                .secret(passwordEncoder.encode("112233"))// appsecret
                //.redirectUris("http://www.baidu.com") // 回调地址
                .redirectUris("http://localhost:8080/login") //回调地址 重定向去你请求前被拦截的地址
                .scopes("all")// 作用域
                .accessTokenValiditySeconds(60)//设置accessToken失效时间为60秒
                 .refreshTokenValiditySeconds(6000)//刷新令牌的失效时间
                .autoApprove(true)//自动授权
                // 资源的id
                .resourceIds("ssodemo")
                // 授权类型：授权码
                .authorizedGrantTypes("authorization_code", "password", "refresh_token")
        ;
    }

    //令牌端点的安全配置
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security
                .tokenKeyAccess("permitAll()")                    //oauth/token_key是公开  开启/oauth/token_key验证端口无权限访问
                .checkTokenAccess("isAuthenticated()")                  //oauth/check_token  开启/oauth/check_token验证端口认证权限访问
                .allowFormAuthenticationForClients();            //表单认证（申请令牌） 请求/oauth/token的，如果不配置这个/oauth/token访问就会显示没有认证

    }

}
