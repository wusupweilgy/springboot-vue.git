package com.wusuowei.oauthserver.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.oauthserver.ucenter.model.po.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface RoleMapper extends BaseMapper<Role> {

}
