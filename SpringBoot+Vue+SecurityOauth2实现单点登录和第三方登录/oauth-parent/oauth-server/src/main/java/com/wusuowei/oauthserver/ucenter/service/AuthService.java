package com.wusuowei.oauthserver.ucenter.service;

import com.wusuowei.oauthserver.ucenter.model.dto.UserExt;
import com.wusuowei.oauthserver.ucenter.model.vo.AuthParamsVo;


/**
 * @description 身份认证接口
 * @author LGY
 * @date 2023/03/31 17:20
 * @version 1.0.0
 */
public interface AuthService {


  /**
   * @description 认证方法
   * @param authParamsVo 身份验证参数
   * @return {@link UserExt }
   * @author LGY
   * @date 2023/03/31 17:20
   */
  UserExt execute(AuthParamsVo authParamsVo);


}
