package com.wusuowei.oauthserver.ucenter.feignclient;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @description 检查代码客户端工厂
 * @author LGY
 * @date 2023/04/04 16:16
 * @version 1.0.0
 */
@Slf4j
@Component
public class CheckCodeClientFactory implements FallbackFactory<CheckCodeClient> {
    @Override
    public CheckCodeClient create(Throwable throwable) {
        return new CheckCodeClient() {
            @Override
            public Boolean verify(String key, String code) {
                log.error("调用验证码服务熔断异常:{}", throwable.getMessage());
                return null;
            }
        };
    }
}
