package com.wusuowei.oauthserver.ucenter.service;

import com.wusuowei.oauthserver.ucenter.model.po.User;
import me.zhyd.oauth.model.AuthUser;


/**
 * @description 身份认证接口
 * @author LGY
 * @date 2023/03/31 17:20
 * @version 1.0.0
 */
public interface ThreeUserService {


  /**
   * @description 添加第三方应用的用户
   * @param authUser 身份验证用户
   * @return {@link User }
   * @author LGY
   * @date 2023/04/02 23:01
   */
  User addUser(AuthUser authUser);


}
