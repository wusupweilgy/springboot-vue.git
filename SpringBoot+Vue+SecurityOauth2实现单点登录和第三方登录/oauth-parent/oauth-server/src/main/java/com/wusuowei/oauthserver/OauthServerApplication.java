package com.wusuowei.oauthserver;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@EnableFeignClients("com.wusuowei.oauthserver.ucenter.feignclient")
@MapperScan("com.wusuowei.oauthserver.ucenter.mapper")
@SpringBootApplication
public class OauthServerApplication {
    private static final Logger logger = LoggerFactory.getLogger(OauthServerApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(OauthServerApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

    @Bean
    RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate(new OkHttp3ClientHttpRequestFactory());
        return  restTemplate;
    }
}
