package com.wusuowei.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;


/**
 * @description 安全配置
 * @author LGY
 * @date 2023/04/10 21:45
 * @version 1.0.0
 */
@EnableWebFluxSecurity
@Configuration
public class SecurityConfig {


    //安全拦截配置
    @Bean
    public SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) {

        return http.authorizeExchange()
                .pathMatchers("/**").permitAll()
                .anyExchange().authenticated()
                .and().csrf().disable()
                //.addFilterBefore(corsWebFilter(), SecurityWebFiltersOrder.CORS)
                .build();
    }
}
