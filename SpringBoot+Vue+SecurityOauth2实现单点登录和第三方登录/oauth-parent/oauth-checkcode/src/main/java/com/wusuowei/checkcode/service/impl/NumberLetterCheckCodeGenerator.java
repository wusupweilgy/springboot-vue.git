package com.wusuowei.checkcode.service.impl;

import com.wusuowei.checkcode.service.CheckCodeService;
import org.springframework.stereotype.Component;

import java.util.Random;


/**
 * @description 数字字母生成器
 * @author LGY
 * @date 2023/04/04 14:28
 * @version 1.0.0
 */
@Component("NumberLetterCheckCodeGenerator")
public class NumberLetterCheckCodeGenerator implements CheckCodeService.CheckCodeGenerator {


    @Override
    public String generate(int length) {
        String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(36);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


}
