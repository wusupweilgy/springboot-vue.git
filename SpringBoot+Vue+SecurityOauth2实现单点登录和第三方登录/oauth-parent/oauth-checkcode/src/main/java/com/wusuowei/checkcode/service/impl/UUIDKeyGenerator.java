package com.wusuowei.checkcode.service.impl;

import com.wusuowei.checkcode.service.CheckCodeService;
import org.springframework.stereotype.Component;

import java.util.UUID;


/**
 * @description uuid生成器，生成key
 * @author LGY
 * @date 2023/04/04 14:28
 * @version 1.0.0
 */
@Component("UUIDKeyGenerator")
public class UUIDKeyGenerator implements CheckCodeService.KeyGenerator {
    @Override
    public String generate(String prefix) {
        String uuid = UUID.randomUUID().toString();
        return prefix + uuid.replaceAll("-", "");
    }
}
