package com.wusuowei.checkcode;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class CheckCodeTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void setStringRedisTemplate() {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set("李光耀", "李光耀");
    }

    @Test
    void getStringRedisTemplate() {
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        System.out.println(ops.get("name"));
        // 输出为：redis
        System.out.println(ops.get("code_name"));
        // 输出为：code_redis
    }

    @Test
    void setRedisTemplate() {
        HashOperations ops = redisTemplate.opsForHash();
        ops.put("code_info", "code_name", "code_info_redis");

    }

    @Test
    void getRedisTemplate() {
        HashOperations ops = redisTemplate.opsForHash();
        System.out.println(ops.get("info", "name"));
        // 输出为：null
        System.out.println(ops.get("code_info", "code_name"));
        // 输出为：code_info_redis
    }
}
