package com.wusuowei.oauthclient.controller;

import com.wusuowei.oauthclient.utils.R;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class ProductController {

    @GetMapping("/list")
    //@PreAuthorize("hasAuthority('system:user:list')")
    public R list(Authentication authentication) {
        HashMap<String, String> map = new HashMap<>();
        map.put("nickname","无所谓^_^");
        map.put("address","浙江");
        map.put("text","在笑大学牲");
        map.put("qq","2673152463");
        map.put("url","https://blog.csdn.net/weixin_51603038?spm=1018.2226.3001.5343");
        return R.ok().setData(map);
    }
}