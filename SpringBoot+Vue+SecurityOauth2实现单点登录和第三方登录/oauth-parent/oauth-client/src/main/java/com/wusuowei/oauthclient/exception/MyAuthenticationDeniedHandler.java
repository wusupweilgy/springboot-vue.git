package com.wusuowei.oauthclient.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 认证过的用户访问无权限时的异常
 * @author LGY
 * @date 2023/04/06 23:28
 * @version 1.0.0
 */
@Component
public class MyAuthenticationDeniedHandler implements AccessDeniedHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException authException) throws IOException, ServletException {
        try {
            // 1. 检查看服务端是否已将数据输出到客户端，如果已返回，则不处理
            if (response.isCommitted()) {
                return;
            }
            // 2. 封装结果信息，应该使用统一结果集封装，这里使用Map
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=utf-8");
            Map<String, Object> result = new HashMap<>();
            result.put("code", 403);
            result.put("msg", authException.getMessage());
            result.put("result", "没有权限");
            objectMapper.writeValue(response.getOutputStream(), result);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
