package com.wusuowei.oauthclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableOAuth2Sso//开启单点登录
@SpringBootApplication
public class OauthClientApplication {
    private static final Logger logger = LoggerFactory.getLogger(OauthClientApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(OauthClientApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
