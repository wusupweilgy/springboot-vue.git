import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'
import Manage from '../views/Manage.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: Manage,
        children:[
        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import("@/views/Login.vue"),
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import("@/views/Register.vue"),
    },
    {
        path: '*',
        name: '404',
        component: () => import('@/views/404.vue')
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
