/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : wusuowei

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 10/03/2024 23:21:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (9, 'sys_dict_data', '字典数据表', NULL, NULL, 'SysDictData', 'crud', 'com.wusuowei.business', 'business', 'data', '字典数据', 'wusuowei', '0', '/', NULL, 'kdyzm', '2023-05-24 16:46:21', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (40, '9', 'dict_code', '字典编码', 'bigint', 'Long', 'dictCode', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (41, '9', 'dict_sort', '字典排序', 'int', 'Long', 'dictSort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (42, '9', 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (43, '9', 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (44, '9', 'dict_type', '字典类型', 'varchar(100)', 'String', 'dictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (45, '9', 'css_class', '样式属性（其他样式扩展）', 'varchar(100)', 'String', 'cssClass', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (46, '9', 'list_class', '表格回显样式', 'varchar(100)', 'String', 'listClass', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (47, '9', 'is_default', '是否默认（Y是 N否）', 'char(1)', 'String', 'isDefault', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (48, '9', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 9, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (49, '9', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (50, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (51, '9', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'kdyzm', '2023-05-24 16:46:21', '', NULL);
INSERT INTO `gen_table_column` VALUES (52, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 13, 'kdyzm', '2023-05-24 16:46:22', '', NULL);
INSERT INTO `gen_table_column` VALUES (53, '9', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 14, 'kdyzm', '2023-05-24 16:46:22', '', NULL);

-- ----------------------------
-- Table structure for sys_dht11
-- ----------------------------
DROP TABLE IF EXISTS `sys_dht11`;
CREATE TABLE `sys_dht11`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `temp` double NULL DEFAULT NULL,
  `humi` double NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 702 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dht11
-- ----------------------------
INSERT INTO `sys_dht11` VALUES (641, 24, 44, '2023-06-21 15:55:40', NULL);
INSERT INTO `sys_dht11` VALUES (642, 24, 45, '2023-06-21 15:56:00', NULL);
INSERT INTO `sys_dht11` VALUES (644, 24, 46, '2023-06-21 15:56:40', NULL);
INSERT INTO `sys_dht11` VALUES (645, 24, 45, '2023-06-21 15:57:00', NULL);
INSERT INTO `sys_dht11` VALUES (646, 24, 59, '2023-06-21 15:57:20', NULL);
INSERT INTO `sys_dht11` VALUES (647, 24, 73, '2023-06-21 15:57:40', NULL);
INSERT INTO `sys_dht11` VALUES (648, 24, 63, '2023-06-21 15:58:00', NULL);
INSERT INTO `sys_dht11` VALUES (649, 23, 54, '2023-06-21 15:58:20', NULL);
INSERT INTO `sys_dht11` VALUES (650, 23, 51, '2023-06-21 15:58:40', NULL);
INSERT INTO `sys_dht11` VALUES (651, 23, 49, '2023-06-21 15:59:00', NULL);
INSERT INTO `sys_dht11` VALUES (652, 23, 49, '2023-06-21 15:59:20', NULL);
INSERT INTO `sys_dht11` VALUES (653, 23, 49, '2023-06-21 15:59:40', NULL);
INSERT INTO `sys_dht11` VALUES (654, 23, 49, '2023-06-21 16:00:00', NULL);
INSERT INTO `sys_dht11` VALUES (655, 0, 0, '2023-06-21 16:00:20', NULL);
INSERT INTO `sys_dht11` VALUES (656, 23, 49, '2023-06-21 16:00:40', NULL);
INSERT INTO `sys_dht11` VALUES (657, 23, 49, '2023-06-21 16:01:00', NULL);
INSERT INTO `sys_dht11` VALUES (658, 23, 49, '2023-06-21 16:01:20', NULL);
INSERT INTO `sys_dht11` VALUES (659, 23, 48, '2023-06-21 16:01:40', NULL);
INSERT INTO `sys_dht11` VALUES (660, 23, 48, '2023-06-21 16:02:10', NULL);
INSERT INTO `sys_dht11` VALUES (661, 23, 49, '2023-06-21 16:03:10', NULL);
INSERT INTO `sys_dht11` VALUES (662, 23, 48, '2023-06-21 16:04:06', NULL);
INSERT INTO `sys_dht11` VALUES (663, 23, 48, '2023-06-21 16:04:20', NULL);
INSERT INTO `sys_dht11` VALUES (664, 23, 48, '2023-06-21 16:04:40', NULL);
INSERT INTO `sys_dht11` VALUES (665, 23, 48, '2023-06-21 16:05:00', NULL);
INSERT INTO `sys_dht11` VALUES (666, 0, 0, '2023-06-21 16:05:20', NULL);
INSERT INTO `sys_dht11` VALUES (667, 23, 48, '2023-06-21 16:05:40', NULL);
INSERT INTO `sys_dht11` VALUES (668, 23, 47, '2023-06-21 16:06:00', NULL);
INSERT INTO `sys_dht11` VALUES (669, 23, 47, '2023-06-21 16:06:20', NULL);
INSERT INTO `sys_dht11` VALUES (670, 23, 47, '2023-06-21 16:07:10', NULL);
INSERT INTO `sys_dht11` VALUES (671, 23, 47, '2023-06-21 16:08:10', NULL);
INSERT INTO `sys_dht11` VALUES (672, 23, 47, '2023-06-21 16:09:10', NULL);
INSERT INTO `sys_dht11` VALUES (673, 0, 0, '2023-06-21 16:10:10', NULL);
INSERT INTO `sys_dht11` VALUES (674, 0, 0, '2023-06-21 16:11:10', NULL);
INSERT INTO `sys_dht11` VALUES (675, 0, 0, '2023-06-21 16:11:55', NULL);
INSERT INTO `sys_dht11` VALUES (676, 23, 48, '2023-06-21 16:12:00', NULL);
INSERT INTO `sys_dht11` VALUES (677, 23, 71, '2023-06-21 16:12:20', NULL);
INSERT INTO `sys_dht11` VALUES (678, 0, 0, '2023-06-21 16:12:40', NULL);
INSERT INTO `sys_dht11` VALUES (679, 0, 0, '2023-06-21 16:13:00', NULL);
INSERT INTO `sys_dht11` VALUES (680, 24, 81, '2023-06-21 16:13:20', NULL);
INSERT INTO `sys_dht11` VALUES (681, 23, 75, '2023-06-21 16:13:40', NULL);
INSERT INTO `sys_dht11` VALUES (682, 23, 64, '2023-06-21 16:14:00', NULL);
INSERT INTO `sys_dht11` VALUES (683, 23, 58, '2023-06-21 16:14:20', NULL);
INSERT INTO `sys_dht11` VALUES (684, 23, 54, '2023-06-21 16:14:40', NULL);
INSERT INTO `sys_dht11` VALUES (685, 23, 51, '2023-06-21 16:15:00', NULL);
INSERT INTO `sys_dht11` VALUES (686, 23, 50, '2023-06-21 16:15:20', NULL);
INSERT INTO `sys_dht11` VALUES (687, 23, 49, '2023-06-21 16:15:40', NULL);
INSERT INTO `sys_dht11` VALUES (688, 24, 49, '2023-06-21 16:16:00', NULL);
INSERT INTO `sys_dht11` VALUES (689, 23, 49, '2023-06-21 16:16:20', NULL);
INSERT INTO `sys_dht11` VALUES (690, 24, 48, '2023-06-21 16:16:40', NULL);
INSERT INTO `sys_dht11` VALUES (691, 24, 48, '2023-06-21 16:17:00', NULL);
INSERT INTO `sys_dht11` VALUES (692, 24, 53, '2023-06-21 16:17:20', NULL);
INSERT INTO `sys_dht11` VALUES (693, 26, 81, '2023-06-21 16:17:40', NULL);
INSERT INTO `sys_dht11` VALUES (694, 26, 81, '2023-06-21 16:18:00', NULL);
INSERT INTO `sys_dht11` VALUES (695, 28, 80, '2023-06-21 16:18:20', NULL);
INSERT INTO `sys_dht11` VALUES (696, 31, 81, '2023-06-21 16:18:40', NULL);
INSERT INTO `sys_dht11` VALUES (697, 32, 83, '2023-06-21 16:19:00', NULL);
INSERT INTO `sys_dht11` VALUES (698, 33, 82, '2023-06-21 16:19:20', NULL);
INSERT INTO `sys_dht11` VALUES (699, 33, 50, '2023-06-21 16:19:40', NULL);
INSERT INTO `sys_dht11` VALUES (700, 32, 41, '2023-06-21 16:20:00', NULL);
INSERT INTO `sys_dht11` VALUES (701, 31, 38, '2023-06-21 16:20:20', NULL);
INSERT INTO `sys_dht11` VALUES (702, 29, 37, '2024-03-10 15:37:34', NULL);
INSERT INTO `sys_dht11` VALUES (703, 29, 37, '2024-03-10 15:37:37', NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '名称',
  `value` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '内容',
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT 'icon' COMMENT '类型',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`name`, `value`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('chat-dot-round', 'el-icon-chat-dot-round', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('coffee', 'el-icon-coffee\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('cpu', 'el-icon-cpu', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('daima', 'iconfont icon-daima', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('delete', 'el-icon-delete\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('document', 'el-icon-document', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('edit-outline', 'el-icon-edit-outline', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('files', 'el-icon-files', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('folder-opened', 'el-icon-folder-opened', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('help', 'el-icon-help', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('house', 'el-icon-house', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('info', 'el-icon-info\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('jiaosequnti', 'iconfont icon-jiaosequnti', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('location-outline', 'el-icon-location-outline', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('map-location', 'el-icon-map-location', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('menu', 'el-icon-menu', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('message', 'el-icon-message', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('message-solid', 'el-icon-message-solid', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('more', 'el-icon-more', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('phone', 'el-icon-phone\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('phone-outline', 'el-icon-phone-outline', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('plane\r\n', 'iconfont icon-7', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-claim', 'el-icon-s-claim', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-cooperation', 'el-icon-s-cooperation', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-custom', 'el-icon-s-custom', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-data', 'el-icon-s-data', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-goods', 'el-icon-s-goods', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-grid', 'el-icon-s-grid', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-help', 'el-icon-s-help', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-home', 'el-icon-s-home', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-management', 'el-icon-s-management', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-marketing', 'el-icon-s-marketing', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-operation', 'el-icon-s-operation', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-opportunity', 'el-icon-s-opportunity\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-order', 'el-icon-s-order\r\n ', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('s-tools', 'el-icon-s-tools', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('setting', 'el-icon-setting', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('share', 'el-icon-share\r\n', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('shopping-cart-2', 'el-icon-shopping-cart-2', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('star-off', 'el-icon-star-off', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('suitcase-1', 'el-icon-suitcase-1', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('upload', 'el-icon-upload', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('user', 'el-icon-user', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('user-solid', 'el-icon-user-solid', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('warning', 'el-icon-warning', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('wenshidu', 'iconfont icon-wenshidu', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('xitongjiankong', 'iconfont icon-xitongjiankong', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('zaixianyonhu', 'iconfont icon-zaixianyonhu', 'icon', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('zidianguanli', 'iconfont icon-zidianguanli', 'icon', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(0) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', 'danger', 'Y', '0', 'admin', '2023-05-14 13:04:06', 'admin', '2023-05-14 23:05:55', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', 'success', 'N', '0', 'admin', '2023-05-14 13:04:06', 'admin', '2023-05-15 22:47:55', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'success', 'N', '0', 'admin', '2023-05-14 13:04:06', 'admin', '2023-05-17 10:32:16', '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-05-14 13:04:06', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (101, 0, '图片', 'image', 'sys_file_type', 'custom-pink', 'primary', 'N', '0', 'admin', '2023-05-17 10:08:51', 'admin', '2023-07-12 12:59:52', NULL);
INSERT INTO `sys_dict_data` VALUES (102, 1, '视频', 'video', 'sys_file_type', NULL, 'success', 'N', '0', 'admin', '2023-05-17 10:09:04', 'admin', '2023-05-17 10:29:18', NULL);
INSERT INTO `sys_dict_data` VALUES (103, 3, 'Excel', 'excel', 'sys_file_type', '', 'info', 'N', '0', 'admin', '2023-05-17 10:09:26', 'admin', '2023-05-17 10:29:58', NULL);
INSERT INTO `sys_dict_data` VALUES (104, 4, 'Word', 'word', 'sys_file_type', NULL, 'info', 'N', '0', 'admin', '2023-05-17 10:09:39', 'admin', '2023-05-17 10:30:01', NULL);
INSERT INTO `sys_dict_data` VALUES (105, 5, 'PDF', 'pdf', 'sys_file_type', NULL, 'info', 'N', '0', 'admin', '2023-05-17 10:09:48', 'admin', '2023-05-17 10:30:05', NULL);
INSERT INTO `sys_dict_data` VALUES (106, 2, '音频', 'radio', 'sys_file_type', NULL, 'warning', 'N', '0', 'admin', '2023-05-17 10:10:10', 'admin', '2023-05-17 10:30:32', NULL);
INSERT INTO `sys_dict_data` VALUES (107, 7, '文本', 'txt', 'sys_file_type', NULL, 'default', 'N', '0', 'admin', '2023-05-17 10:12:44', 'admin', '2023-05-17 10:27:02', NULL);
INSERT INTO `sys_dict_data` VALUES (108, 6, 'PPT', 'ppt', 'sys_file_type', NULL, 'info', 'N', '0', 'admin', '2023-05-17 10:13:00', 'admin', '2023-05-17 10:30:09', NULL);
INSERT INTO `sys_dict_data` VALUES (109, 8, '其他', 'other', 'sys_file_type', NULL, 'danger', 'N', '0', 'admin', '2023-05-17 10:13:52', 'admin', '2023-05-17 10:30:19', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-05-14 13:03:56', 'admin', '2024-03-10 15:44:08', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-05-14 13:03:56', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '文件类型', 'sys_file_type', '0', 'admin', '2023-05-16 18:52:53', '', NULL, '文件类型列表');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `upload_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分片上传uploadId',
  `file_md5` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '文件md5',
  `url` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '下载链接',
  `file_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `bucket_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '桶名',
  `file_type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `file_size` bigint(0) NULL DEFAULT NULL COMMENT '文件大小(byte)',
  `chunk_size` bigint(0) NULL DEFAULT NULL COMMENT '每个分片的大小（byte）',
  `chunk_num` int(0) NULL DEFAULT NULL COMMENT '分片数量',
  `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除',
  `enable` tinyint(1) NULL DEFAULT 1 COMMENT '是否禁用链接(0 禁用 1启用)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 305 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3954 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES (3538, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 18:28:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3539, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 20:50:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3540, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 21:27:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3541, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 21:53:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3542, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 22:28:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3543, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-15 22:57:03', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3544, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 14:43:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3545, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 15:30:32', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3546, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 15:46:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3547, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 15:58:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3548, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 16:36:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3549, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 16:56:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3550, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:00:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3551, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:06:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3552, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:39:13', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3553, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:46:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3554, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:53:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3555, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-16 17:55:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3556, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-16 17:55:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3557, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-16 17:55:22', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3558, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 17:55:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3559, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-16 18:47:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3560, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-16 18:49:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3561, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:08:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3562, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:44:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3563, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:46:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3564, 'LGY', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-17 10:46:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3565, 'LGY', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-17 10:47:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3566, 'LGY', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:47:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3567, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:54:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3568, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:55:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3569, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:55:13', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3570, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:55:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3571, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:56:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3572, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:56:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3573, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 10:57:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3574, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-17 11:00:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3575, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:00:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3576, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:02:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3577, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:06:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3578, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:07:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3579, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:08:22', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3580, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:08:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3581, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:09:03', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3582, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:13:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3583, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:15:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3584, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:15:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3585, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:16:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3586, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:16:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3587, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:17:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3588, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:22:11', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3589, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:25:22', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3590, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:25:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3591, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 11:26:31', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3592, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:34:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3593, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:35:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3594, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:35:38', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3595, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:36:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3596, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:37:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3597, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:38:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3598, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:44:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3599, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:45:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3600, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:55:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3601, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:57:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3602, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 12:58:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3603, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:04:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3604, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:05:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3605, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:14:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3606, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:15:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3607, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:15:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3608, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:16:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3609, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:41:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3610, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:54:11', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3611, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:55:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3612, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 13:55:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3613, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 14:03:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3614, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 14:06:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3615, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 14:37:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3616, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 14:38:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3617, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 16:41:31', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3618, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:08:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3619, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:10:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3620, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:11:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3621, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:12:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3622, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:13:42', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3623, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:19:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3624, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:46:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3625, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:48:12', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3626, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 17:58:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3627, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:01:58', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3628, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:02:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3629, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:06:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3630, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:06:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3631, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:07:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3632, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:07:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3633, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-17 18:08:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3634, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:08:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3635, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:09:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3636, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:09:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3637, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:09:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3638, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'password cannot be null', '2023-05-17 18:10:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3639, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:12:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3640, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:32:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3641, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:39:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3642, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:46:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3643, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:50:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3644, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:55:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3645, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-17 18:57:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3646, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 07:55:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3647, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:27:52', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3648, '无所谓ㅍ_ㅍ', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:28:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3649, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:30:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3650, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:46:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3651, '是强子啊', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:49:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3652, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 08:50:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3653, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 09:04:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3654, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 09:27:01', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3655, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 09:52:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3656, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:08:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3657, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:18:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3658, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:22:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3659, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:28:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3660, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:30:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3661, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:31:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3662, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:32:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3663, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:33:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3664, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:38:31', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3665, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:38:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3666, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-18 10:43:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3667, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-18 10:44:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3668, 'wusupweilgy', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:54:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3669, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 10:56:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3670, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 11:18:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3671, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 12:53:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3672, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 13:14:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3673, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 13:19:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3674, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 13:34:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3675, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-18 13:50:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3676, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 12:03:14', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3677, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 12:19:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3678, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:06:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3679, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:19:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3680, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:20:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3681, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:21:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3682, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:34:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3683, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:35:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3684, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:35:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3685, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:36:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3686, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:38:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3687, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:42:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3688, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:42:58', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3689, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:45:38', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3690, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:47:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3691, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:48:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3692, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:49:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3693, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:49:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3694, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:52:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3695, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:52:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3696, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:53:38', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3697, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:53:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3698, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:55:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3699, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:55:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3700, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 13:56:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3701, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:02:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3702, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:03:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3703, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:04:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3704, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:06:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3705, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:08:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3706, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:09:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3707, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:09:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3708, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:10:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3709, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:10:11', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3710, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 14:10:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3711, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:02:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3712, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:37:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3713, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:38:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3714, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:40:12', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3715, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:40:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3716, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 15:54:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3717, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-19 16:13:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3718, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-21 22:18:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3719, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 09:52:28', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3720, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 10:51:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3721, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 11:46:32', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3722, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 11:50:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3723, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 11:57:14', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3724, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 11:59:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3725, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 12:03:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3726, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:30:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3727, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:32:42', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3728, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:32:52', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3729, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:35:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3730, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:46:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3731, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 14:58:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3732, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 15:12:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3733, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 15:52:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3734, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:01:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3735, '无所谓ㅍ_ㅍ', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:02:14', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3736, '无所谓ㅍ_ㅍ', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:03:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3737, 'wusupweilgy', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:12:57', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3738, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:13:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3739, 'wusupweilgy', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:14:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3740, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:16:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3741, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:45:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3742, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:45:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3743, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:55:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3744, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:57:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3745, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:58:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3746, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:58:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3747, 'admin', '127.0.0.1,127.0.0.1', '未知', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 16:59:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3748, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:00:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3749, 'admin', '127.0.0.1,127.0.0.1', '未知', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:03:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3750, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:03:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3751, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-22 17:29:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3752, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 17:29:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3753, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 17:29:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3754, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 17:29:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3755, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 17:30:28', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3756, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:38:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3757, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:48:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3758, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 17:54:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3759, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 17:54:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3760, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:00:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3761, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:01:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3762, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 18:08:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3763, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:08:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3764, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:12:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3765, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:16:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3766, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:18:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3767, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:19:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3768, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:25:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3769, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-22 18:26:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3770, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:26:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3771, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-22 18:30:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3772, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:30:28', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3773, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-22 18:30:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3774, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:31:01', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3775, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-22 18:31:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3776, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:32:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3777, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 18:35:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3778, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-22 18:35:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3779, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:35:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3780, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 18:36:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3781, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:36:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3782, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:38:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3783, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 18:39:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3784, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:39:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3785, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:39:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3786, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:40:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3787, 'admin', '127.0.0.1,127.0.0.1', '未知', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:41:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3788, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:46:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3789, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', 'One record is expected, but the query result is multiple records', '2023-05-22 18:49:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3790, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', 'One record is expected, but the query result is multiple records', '2023-05-22 18:51:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3791, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', 'One record is expected, but the query result is multiple records', '2023-05-22 18:53:18', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3792, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 18:55:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3793, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 19:03:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3794, '', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-22 19:09:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3795, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 19:20:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3796, 'wusupweilgy', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 19:21:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3797, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 19:53:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3798, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 20:11:03', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3799, 'wusupweilgy', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 20:17:51', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3800, '', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-22 20:25:13', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3801, '', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-22 20:27:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3802, '', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-22 20:29:38', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3803, 'wusupweilgy', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 20:37:52', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3804, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 21:32:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3805, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 21:53:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3806, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-22 21:53:57', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3807, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 22:10:57', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3808, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 22:40:14', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3809, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 22:57:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3810, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 23:16:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3811, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 23:18:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3812, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 23:22:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3813, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-22 23:40:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3814, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-23 17:38:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3815, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 17:39:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3816, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码为空', '2023-05-23 19:15:05', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3817, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 19:15:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3818, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 19:16:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3819, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 19:21:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3820, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 20:54:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3821, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 20:57:01', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3822, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:10:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3823, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 21:10:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3824, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:10:32', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3825, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:11:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3826, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:12:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3827, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 21:19:42', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3828, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:19:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3829, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:20:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3830, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:52:23', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3831, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号不存在', '2023-05-23 21:52:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3832, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号不存在', '2023-05-23 21:52:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3833, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-23 21:53:04', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3834, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 21:53:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3835, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-23 21:53:15', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3836, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:53:48', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3837, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:54:13', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3838, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 21:54:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3839, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 22:26:58', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3840, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 22:44:42', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3841, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号不存在', '2023-05-23 23:07:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3842, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:07:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3843, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:08:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3844, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:09:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3845, 'jjj', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:09:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3846, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:10:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3847, 'wusuowei', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '账号或密码错误', '2023-05-23 23:10:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3848, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:11:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3849, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:11:31', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3850, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:11:45', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3851, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:12:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3852, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 23:12:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3853, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:12:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3854, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:13:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3855, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:14:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3856, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:15:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3857, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:15:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3858, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:16:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3859, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:16:59', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3860, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:24:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3861, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:26:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3862, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 23:26:59', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3863, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:28:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3864, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:34:14', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3865, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:34:28', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3866, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-23 23:34:46', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3867, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:35:11', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3868, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:38:57', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3869, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:39:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3870, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:40:31', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3871, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-23 23:40:47', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3872, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 00:02:49', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3873, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 00:02:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3874, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 00:03:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3875, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 00:05:28', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3876, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 00:05:34', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3877, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:22:53', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3878, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:23:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3879, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 08:26:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3880, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:26:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3881, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:30:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3882, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:33:33', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3883, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:36:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3884, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:38:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3885, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 08:40:38', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3886, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:14:01', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3887, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:14:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3888, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:14:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3889, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:15:20', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3890, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:16:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3891, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:16:40', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3892, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:20:10', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3893, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:22:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3894, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:26:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3895, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:41:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3896, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:43:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3897, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 09:44:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3898, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 10:00:03', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3899, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 10:05:06', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3900, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 15:50:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3901, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:21:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3902, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 16:31:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3903, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:31:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3904, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:40:07', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3905, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:42:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3906, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:43:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3907, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:44:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3908, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:44:36', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3909, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:45:12', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3910, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 16:49:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3911, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 16:49:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3912, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 16:50:00', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3913, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 16:50:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3914, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', 'One record is expected, but the query result is multiple records', '2023-05-24 17:04:57', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3915, '2553229176@qq.com', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:09:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3916, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:10:12', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3917, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:10:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3918, '2553229176@qq.com', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:12:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3919, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 17:12:21', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3920, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 17:12:44', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3921, '2553229176@qq.com', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:13:54', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3922, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:45:59', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3923, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:46:11', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3924, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 17:46:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3925, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 21:46:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3926, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 23:15:50', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3927, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-24 23:19:08', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3928, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 23:19:13', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3929, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-24 23:22:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3930, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-25 14:35:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3931, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-25 14:37:09', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3932, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-25 14:37:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3933, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', 'org.bouncycastle.jcajce.provider.util.BadBlockException: unable to decrypt block', '2023-05-26 16:03:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3934, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:05:19', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3935, 'vip', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:08:32', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3936, '无所谓ㅍ_ㅍ', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:10:56', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3937, '无所谓ㅍ_ㅍ', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:12:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3938, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '1', '验证码错误', '2023-05-26 16:13:29', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3939, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:13:35', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3940, 'vip', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:40:24', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3941, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:40:43', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3942, 'admin', '127.0.0.1,127.0.0.1', ' 本机地址', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-05-26 16:48:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3943, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-18 16:29:02', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3944, 'vip', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-18 16:30:26', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3945, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-18 16:30:39', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3946, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-18 16:30:55', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3947, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-18 16:31:41', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3948, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-06-20 19:56:16', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3949, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-07-12 11:14:25', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3950, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-07-17 17:13:30', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3951, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-07-17 17:14:17', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3952, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-07-17 17:14:27', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3953, 'admin', '127.0.0.1', '内网', 'CHROME11', 'Windows 11', '0', '登录成功', '2023-07-17 17:16:37', NULL, NULL);
INSERT INTO `sys_login_log` VALUES (3954, 'admin', '192.168.131.137', '内网', 'CHROME12', 'Windows 11', '0', '登录成功', '2024-03-10 15:15:09', NULL, NULL);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '路径',
  `icon` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图标',
  `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '描述',
  `permission` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `pid` int(0) NULL DEFAULT NULL COMMENT '父级id',
  `page_path` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '页面路径',
  `sort_num` int(0) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除',
  `menu_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '菜单类型（M目录 C菜单 F按钮）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 106 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (3, '首页', '/home', 'el-icon-house', '首页', 'home', NULL, '/Home', 1, NULL, '2023-04-20 09:45:07', 0, 'C');
INSERT INTO `sys_menu` VALUES (4, '系统管理', '/system', 'el-icon-s-tools', NULL, 'system', NULL, '/system', 2, NULL, '2023-05-05 12:56:57', 0, 'M');
INSERT INTO `sys_menu` VALUES (5, '用户管理', '/user', 'el-icon-user', NULL, 'system:user:list', 4, '/user/index', 1, NULL, '2023-04-20 10:17:23', 0, 'C');
INSERT INTO `sys_menu` VALUES (6, '角色管理', '/role', 'el-icon-s-custom', '角色管理', 'system:role:list', 4, '/role/index', 2, NULL, '2023-04-20 10:53:11', 0, 'C');
INSERT INTO `sys_menu` VALUES (7, '菜单管理', '/menu', 'el-icon-menu', '菜单管理', 'system:menu:list', 4, '/menu/index', 3, NULL, '2023-05-14 12:57:20', 0, 'C');
INSERT INTO `sys_menu` VALUES (47, '文件管理', '/allfile', 'el-icon-files', '文件管理', 'system:file:list', 4, '/file/index', 5, '2023-04-21 17:02:22', '2023-04-21 17:05:54', 0, 'C');
INSERT INTO `sys_menu` VALUES (57, '日志管理', '/log', 'el-icon-edit-outline', '日志管理', 'log', 4, '/log', 6, '2023-05-06 10:07:05', '2023-05-06 10:56:23', 0, 'M');
INSERT INTO `sys_menu` VALUES (58, '登录日志', '/loginlog', 'el-icon-s-claim', '登录日志', 'system:loginlog:list', 57, '/loginlog/index', 2, '2023-05-06 10:08:38', '2023-05-06 10:59:41', 0, 'C');
INSERT INTO `sys_menu` VALUES (59, '操作日志', '/operlog', 'el-icon-warning', '操作日志', 'system:operlog:list', 57, '/operlog/index', 1, '2023-05-06 10:16:48', '2023-05-06 10:57:11', 0, 'C');
INSERT INTO `sys_menu` VALUES (61, '系统工具', '/tool', 'el-icon-s-cooperation', '系统工具', 'tool', NULL, '/tool', 4, '2023-05-11 14:10:28', '2023-05-16 17:58:11', 0, 'M');
INSERT INTO `sys_menu` VALUES (62, '代码生成', '/gen', 'iconfont icon-daima', '代码生成', 'tool:gen:list', 61, '/Gen', 1, '2023-05-11 14:33:05', '2023-05-16 17:59:41', 0, 'C');
INSERT INTO `sys_menu` VALUES (63, '系统监控', '/monitor', 'iconfont icon-xitongjiankong', '系统监控', 'monitor', NULL, '/monitor', 3, '2023-05-13 12:18:25', '2023-05-14 23:02:41', 0, 'M');
INSERT INTO `sys_menu` VALUES (64, '在线用户', '/online', 'iconfont icon-zaixianyonhu', '在线用户', 'monitor:online:list', 63, '/Online', 1, '2023-05-13 12:21:02', '2023-05-13 12:21:55', 0, 'C');
INSERT INTO `sys_menu` VALUES (65, '字典管理', '/dict', 'iconfont icon-zidianguanli', '字典管理', 'system:dict:list', 4, '/dict/index', 4, '2023-05-13 15:57:59', '2023-05-13 15:58:22', 0, 'C');
INSERT INTO `sys_menu` VALUES (68, '用户查询', NULL, NULL, NULL, 'system:user:query', 5, NULL, 1, '2023-05-17 17:03:56', '2023-05-17 17:29:51', 0, 'F');
INSERT INTO `sys_menu` VALUES (69, '用户新增', NULL, NULL, NULL, 'system:user:add', 5, NULL, 2, '2023-05-17 17:29:39', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (70, '用户修改', NULL, NULL, NULL, 'system:user:edit', 5, NULL, 3, '2023-05-17 17:31:03', '2023-05-17 17:31:13', 0, 'F');
INSERT INTO `sys_menu` VALUES (71, '用户删除', NULL, NULL, NULL, 'system:user:remove', 5, NULL, 4, '2023-05-17 17:31:38', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (72, '角色查询', NULL, NULL, NULL, 'system:role:query', 6, NULL, 1, '2023-05-17 17:34:22', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (73, '角色新增', NULL, NULL, NULL, 'system:role:add', 6, NULL, 2, '2023-05-17 17:39:22', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (74, '角色修改', '', NULL, NULL, 'system:role:edit', 6, NULL, 3, '2023-05-17 17:39:45', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (75, '角色删除', NULL, NULL, NULL, 'system:role:remove', 6, NULL, 4, '2023-05-17 17:41:28', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (76, '菜单查询', NULL, NULL, NULL, 'system:menu:query', 7, NULL, 1, '2023-05-17 17:43:30', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (77, '菜单新增', NULL, NULL, NULL, 'system:menu:add', 7, NULL, 2, '2023-05-17 17:44:03', '2023-05-17 17:44:08', 0, 'F');
INSERT INTO `sys_menu` VALUES (78, '菜单修改', NULL, NULL, NULL, 'system:menu:edit', 7, NULL, 3, '2023-05-17 17:44:24', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (79, '菜单删除', NULL, NULL, NULL, 'system:menu:remove', 7, NULL, 4, '2023-05-17 17:44:37', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (80, '字典查询', NULL, NULL, NULL, 'system:dict:query', 65, NULL, 1, '2023-05-17 18:34:31', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (81, '字典新增', NULL, NULL, NULL, 'system:dict:add', 65, NULL, 2, '2023-05-17 18:35:03', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (82, '字典修改', NULL, NULL, NULL, 'system:dict:edit', 65, NULL, 3, '2023-05-17 18:35:20', '2023-05-17 18:35:43', 0, 'F');
INSERT INTO `sys_menu` VALUES (83, '字典删除', NULL, NULL, NULL, 'system:dict:remove', 65, NULL, 4, '2023-05-17 18:35:38', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (84, '文件查询', NULL, NULL, NULL, 'system:file:query', 47, NULL, 1, '2023-05-17 18:40:14', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (85, '文件新增', NULL, NULL, NULL, 'system:file:add', 47, NULL, 2, '2023-05-17 18:41:42', '2023-05-17 18:41:45', 0, 'F');
INSERT INTO `sys_menu` VALUES (86, '文件修改', NULL, NULL, NULL, 'system:file:edit', 47, NULL, 3, '2023-05-17 18:42:11', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (87, '文件删除', NULL, NULL, NULL, 'system:file:remove', 47, NULL, 4, '2023-05-17 18:42:26', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (88, '清空日志', NULL, NULL, NULL, 'system:operlog:remove', 59, NULL, 2, '2023-05-17 18:52:35', '2023-05-17 18:52:47', 0, 'F');
INSERT INTO `sys_menu` VALUES (89, '清空日志', NULL, NULL, NULL, 'system:loginlog:remove', 58, NULL, 2, '2023-05-17 18:53:23', '2023-05-23 23:05:37', 0, 'F');
INSERT INTO `sys_menu` VALUES (90, '强退用户', NULL, NULL, NULL, 'monitor:online:quit', 64, NULL, 1, '2023-05-17 18:56:54', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (94, '个人博客', '/mycsdn', 'iconfont icon-7', NULL, 'mycsdn', NULL, '/MyCSDN', 5, '2023-05-22 16:54:28', '2023-05-22 16:54:33', 0, 'C');
INSERT INTO `sys_menu` VALUES (95, '查询日志', NULL, NULL, NULL, 'system:operlog:query', 59, NULL, 1, '2023-05-23 23:05:05', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (96, '查询日志', NULL, NULL, NULL, 'system:loginlog:query', 58, NULL, 1, '2023-05-23 23:05:31', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (97, '查询用户', NULL, NULL, NULL, 'monitor:online:query', 64, NULL, 1, '2023-05-23 23:05:56', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (98, '数据库查询', NULL, NULL, NULL, 'tool:gen:query', 62, NULL, 2, '2023-05-24 16:35:53', '2023-05-24 16:36:33', 0, 'F');
INSERT INTO `sys_menu` VALUES (99, '导入表结构', NULL, NULL, NULL, 'tool:gen:import', 62, NULL, 3, '2023-05-24 16:37:33', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (100, '预览代码', NULL, NULL, NULL, 'tool:gen:preview', 62, NULL, 4, '2023-05-24 16:38:01', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (101, '删除代码', NULL, NULL, NULL, 'tool:gen:remove', 62, NULL, 5, '2023-05-24 16:38:23', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (102, '生成代码', NULL, NULL, NULL, 'tool:gen:code', 62, NULL, 6, '2023-05-24 16:38:44', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (103, '下载代码', NULL, NULL, NULL, 'tool:gen:code', 62, NULL, 1, '2023-05-24 16:39:02', NULL, 0, 'F');
INSERT INTO `sys_menu` VALUES (104, '修改个人信息', NULL, NULL, NULL, 'system:user:ordinary', 5, NULL, 5, '2023-05-24 23:17:12', '2023-05-24 23:17:27', 0, 'F');
INSERT INTO `sys_menu` VALUES (105, '温湿度监控', '/dht11', 'iconfont icon-wenshidu', NULL, 'monitor:dht11:list', 63, '/DHT11', 2, '2023-06-18 16:29:40', '2023-06-18 16:31:31', 0, 'C');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(0) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(0) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(0) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(0) NULL DEFAULT 0 COMMENT '消耗时间（毫秒）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 533 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (300, '操作日志', 9, 'com.wusuowei.system.controller.SysOperLogController.deleteAll()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/log/operLog/del/all', '127.0.0.1', '内网', '{}', '{\"msg\":\"success\",\"code\":200,\"data\":2}', 0, NULL, '2023-05-17 10:34:07', 5);
INSERT INTO `sys_oper_log` VALUES (301, '字典类型', 1, 'com.wusuowei.system.controller.SysDictTypeController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/type', '127.0.0.1', '内网', '{\"dict\":{\"dictName\":\"登录状态\",\"dictType\":\"sys_login_status\",\"status\":\"0\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":1}', 0, NULL, '2023-05-17 10:36:31', 10);
INSERT INTO `sys_oper_log` VALUES (302, '字典数据', 1, 'com.wusuowei.system.controller.SysDictDataController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/data', '127.0.0.1', '内网', '{\"dict\":{\"dictLabel\":\"成功\",\"dictSort\":0,\"dictType\":\"sys_login_status\",\"dictValue\":\"0\",\"listClass\":\"success\",\"status\":\"0\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":1}', 0, NULL, '2023-05-17 10:36:49', 9);
INSERT INTO `sys_oper_log` VALUES (303, '字典数据', 1, 'com.wusuowei.system.controller.SysDictDataController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/data', '127.0.0.1', '内网', '{\"dict\":{\"dictLabel\":\"失败\",\"dictSort\":1,\"dictType\":\"sys_login_status\",\"dictValue\":\"1\",\"listClass\":\"danger\",\"status\":\"0\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":1}', 0, NULL, '2023-05-17 10:37:02', 10);
INSERT INTO `sys_oper_log` VALUES (304, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/monitor/online/1', '127.0.0.1', '内网', '{\"uid\":\"1\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 10:44:28', 14);
INSERT INTO `sys_oper_log` VALUES (305, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/monitor/online/1', '127.0.0.1', '内网', '{\"uid\":\"1\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 10:46:10', 2);
INSERT INTO `sys_oper_log` VALUES (306, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/monitor/online/43', '127.0.0.1', '内网', '{\"uid\":\"43\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 10:47:17', 3);
INSERT INTO `sys_oper_log` VALUES (307, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-user\",\"id\":5,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"用户管理\",\"pagePath\":\"/user/index\",\"path\":\"/user\",\"permission\":\"system:user:list\",\"pid\":4,\"sortNum\":301,\"updateTime\":1681957043000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:00:52', 15);
INSERT INTO `sys_oper_log` VALUES (308, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"用户查询\",\"permission\":\"system:user:query\",\"pid\":5,\"sortNum\":100}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:03:56', 17);
INSERT INTO `sys_oper_log` VALUES (309, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-s-tools\",\"id\":4,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"系统管理\",\"pagePath\":\"/system\",\"path\":\"/system\",\"permission\":\"system\",\"sortNum\":300,\"updateTime\":1683262617000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:08:27', 15);
INSERT INTO `sys_oper_log` VALUES (310, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-s-tools\",\"id\":4,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"系统管理\",\"pagePath\":\"/system\",\"path\":\"/system\",\"permission\":\"system\",\"sortNum\":2,\"updateTime\":1683262617000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:16:55', 7);
INSERT INTO `sys_oper_log` VALUES (311, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683951505000,\"description\":\"系统监控\",\"icon\":\"iconfont icon-xitongjiankong\",\"id\":63,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"系统监控\",\"pagePath\":\"/monitor\",\"path\":\"/monitor\",\"permission\":\"monitor\",\"sortNum\":3,\"updateTime\":1684076561000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:17:19', 6);
INSERT INTO `sys_oper_log` VALUES (312, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683785428000,\"description\":\"系统工具\",\"icon\":\"el-icon-s-cooperation\",\"id\":61,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"系统工具\",\"pagePath\":\"/tool\",\"path\":\"/tool\",\"permission\":\"tool\",\"sortNum\":4,\"updateTime\":1684231091000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:17:29', 7);
INSERT INTO `sys_oper_log` VALUES (313, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-user\",\"id\":5,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"用户管理\",\"pagePath\":\"/user/index\",\"path\":\"/user\",\"permission\":\"system:user:list\",\"pid\":4,\"sortNum\":1,\"updateTime\":1681957043000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:05', 7);
INSERT INTO `sys_oper_log` VALUES (314, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"description\":\"角色管理\",\"icon\":\"el-icon-s-custom\",\"id\":6,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"角色管理\",\"pagePath\":\"/role/index\",\"path\":\"/role\",\"permission\":\"role\",\"pid\":4,\"sortNum\":2,\"updateTime\":1681959191000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:13', 6);
INSERT INTO `sys_oper_log` VALUES (315, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"description\":\"菜单管理\",\"icon\":\"el-icon-menu\",\"id\":7,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"菜单管理\",\"pagePath\":\"/menu/index\",\"path\":\"/menu\",\"permission\":\"menu\",\"pid\":4,\"sortNum\":3,\"updateTime\":1684040240000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:17', 5);
INSERT INTO `sys_oper_log` VALUES (316, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683964679000,\"description\":\"字典管理\",\"icon\":\"iconfont icon-zidianguanli\",\"id\":65,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"字典管理\",\"pagePath\":\"/dict/index\",\"path\":\"/dict\",\"permission\":\"dict\",\"pid\":4,\"sortNum\":4,\"updateTime\":1683964702000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:20', 5);
INSERT INTO `sys_oper_log` VALUES (317, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1682067742000,\"description\":\"文件管理\",\"icon\":\"el-icon-files\",\"id\":47,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"文件管理\",\"pagePath\":\"/file/index\",\"path\":\"/allfile\",\"permission\":\"file\",\"pid\":4,\"sortNum\":5,\"updateTime\":1682067954000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:23', 6);
INSERT INTO `sys_oper_log` VALUES (318, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683338825000,\"description\":\"日志管理\",\"icon\":\"el-icon-edit-outline\",\"id\":57,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"日志管理\",\"pagePath\":\"/log\",\"path\":\"/log\",\"permission\":\"log\",\"pid\":4,\"sortNum\":6,\"updateTime\":1683341783000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:19:41', 5);
INSERT INTO `sys_oper_log` VALUES (319, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683786785000,\"description\":\"代码生成\",\"icon\":\"iconfont icon-daima\",\"id\":62,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"代码生成\",\"pagePath\":\"/Gen\",\"path\":\"/gen\",\"permission\":\"gen\",\"pid\":61,\"sortNum\":1,\"updateTime\":1684231181000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:23:00', 8);
INSERT INTO `sys_oper_log` VALUES (320, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"用户新增\",\"permission\":\"system:user:add\",\"pid\":5,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:29:39', 7);
INSERT INTO `sys_oper_log` VALUES (321, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684314236000,\"id\":68,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"用户查询\",\"permission\":\"system:user:query\",\"pid\":5,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:29:51', 6);
INSERT INTO `sys_oper_log` VALUES (322, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"用户修改\",\"permission\":\"system:user:edit\",\"pid\":5,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:31:03', 8);
INSERT INTO `sys_oper_log` VALUES (323, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684315863000,\"id\":70,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"用户修改\",\"permission\":\"system:user:edit\",\"pid\":5,\"sortNum\":3}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:31:13', 5);
INSERT INTO `sys_oper_log` VALUES (324, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"用户删除\",\"permission\":\"system:user:remove\",\"pid\":5,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:31:38', 6);
INSERT INTO `sys_oper_log` VALUES (325, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"角色查询\",\"permission\":\"system:role:query\",\"pid\":6,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:34:22', 7);
INSERT INTO `sys_oper_log` VALUES (326, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"description\":\"角色管理\",\"icon\":\"el-icon-s-custom\",\"id\":6,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"角色管理\",\"pagePath\":\"/role/index\",\"path\":\"/role\",\"permission\":\"system:role:list\",\"pid\":4,\"sortNum\":2,\"updateTime\":1681959191000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:34:37', 6);
INSERT INTO `sys_oper_log` VALUES (327, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"角色新增\",\"permission\":\"system:role:add\",\"pid\":6,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:39:22', 5);
INSERT INTO `sys_oper_log` VALUES (328, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"角色修改\",\"path\":\"system:role:edit\",\"permission\":\"system:role:edit\",\"pid\":6,\"sortNum\":3}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:39:45', 5);
INSERT INTO `sys_oper_log` VALUES (329, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"角色删除\",\"permission\":\"system:role:remove\",\"pid\":6,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:41:28', 6);
INSERT INTO `sys_oper_log` VALUES (330, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"菜单查询\",\"permission\":\"system:menu:query\",\"pid\":7,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:43:30', 10);
INSERT INTO `sys_oper_log` VALUES (331, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"description\":\"菜单管理\",\"icon\":\"el-icon-menu\",\"id\":7,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"菜单管理\",\"pagePath\":\"/menu/index\",\"path\":\"/menu\",\"permission\":\"system:menu:list\",\"pid\":4,\"sortNum\":3,\"updateTime\":1684040240000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:43:40', 6);
INSERT INTO `sys_oper_log` VALUES (332, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"菜单新增\",\"permission\":\"system:menu:add\",\"pid\":7,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:44:03', 7);
INSERT INTO `sys_oper_log` VALUES (333, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684316643000,\"id\":77,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"菜单新增\",\"permission\":\"system:menu:add\",\"pid\":7,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:44:08', 5);
INSERT INTO `sys_oper_log` VALUES (334, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"菜单修改\",\"permission\":\"system:menu:edit\",\"pid\":7,\"sortNum\":3}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:44:24', 5);
INSERT INTO `sys_oper_log` VALUES (335, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"菜单删除\",\"permission\":\"system:menu:remove\",\"pid\":7,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:44:37', 5);
INSERT INTO `sys_oper_log` VALUES (336, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 17:58:17', 147);
INSERT INTO `sys_oper_log` VALUES (337, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"字典查询\",\"permission\":\"system:dict:query\",\"pid\":65,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:34:31', 19);
INSERT INTO `sys_oper_log` VALUES (338, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683964679000,\"description\":\"字典管理\",\"icon\":\"iconfont icon-zidianguanli\",\"id\":65,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"字典管理\",\"pagePath\":\"/dict/index\",\"path\":\"/dict\",\"permission\":\"system:dict:list\",\"pid\":4,\"sortNum\":4,\"updateTime\":1683964702000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:34:39', 11);
INSERT INTO `sys_oper_log` VALUES (339, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"字典新增\",\"permission\":\"system:dict:add\",\"pid\":65,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:35:03', 9);
INSERT INTO `sys_oper_log` VALUES (340, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"字典修改\",\"permission\":\"system:dict:edit\",\"pid\":65,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:35:20', 6);
INSERT INTO `sys_oper_log` VALUES (341, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"字典删除\",\"permission\":\"system:dict:remove\",\"pid\":65,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:35:38', 7);
INSERT INTO `sys_oper_log` VALUES (342, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684319720000,\"id\":82,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"字典修改\",\"permission\":\"system:dict:edit\",\"pid\":65,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:35:43', 19);
INSERT INTO `sys_oper_log` VALUES (343, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684319720000,\"id\":82,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"字典修改\",\"permission\":\"system:dict:edit\",\"pid\":65,\"sortNum\":3,\"updateTime\":1684319743000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:35:45', 7);
INSERT INTO `sys_oper_log` VALUES (344, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:39:22', 239);
INSERT INTO `sys_oper_log` VALUES (345, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"文件查询\",\"permission\":\"system:file:query\",\"pid\":47,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:40:14', 7);
INSERT INTO `sys_oper_log` VALUES (346, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1682067742000,\"description\":\"文件管理\",\"icon\":\"el-icon-files\",\"id\":47,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"文件管理\",\"pagePath\":\"/file/index\",\"path\":\"/allfile\",\"permission\":\"system:file:list\",\"pid\":4,\"sortNum\":5,\"updateTime\":1682067954000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:40:47', 6);
INSERT INTO `sys_oper_log` VALUES (347, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"文件新增\",\"permission\":\"system:file:add\",\"pid\":47,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:41:42', 5);
INSERT INTO `sys_oper_log` VALUES (348, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684320102000,\"id\":85,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"文件新增\",\"permission\":\"system:file:add\",\"pid\":47,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:41:45', 5);
INSERT INTO `sys_oper_log` VALUES (349, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"文件修改\",\"permission\":\"system:file:edit\",\"pid\":47,\"sortNum\":3}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:42:11', 6);
INSERT INTO `sys_oper_log` VALUES (350, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"文件删除\",\"permission\":\"system:file:remove\",\"pid\":47,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:42:26', 6);
INSERT INTO `sys_oper_log` VALUES (351, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:46:49', 144);
INSERT INTO `sys_oper_log` VALUES (352, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683339408000,\"description\":\"操作日志\",\"icon\":\"el-icon-warning\",\"id\":59,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"操作日志\",\"pagePath\":\"/operlog/index\",\"path\":\"/operlog\",\"permission\":\"system:operlog:list\",\"pid\":57,\"sortNum\":307,\"updateTime\":1683341831000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:50:53', 27);
INSERT INTO `sys_oper_log` VALUES (353, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683338918000,\"description\":\"登录日志\",\"icon\":\"el-icon-s-claim\",\"id\":58,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"登录日志\",\"pagePath\":\"/loginlog/index\",\"path\":\"/loginlog\",\"permission\":\"system:loginlog:list\",\"pid\":57,\"sortNum\":308,\"updateTime\":1683341981000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:51:07', 7);
INSERT INTO `sys_oper_log` VALUES (354, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683339408000,\"description\":\"操作日志\",\"icon\":\"el-icon-warning\",\"id\":59,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"操作日志\",\"pagePath\":\"/operlog/index\",\"path\":\"/operlog\",\"permission\":\"system:operlog:list\",\"pid\":57,\"sortNum\":1,\"updateTime\":1683341831000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:51:13', 7);
INSERT INTO `sys_oper_log` VALUES (355, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683338918000,\"description\":\"登录日志\",\"icon\":\"el-icon-s-claim\",\"id\":58,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"登录日志\",\"pagePath\":\"/loginlog/index\",\"path\":\"/loginlog\",\"permission\":\"system:loginlog:list\",\"pid\":57,\"sortNum\":2,\"updateTime\":1683341981000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:51:18', 7);
INSERT INTO `sys_oper_log` VALUES (356, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"清空操作日志\",\"permission\":\"system:operlog:remove\",\"pid\":59,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:52:35', 15);
INSERT INTO `sys_oper_log` VALUES (357, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1684320755000,\"id\":88,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"清空日志\",\"permission\":\"system:operlog:remove\",\"pid\":59,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:52:47', 6);
INSERT INTO `sys_oper_log` VALUES (358, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"清空日志\",\"permission\":\"system:loginlog:remove\",\"pid\":58,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:53:23', 5);
INSERT INTO `sys_oper_log` VALUES (359, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:55:08', 150);
INSERT INTO `sys_oper_log` VALUES (360, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683951662000,\"description\":\"在线用户\",\"icon\":\"iconfont icon-zaixianyonhu\",\"id\":64,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"在线用户\",\"pagePath\":\"/Online\",\"path\":\"/online\",\"permission\":\"monitor:online:list\",\"pid\":63,\"sortNum\":401,\"updateTime\":1683951715000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:56:10', 7);
INSERT INTO `sys_oper_log` VALUES (361, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":1683951662000,\"description\":\"在线用户\",\"icon\":\"iconfont icon-zaixianyonhu\",\"id\":64,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"在线用户\",\"pagePath\":\"/Online\",\"path\":\"/online\",\"permission\":\"monitor:online:list\",\"pid\":63,\"sortNum\":1,\"updateTime\":1683951715000}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:56:17', 5);
INSERT INTO `sys_oper_log` VALUES (362, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"强退用户\",\"permission\":\"monitor:online:quit\",\"pid\":64,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:56:54', 5);
INSERT INTO `sys_oper_log` VALUES (363, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-17 18:57:39', 110);
INSERT INTO `sys_oper_log` VALUES (364, '字典类型', 3, 'com.wusuowei.system.controller.SysDictTypeController.remove()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/type/102', '127.0.0.1', '内网', '{\"dictIds\":[102]}', '\"登录状态已分配,不能删除\"', 1, 'com.wusuowei.system.exception.ServiceException: 登录状态已分配,不能删除\r\n	at com.wusuowei.system.service.impl.SysDictTypeServiceImpl.deleteDictTypeByIds(SysDictTypeServiceImpl.java:119)\r\n	at com.wusuowei.system.service.impl.SysDictTypeServiceImpl$$FastClassBySpringCGLIB$$1.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:687)\r\n	at com.wusuowei.system.service.impl.SysDictTypeServiceImpl$$EnhancerBySpringCGLIB$$1.deleteDictTypeByIds(<generated>)\r\n	at com.wusuowei.system.controller.SysDictTypeController.remove(SysDictTypeController.java:86)\r\n	at com.wusuowei.system.controller.SysDictTypeController$$FastClassBySpringCGLIB$$1.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:771)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.aspectj.AspectJAfterThrowingAdvice.invoke(AspectJAfterThrowingAdvice.java:62)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.adapter.AfterReturningAdviceInterceptor.invoke(AfterReturningAdviceInterceptor.java:55)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.adapter.MethodBeforeAdviceInterceptor.invoke(MethodBeforeAdviceInterceptor.java:56)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor.invoke(MethodSecurityInterceptor.java:69)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:95)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:691)\r\n	at com.wusuowei.system.controller.SysDictTypeController$$EnhancerBySpringCGLIB$$1.remove(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:190)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:105)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:878)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:792)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1040)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:943)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doDelete(FrameworkServlet.java:931)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:658)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:733)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:320)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.invoke(FilterSecurityInterceptor.java:126)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.doFilter(FilterSecurityInterceptor.java:90)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.access.ExceptionTranslationFilter.doFilter(ExceptionTranslationFilter.java:118)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.session.SessionManagementFilter.doFilter(SessionManagementFilter.java:137)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.AnonymousAuthenticationFilter.doFilter(AnonymousAuthenticationFilter.java:111)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter.doFilter(SecurityContextHolderAwareRequestFilter.java:158)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.savedrequest.RequestCacheAwareFilter.doFilter(RequestCacheAwareFilter.java:63)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter.doFilter(OAuth2AuthenticationProcessingFilter.java:176)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.logout.LogoutFilter.doFilter(LogoutFilter.java:116)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.header.HeaderWriterFilter.doHeadersAfter(HeaderWriterFilter.java:92)\r\n	at org.springframework.security.web.header.HeaderWriterFilter.doFilterInternal(HeaderWriterFilter.java:77)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.SecurityContextPersistenceFilter.doFilter(SecurityContextPersistenceFilter.java:105)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter.doFilterInternal(WebAsyncManagerIntegrationFilter.java:56)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:215)\r\n	at org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:178)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:358)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:271)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.doFilterInternal(WebMvcMetricsFilter.java:93)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:202)\r\n	at org.apache.catalina.core.StandardContextValve.__invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:41002)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:542)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:143)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:374)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:888)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1597)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\r\n	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:748)\r\n', '2023-05-18 08:46:21', 10);
INSERT INTO `sys_oper_log` VALUES (365, '字典类型', 3, 'com.wusuowei.system.controller.SysDictDataController.remove()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/data/110,111', '127.0.0.1', '内网', '{\"dictCodes\":[110,111]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:46:28', 39);
INSERT INTO `sys_oper_log` VALUES (366, '字典类型', 3, 'com.wusuowei.system.controller.SysDictTypeController.remove()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/dict/type/102', '127.0.0.1', '内网', '{\"dictIds\":[102]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:46:36', 50);
INSERT INTO `sys_oper_log` VALUES (367, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/monitor/online/46', '127.0.0.1', '内网', '{\"uid\":\"46\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:51:09', 7);
INSERT INTO `sys_oper_log` VALUES (368, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"bd85d565191f88bf5e799756c21f4b0f\",\"fileName\":\"202013140李光耀.docx\",\"fileSize\":411485,\"fileType\":\"word\",\"id\":293}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:53:26', 58);
INSERT INTO `sys_oper_log` VALUES (369, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"9a800fdd4c9d5b5117f59568636477b4\",\"fileName\":\"附件2：参赛题库.docx\",\"fileSize\":138148,\"fileType\":\"word\",\"id\":294}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:55:32', 58);
INSERT INTO `sys_oper_log` VALUES (370, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"e7aa824040cf0377cfe459a584c1d65b\",\"fileName\":\"第7章 项目总结.docx\",\"fileSize\":2917387,\"fileType\":\"word\",\"id\":295}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 08:55:38', 20);
INSERT INTO `sys_oper_log` VALUES (371, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"e545a12e3ea738bdfc1e149012e00b13\",\"fileName\":\"20-21-1期末考试预排表 -按星期 20.11.26.xlsx\",\"fileSize\":16628,\"fileType\":\"excel\",\"id\":296}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 09:15:09', 20);
INSERT INTO `sys_oper_log` VALUES (372, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"e7aa824040cf0377cfe459a584c1d65b\",\"fileName\":\"第7章 项目总结.docx\",\"fileSize\":2917387,\"fileType\":\"word\",\"id\":297}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 09:16:47', 24);
INSERT INTO `sys_oper_log` VALUES (373, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"9a800fdd4c9d5b5117f59568636477b4\",\"fileName\":\"附件2：参赛题库.docx\",\"fileSize\":138148,\"fileType\":\"word\",\"id\":298}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:11:54', 1567);
INSERT INTO `sys_oper_log` VALUES (374, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"e7aa824040cf0377cfe459a584c1d65b\",\"fileName\":\"第7章 项目总结.docx\",\"fileSize\":2917387,\"fileType\":\"word\",\"id\":299}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:12:02', 22);
INSERT INTO `sys_oper_log` VALUES (375, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"e7aa824040cf0377cfe459a584c1d65b\",\"fileName\":\"第7章 项目总结.docx\",\"fileSize\":2917387,\"fileType\":\"word\",\"id\":300}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:13:46', 26);
INSERT INTO `sys_oper_log` VALUES (376, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:9090/system/file/', '127.0.0.1', '内网', '{\"fileUploadInfo\":{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"9a800fdd4c9d5b5117f59568636477b4\",\"fileName\":\"附件2：参赛题库.docx\",\"fileSize\":138148,\"fileType\":\"word\",\"id\":301}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:13:49', 19);
INSERT INTO `sys_oper_log` VALUES (377, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-cpu\",\"menuType\":\"C\",\"name\":\"test\",\"pagePath\":\"/test\",\"path\":\"/test\",\"permission\":\"test\",\"pid\":61,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:18:10', 32);
INSERT INTO `sys_oper_log` VALUES (378, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,91,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:18:14', 226);
INSERT INTO `sys_oper_log` VALUES (379, '菜单管理', 3, 'com.wusuowei.ucenter.controller.SysMenuController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu/91', '127.0.0.1', '内网', '{\"menuId\":91}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":1011}', 0, NULL, '2023-05-18 10:21:56', 39);
INSERT INTO `sys_oper_log` VALUES (380, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:22:02', 189);
INSERT INTO `sys_oper_log` VALUES (381, '菜单管理', 3, 'com.wusuowei.ucenter.controller.SysMenuController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu/91', '127.0.0.1', '内网', '{\"menuId\":91}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:22:13', 76);
INSERT INTO `sys_oper_log` VALUES (382, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:23:21', 116);
INSERT INTO `sys_oper_log` VALUES (383, '用户管理', 3, 'com.wusuowei.ucenter.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/37', '127.0.0.1', '内网', '{\"userIds\":[\"37\"]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:31:00', 2931);
INSERT INTO `sys_oper_log` VALUES (384, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:33:18', 119);
INSERT INTO `sys_oper_log` VALUES (385, '用户管理', 3, 'com.wusuowei.ucenter.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'wusupweilgy', '普通用户', 'http://192.168.56.1:8160/auth/user/36', '127.0.0.1', '内网', '{\"userIds\":[\"36\"]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:55:13', 80);
INSERT INTO `sys_oper_log` VALUES (386, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'wusupweilgy', '普通用户', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-coffee\\r\\n\",\"menuType\":\"M\",\"name\":\"fff\",\"path\":\"/fff\",\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:55:46', 26);
INSERT INTO `sys_oper_log` VALUES (387, '菜单管理', 3, 'com.wusuowei.ucenter.controller.SysMenuController.deleteBatch()', 'DELETE', 0, 'wusupweilgy', '普通用户', 'http://192.168.56.1:8160/auth/menu/92', '127.0.0.1', '内网', '{\"menuId\":92}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 10:55:49', 55);
INSERT INTO `sys_oper_log` VALUES (388, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:5000/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_role\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 11:05:58', 3769);
INSERT INTO `sys_oper_log` VALUES (389, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-document\",\"menuType\":\"C\",\"name\":\"头像上传\",\"pagePath\":\"/test\",\"path\":\"/test\",\"permission\":\"test\",\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 13:18:49', 31);
INSERT INTO `sys_oper_log` VALUES (390, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,93,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-18 13:19:23', 205);
INSERT INTO `sys_oper_log` VALUES (391, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://localhost:9000/image/lgy.jpg\",\"createTime\":1642857027000,\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":1684388079000,\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":1682093490000,\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-18 13:40:40', 4);
INSERT INTO `sys_oper_log` VALUES (392, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://localhost:9000/image/LGY.png\",\"createTime\":1642857027000,\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":1684388079000,\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":1682093490000,\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-18 13:40:50', 7);
INSERT INTO `sys_oper_log` VALUES (393, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://localhost:9000/image/LGY.png\",\"createTime\":1642857027000,\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":1684678732000,\"nickname\":\"无所谓^_^ff\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":1682093490000,\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-21 22:19:00', 7);
INSERT INTO `sys_oper_log` VALUES (394, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"icon\":\"iconfont icon-7\",\"menuType\":\"M\",\"name\":\"个人博客\",\"path\":\"/mycsdn\",\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:54:28', 11);
INSERT INTO `sys_oper_log` VALUES (395, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"M\",\"name\":\"个人博客\",\"path\":\"/mycsdn\",\"sortNum\":5}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:54:33', 10);
INSERT INTO `sys_oper_log` VALUES (396, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:54:58', 8);
INSERT INTO `sys_oper_log` VALUES (397, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:55:28', 150);
INSERT INTO `sys_oper_log` VALUES (398, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/2', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":2,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:57:26', 94);
INSERT INTO `sys_oper_log` VALUES (399, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:57:31', 102);
INSERT INTO `sys_oper_log` VALUES (400, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:57:51', 89);
INSERT INTO `sys_oper_log` VALUES (401, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:58:58', 81);
INSERT INTO `sys_oper_log` VALUES (402, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 16:59:58', 99);
INSERT INTO `sys_oper_log` VALUES (403, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:02:25', 75);
INSERT INTO `sys_oper_log` VALUES (404, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:03:37', 63);
INSERT INTO `sys_oper_log` VALUES (405, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:09:57', 7);
INSERT INTO `sys_oper_log` VALUES (406, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:10:36', 5);
INSERT INTO `sys_oper_log` VALUES (407, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:10:54', 6);
INSERT INTO `sys_oper_log` VALUES (408, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/menu', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 17:11:24', 5);
INSERT INTO `sys_oper_log` VALUES (409, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:48:20', 26);
INSERT INTO `sys_oper_log` VALUES (410, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:48:49', 22989);
INSERT INTO `sys_oper_log` VALUES (411, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:49:15', 36733);
INSERT INTO `sys_oper_log` VALUES (412, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:54:51', 156);
INSERT INTO `sys_oper_log` VALUES (413, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:55:00', 52197);
INSERT INTO `sys_oper_log` VALUES (414, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:55:56', 4007);
INSERT INTO `sys_oper_log` VALUES (415, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 17:56:48', 69513);
INSERT INTO `sys_oper_log` VALUES (416, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:01:15', 2409);
INSERT INTO `sys_oper_log` VALUES (417, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:01:24', 89751);
INSERT INTO `sys_oper_log` VALUES (418, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:08:40', 38140);
INSERT INTO `sys_oper_log` VALUES (419, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:12:27', 6911);
INSERT INTO `sys_oper_log` VALUES (420, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:13:05', 5127);
INSERT INTO `sys_oper_log` VALUES (421, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:19:28', 144345);
INSERT INTO `sys_oper_log` VALUES (422, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:25:41', 23102);
INSERT INTO `sys_oper_log` VALUES (423, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"123\",\"password\":\"admin\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:26:58', 153);
INSERT INTO `sys_oper_log` VALUES (424, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:29:09', 65);
INSERT INTO `sys_oper_log` VALUES (425, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"123\",\"password\":\"admin\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:29:23', 133);
INSERT INTO `sys_oper_log` VALUES (426, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:29:33', 70);
INSERT INTO `sys_oper_log` VALUES (427, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:29:42', 139);
INSERT INTO `sys_oper_log` VALUES (428, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"123\",\"password\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:29:53', 135);
INSERT INTO `sys_oper_log` VALUES (429, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:30:04', 66);
INSERT INTO `sys_oper_log` VALUES (430, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:30:36', 134);
INSERT INTO `sys_oper_log` VALUES (431, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"123\",\"password\":\"admin\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:31:10', 133);
INSERT INTO `sys_oper_log` VALUES (432, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\"}}', '{\"msg\":\"原密码错误\",\"code\":500}', 0, NULL, '2023-05-22 18:31:16', 65);
INSERT INTO `sys_oper_log` VALUES (433, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:32:09', 136);
INSERT INTO `sys_oper_log` VALUES (434, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:37:48', 129);
INSERT INTO `sys_oper_log` VALUES (435, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,88,58,89,63,64,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:38:55', 104);
INSERT INTO `sys_oper_log` VALUES (436, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"123\",\"password\":\"admin\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:39:17', 135);
INSERT INTO `sys_oper_log` VALUES (437, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/password/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userPasswordVo\":{\"id\":1,\"newPassword\":\"admin\",\"password\":\"123\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-22 18:39:37', 135);
INSERT INTO `sys_oper_log` VALUES (438, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-22 23:22:24\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-22 23:23:05', 8);
INSERT INTO `sys_oper_log` VALUES (439, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-22 23:22:24\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-22 23:23:08', 3);
INSERT INTO `sys_oper_log` VALUES (440, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/LGY.png\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-22 23:22:24\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-22 23:23:18', 7);
INSERT INTO `sys_oper_log` VALUES (441, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-22 23:22:24\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-22 23:23:33', 6);
INSERT INTO `sys_oper_log` VALUES (442, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-22 23:22:24\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-22 23:24:38', 7);
INSERT INTO `sys_oper_log` VALUES (443, '修改密码', 2, 'com.wusuowei.ucenter.controller.SysUserController.password()', 'POST', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/user/password/', '127.0.0.1', '内网', '{\"userPasswordVo\":{\"id\":16,\"newPassword\":\"vip\",\"password\":\"admin\",\"username\":\"jjj\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 21:54:25', 143);
INSERT INTO `sys_oper_log` VALUES (444, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"查询日志\",\"permission\":\"system:operlog:query\",\"pid\":59,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:05:05', 24);
INSERT INTO `sys_oper_log` VALUES (445, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-17 18:52:35\",\"id\":88,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"清空日志\",\"permission\":\"system:operlog:remove\",\"pid\":59,\"sortNum\":2,\"updateTime\":\"2023-05-17 18:52:47\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:05:10', 14);
INSERT INTO `sys_oper_log` VALUES (446, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"查询日志\",\"permission\":\"system:loginlog:query\",\"pid\":58,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:05:31', 9);
INSERT INTO `sys_oper_log` VALUES (447, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-17 18:53:23\",\"id\":89,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"清空日志\",\"permission\":\"system:loginlog:remove\",\"pid\":58,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:05:37', 6);
INSERT INTO `sys_oper_log` VALUES (448, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"查询用户\",\"permission\":\"monitor:online:query\",\"pid\":64,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:05:56', 6);
INSERT INTO `sys_oper_log` VALUES (449, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,95,96,97,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:06:18', 133);
INSERT INTO `sys_oper_log` VALUES (450, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/system/monitor/online/16', '127.0.0.1', '内网', '{\"uid\":\"16\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:07:25', 76);
INSERT INTO `sys_oper_log` VALUES (451, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,88,89,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:07:54', 81);
INSERT INTO `sys_oper_log` VALUES (452, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,5,68,69,70,71,72,76,77,80,81,84,85,86,95,96,97,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:08:58', 62);
INSERT INTO `sys_oper_log` VALUES (453, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,80,81,47,84,85,86,87,88,89,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:09:13', 68);
INSERT INTO `sys_oper_log` VALUES (454, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,88,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:09:51', 51);
INSERT INTO `sys_oper_log` VALUES (455, '用户管理', 2, 'com.wusuowei.ucenter.controller.SysUserController.edit()', 'PUT', 0, 'jjj', '超级管理员,普通用户', 'http://localhost:8160/auth/user', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"河南\",\"createTime\":\"2022-02-26 22:10:14\",\"email\":\"2553229176@qq.com\",\"id\":\"16\",\"isDelete\":false,\"loginTime\":\"2023-05-23 23:09:56\",\"nickname\":\"小黑子\",\"phonenumber\":\"18337572656\",\"roleid\":[3],\"status\":\"0\",\"updateTime\":\"2023-05-23 21:53:48\",\"username\":\"vip\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-23 23:10:17', 17);
INSERT INTO `sys_oper_log` VALUES (456, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,76,77,80,81,84,85,86,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:11:21', 41);
INSERT INTO `sys_oper_log` VALUES (457, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,76,77,80,81,84,85,86,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:11:53', 39);
INSERT INTO `sys_oper_log` VALUES (458, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,80,81,84,85,86,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:13:09', 42);
INSERT INTO `sys_oper_log` VALUES (459, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:14:58', 77);
INSERT INTO `sys_oper_log` VALUES (460, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,69,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:16:48', 61);
INSERT INTO `sys_oper_log` VALUES (461, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:25:58', 34);
INSERT INTO `sys_oper_log` VALUES (462, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,88,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:26:07', 34);
INSERT INTO `sys_oper_log` VALUES (463, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:36:39', 90);
INSERT INTO `sys_oper_log` VALUES (464, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:37:20', 83);
INSERT INTO `sys_oper_log` VALUES (465, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:37:29', 83);
INSERT INTO `sys_oper_log` VALUES (466, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:39:56', 154);
INSERT INTO `sys_oper_log` VALUES (467, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,5,68,69,70,71,72,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,88,89,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:40:18', 100);
INSERT INTO `sys_oper_log` VALUES (468, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,90,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:40:40', 143);
INSERT INTO `sys_oper_log` VALUES (469, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,6,72,73,74,75,76,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:41:10', 83);
INSERT INTO `sys_oper_log` VALUES (470, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,5,68,69,70,71,6,72,73,74,75,76,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:41:24', 135);
INSERT INTO `sys_oper_log` VALUES (471, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,6,72,73,74,75,76,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:50:11', 287);
INSERT INTO `sys_oper_log` VALUES (472, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:53:01', 225);
INSERT INTO `sys_oper_log` VALUES (473, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:53:08', 179);
INSERT INTO `sys_oper_log` VALUES (474, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:56:39', 157);
INSERT INTO `sys_oper_log` VALUES (475, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:58:35', 162);
INSERT INTO `sys_oper_log` VALUES (476, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,97,61,62,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-23 23:59:27', 64);
INSERT INTO `sys_oper_log` VALUES (477, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,97,61,62,94,63,64]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 00:01:17', 171);
INSERT INTO `sys_oper_log` VALUES (478, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,61,62,94,4,5,6,7,65,47,57,59,58,63,64]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 00:02:35', 140);
INSERT INTO `sys_oper_log` VALUES (479, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_dict\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 15:40:48', 121);
INSERT INTO `sys_oper_log` VALUES (480, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_dict_data\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 15:41:55', 117);
INSERT INTO `sys_oper_log` VALUES (481, '代码生成', 3, 'com.wusuowei.generator.controller.GenController.remove()', 'DELETE', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/4', '127.0.0.1', '内网', '{\"tableIds\":[4]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 15:58:16', 70);
INSERT INTO `sys_oper_log` VALUES (482, '代码生成', 3, 'com.wusuowei.generator.controller.GenController.remove()', 'DELETE', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/5', '127.0.0.1', '内网', '{\"tableIds\":[5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 15:58:19', 42);
INSERT INTO `sys_oper_log` VALUES (483, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_dict\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:26:16', 89);
INSERT INTO `sys_oper_log` VALUES (484, '代码生成', 3, 'com.wusuowei.generator.controller.GenController.remove()', 'DELETE', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/6,7', '127.0.0.1', '内网', '{\"tableIds\":[6,7]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:30:35', 24);
INSERT INTO `sys_oper_log` VALUES (485, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"数据库列表\",\"permission\":\"tool:gen:list\",\"pid\":62,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:35:53', 22);
INSERT INTO `sys_oper_log` VALUES (486, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-11 14:33:05\",\"description\":\"代码生成\",\"icon\":\"iconfont icon-daima\",\"id\":62,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"代码生成\",\"pagePath\":\"/Gen\",\"path\":\"/gen\",\"permission\":\"tool:gen:list\",\"pid\":61,\"sortNum\":1,\"updateTime\":\"2023-05-16 17:59:41\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:36:17', 18);
INSERT INTO `sys_oper_log` VALUES (487, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-24 16:35:53\",\"id\":98,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"数据库查询\",\"permission\":\"tool:gen:query\",\"pid\":62,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:36:33', 40);
INSERT INTO `sys_oper_log` VALUES (488, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"导入表结构\",\"permission\":\"tool:gen:import\",\"pid\":62,\"sortNum\":3}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:37:33', 11);
INSERT INTO `sys_oper_log` VALUES (489, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"预览代码\",\"permission\":\"tool:gen:preview\",\"pid\":62,\"sortNum\":4}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:38:01', 11);
INSERT INTO `sys_oper_log` VALUES (490, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"删除代码\",\"permission\":\"tool:gen:remove\",\"pid\":62,\"sortNum\":5}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:38:23', 11);
INSERT INTO `sys_oper_log` VALUES (491, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"生成代码\",\"permission\":\"tool:gen:code\",\"pid\":62,\"sortNum\":6}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:38:44', 12);
INSERT INTO `sys_oper_log` VALUES (492, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"下载代码\",\"permission\":\"tool:gen:code\",\"pid\":62,\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:39:02', 37);
INSERT INTO `sys_oper_log` VALUES (493, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,103,98,99,100,102,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:39:28', 273);
INSERT INTO `sys_oper_log` VALUES (494, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,103,98,99,102,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:39:51', 191);
INSERT INTO `sys_oper_log` VALUES (495, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_dict\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:40:16', 63);
INSERT INTO `sys_oper_log` VALUES (496, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,94,4,5,6,7,65,47,57,59,58,63,64]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:44:01', 147);
INSERT INTO `sys_oper_log` VALUES (497, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,95,96,97,99,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:44:52', 151);
INSERT INTO `sys_oper_log` VALUES (498, '代码生成', 6, 'com.wusuowei.generator.controller.GenController.importTableSave()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/importTable', '127.0.0.1', '内网', '{\"tables\":\"sys_dict_data\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:46:22', 64);
INSERT INTO `sys_oper_log` VALUES (499, '代码生成', 3, 'com.wusuowei.generator.controller.GenController.remove()', 'DELETE', 0, 'vip', 'Vip用户', 'http://localhost:8160/generator/gen/8', '127.0.0.1', '内网', '{\"tableIds\":[8]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 16:46:24', 11);
INSERT INTO `sys_oper_log` VALUES (500, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/2', '127.0.0.1', '内网', '{\"roleId\":2,\"menuIds\":[3,68,72,76,80,84,95,96,97,103,98,99,100,102,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 17:11:30', 208);
INSERT INTO `sys_oper_log` VALUES (501, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 17:46:24', 389);
INSERT INTO `sys_oper_log` VALUES (502, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/3', '127.0.0.1', '内网', '{\"roleId\":3,\"menuIds\":[3,68,72,73,76,77,80,81,84,85,86,95,96,97,103,98,99,100,102,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 17:52:25', 188);
INSERT INTO `sys_oper_log` VALUES (503, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"menuType\":\"F\",\"name\":\"修改个人资料\",\"permission\":\"system:user:ordinary\",\"pid\":5,\"sortNum\":5}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:17:12', 18);
INSERT INTO `sys_oper_log` VALUES (504, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-24 23:17:12\",\"id\":104,\"isDelete\":false,\"menuType\":\"F\",\"name\":\"修改个人信息\",\"permission\":\"system:user:ordinary\",\"pid\":5,\"sortNum\":5}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:17:27', 13);
INSERT INTO `sys_oper_log` VALUES (505, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,104,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:19:02', 210);
INSERT INTO `sys_oper_log` VALUES (506, '修改个人信息', 2, 'com.wusuowei.ucenter.controller.SysUserController.updateUser()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user/updateUser', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"浙江省\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-24 23:19:12\",\"nickname\":\"无所谓^_^11\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-24 23:19:17', 7);
INSERT INTO `sys_oper_log` VALUES (507, '修改个人信息', 2, 'com.wusuowei.ucenter.controller.SysUserController.updateUser()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user/updateUser', '127.0.0.1', '内网', '{\"sysUser\":{\"address\":\"浙江省\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-24 23:19:12\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-24 23:19:27', 6);
INSERT INTO `sys_oper_log` VALUES (508, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:19:36', 139);
INSERT INTO `sys_oper_log` VALUES (509, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:19:39', 116);
INSERT INTO `sys_oper_log` VALUES (510, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:19:42', 112);
INSERT INTO `sys_oper_log` VALUES (511, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:20:00', 111);
INSERT INTO `sys_oper_log` VALUES (512, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:20:06', 111);
INSERT INTO `sys_oper_log` VALUES (513, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,68,69,70,71,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94,4,5]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-24 23:21:25', 86);
INSERT INTO `sys_oper_log` VALUES (514, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-05-22 16:54:28\",\"icon\":\"iconfont icon-7\",\"id\":94,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"个人博客\",\"pagePath\":\"/MyCSDN\",\"path\":\"/mycsdn\",\"permission\":\"mycsdn\",\"sortNum\":5,\"updateTime\":\"2023-05-22 16:54:33\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-25 14:37:33', 13);
INSERT INTO `sys_oper_log` VALUES (515, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:6000/system/file/del/batch', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"fileUploadInfos\":[{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"eb81db8974a4924ba39ccc049c078516\",\"fileName\":\"lgy.jpg\",\"fileSize\":35951,\"fileType\":\"image\",\"id\":289}]}', '{\"msg\":\"文件删除失败\",\"code\":500}', 0, NULL, '2023-05-26 16:05:39', 2248);
INSERT INTO `sys_oper_log` VALUES (516, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:6000/system/file/del/batch', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"fileUploadInfos\":[{\"chunkNum\":1,\"chunkSize\":10485760,\"fileMd5\":\"eb81db8974a4924ba39ccc049c078516\",\"fileName\":\"lgy.jpg\",\"fileSize\":35951,\"fileType\":\"image\",\"id\":289}]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:06:08', 406);
INSERT INTO `sys_oper_log` VALUES (517, '在线用户', 3, 'com.wusuowei.system.controller.SysOnlineController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:6000/system/monitor/online/16', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"uid\":\"16\"}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:09:13', 5);
INSERT INTO `sys_oper_log` VALUES (518, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:6000/system/file/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"fileUploadInfo\":{\"chunkNum\":3,\"chunkSize\":10485760,\"fileMd5\":\"d2549e49792fd8a318ac53a6cad19f5e\",\"fileName\":\"share_ee75f3c80f365477f4825173824fbde4.mp4\",\"fileSize\":21858927,\"fileType\":\"video\",\"id\":291,\"uploadId\":\"c34a463c-b67c-4883-be88-76a528099457\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:13:42', 18);
INSERT INTO `sys_oper_log` VALUES (519, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:6000/system/file/', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"fileUploadInfo\":{\"chunkNum\":3,\"chunkSize\":10485760,\"fileMd5\":\"d2549e49792fd8a318ac53a6cad19f5e\",\"fileName\":\"share_ee75f3c80f365477f4825173824fbde4.mp4\",\"fileSize\":21858927,\"fileType\":\"video\",\"id\":304,\"uploadId\":\"7f773587-785e-4f90-bad2-61f69ebf9716\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:13:56', 13);
INSERT INTO `sys_oper_log` VALUES (520, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/1', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,104,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,61,62,103,98,99,100,101,102,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:48:08', 190);
INSERT INTO `sys_oper_log` VALUES (521, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/role/roleMenu/2', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"roleId\":2,\"menuIds\":[3,68,104,72,76,80,84,95,96,97,103,98,99,100,102,94,4,5,6,7,65,47,57,59,58,63,64,61,62]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:48:28', 113);
INSERT INTO `sys_oper_log` VALUES (522, '修改个人信息', 2, 'com.wusuowei.ucenter.controller.SysUserController.updateUser()', 'PUT', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/updateUser', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"sysUser\":{\"address\":\"浙江省台州市\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2023-05-26 16:48:15\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2023-05-26 16:48:36', 8);
INSERT INTO `sys_oper_log` VALUES (523, '用户管理', 3, 'com.wusuowei.ucenter.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/26', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userIds\":[\"26\"]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:51:43', 68);
INSERT INTO `sys_oper_log` VALUES (524, '用户管理', 3, 'com.wusuowei.ucenter.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/28,29', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userIds\":[\"28\",\"29\"]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:51:49', 13);
INSERT INTO `sys_oper_log` VALUES (525, '用户管理', 3, 'com.wusuowei.ucenter.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://192.168.56.1:8160/auth/user/17,18,25', '127.0.0.1,127.0.0.1', ' 本机地址', '{\"userIds\":[\"17\",\"18\",\"25\"]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-05-26 16:52:00', 17);
INSERT INTO `sys_oper_log` VALUES (526, '菜单管理`', 1, 'com.wusuowei.ucenter.controller.SysMenuController.add()', 'POST', 0, 'vip', 'Vip用户', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"icon\":\"el-icon-chat-dot-round\",\"menuType\":\"C\",\"name\":\"温湿度监控\",\"pagePath\":\"/DHT1\",\"path\":\"/dht11\",\"permission\":\"monitor:dht11:list\",\"pid\":63,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-06-18 16:29:40', 13);
INSERT INTO `sys_oper_log` VALUES (527, '角色管理', 1, 'com.wusuowei.ucenter.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '127.0.0.1', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,104,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,105,61,62,103,98,99,100,101,102,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-06-18 16:30:50', 138);
INSERT INTO `sys_oper_log` VALUES (528, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-06-18 16:29:40\",\"icon\":\"el-icon-chat-dot-round\",\"id\":105,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"温湿度监控\",\"pagePath\":\"/DHT11\",\"path\":\"/dht11\",\"permission\":\"monitor:dht11:list\",\"pid\":63,\"sortNum\":2}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-06-18 16:31:31', 9);
INSERT INTO `sys_oper_log` VALUES (529, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-06-18 16:29:40\",\"icon\":\"icon-wenshidu\",\"id\":105,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"温湿度监控\",\"pagePath\":\"/DHT11\",\"path\":\"/dht11\",\"permission\":\"monitor:dht11:list\",\"pid\":63,\"sortNum\":2,\"updateTime\":\"2023-06-18 16:31:31\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-06-20 19:55:39', 10);
INSERT INTO `sys_oper_log` VALUES (530, '菜单管理', 2, 'com.wusuowei.ucenter.controller.SysMenuController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '127.0.0.1', '内网', '{\"menu\":{\"createTime\":\"2023-06-18 16:29:40\",\"icon\":\"iconfont icon-wenshidu\",\"id\":105,\"isDelete\":false,\"menuType\":\"C\",\"name\":\"温湿度监控\",\"pagePath\":\"/DHT11\",\"path\":\"/dht11\",\"permission\":\"monitor:dht11:list\",\"pid\":63,\"sortNum\":2,\"updateTime\":\"2023-06-18 16:31:31\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2023-06-20 19:56:08', 5);
INSERT INTO `sys_oper_log` VALUES (531, '字典数据', 2, 'com.wusuowei.system.controller.SysDictDataController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/system/dict/data', '127.0.0.1', '内网', '{\"dict\":{\"createBy\":\"admin\",\"createTime\":\"2023-05-17 10:08:51\",\"cssClass\":\"custom-pink\",\"dictCode\":101,\"dictLabel\":\"图片\",\"dictSort\":0,\"dictType\":\"sys_file_type\",\"dictValue\":\"image\",\"isDefault\":\"N\",\"listClass\":\"primary\",\"status\":\"0\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":1}', 0, NULL, '2023-07-12 12:50:45', 19);
INSERT INTO `sys_oper_log` VALUES (532, '字典数据', 2, 'com.wusuowei.system.controller.SysDictDataController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/system/dict/data', '127.0.0.1', '内网', '{\"dict\":{\"createBy\":\"admin\",\"createTime\":\"2023-05-17 10:08:51\",\"cssClass\":\"custom-pink\",\"dictCode\":101,\"dictLabel\":\"图片\",\"dictSort\":0,\"dictType\":\"sys_file_type\",\"dictValue\":\"image\",\"isDefault\":\"N\",\"listClass\":\"primary\",\"status\":\"0\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":1}', 0, NULL, '2023-07-12 12:59:53', 17);
INSERT INTO `sys_oper_log` VALUES (533, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '123', '234', 0, '', '2024-03-10 16:09:49', 14);
INSERT INTO `sys_oper_log` VALUES (534, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"河南\",\"avatarUrl\":\"https://foruda.gitee.com/avatar/1677168816178868832/10095008_wusupweilgy_1677168816.png\",\"createTime\":\"2023-05-03 22:49:37\",\"email\":\"2553229176@qq.com\",\"id\":\"42\",\"isDelete\":false,\"loginTime\":\"2023-06-18 16:19:24\",\"nickname\":\"LGY\",\"phonenumber\":\"18337572656\",\"roleid\":[2],\"sex\":0,\"status\":\"0\",\"unionid\":\"10095008\",\"updateTime\":\"2023-05-03 23:11:02\",\"username\":\"wusu\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2024-03-10 16:09:49', 14);
INSERT INTO `sys_oper_log` VALUES (535, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"河南\",\"avatarUrl\":\"https://foruda.gitee.com/avatar/1677168816178868832/10095008_wusupweilgy_1677168816.png\",\"createTime\":\"2023-05-03 22:49:37\",\"email\":\"2553229176@qq.com\",\"id\":\"42\",\"isDelete\":false,\"loginTime\":\"2023-06-18 16:19:24\",\"nickname\":\"LGY\",\"phonenumber\":\"18337572656\",\"roleid\":[2],\"sex\":0,\"status\":\"0\",\"unionid\":\"10095008\",\"updateTime\":\"2023-05-03 23:11:02\",\"username\":\"wusu\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2024-03-10 16:14:25', 15);
INSERT INTO `sys_oper_log` VALUES (536, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"河南\",\"avatarUrl\":\"https://foruda.gitee.com/avatar/1677168816178868832/10095008_wusupweilgy_1677168816.png\",\"createTime\":\"2023-05-03 22:49:37\",\"email\":\"2553229176@qq.com\",\"id\":\"42\",\"isDelete\":false,\"loginTime\":\"2023-06-18 16:19:24\",\"nickname\":\"LGY\",\"phonenumber\":\"18337572656\",\"roleid\":[2],\"sex\":0,\"status\":\"0\",\"unionid\":\"10095008\",\"updateTime\":\"2023-05-03 23:11:02\",\"username\":\"wusuowei\"}}', '{\"msg\":\"success\",\"code\":200,\"data\":true}', 0, NULL, '2024-03-10 16:28:28', 21);
INSERT INTO `sys_oper_log` VALUES (537, '角色管理', 2, 'com.wusuowei.system.controller.SysRoleController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role', '192.168.131.137', '内网', '{\"role\":{\"createTime\":\"2023-05-07 18:31:45\",\"description\":\"sss334\",\"id\":3,\"isDelete\":false,\"name\":\"Vip用户\",\"roleKey\":\"vip\",\"updateTime\":\"2024-03-10 15:39:40\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 16:28:58', 7);
INSERT INTO `sys_oper_log` VALUES (538, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createTime\":\"2024-03-10 19:19:28\",\"enable\":true,\"fileMd5\":\"ef5f45b70a4d6942ea49dda792bd316c\",\"fileName\":\"屏幕截图 2023-11-09 203211.png\",\"fileSize\":130703,\"fileType\":\"image\",\"id\":305,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/ef5f45b70a4d6942ea49dda792bd316c.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 19:38:50', 20);
INSERT INTO `sys_oper_log` VALUES (539, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createTime\":\"2024-03-10 19:38:22\",\"enable\":true,\"fileMd5\":\"65671d60e96db0c2f12492d867e42589\",\"fileName\":\"屏幕截图 2023-11-16 211129.png\",\"fileSize\":3838,\"fileType\":\"image\",\"id\":306,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/65671d60e96db0c2f12492d867e42589.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 19:38:51', 12);
INSERT INTO `sys_oper_log` VALUES (540, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"浙江省台1州市1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2024-03-10 15:15:09\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"roleid\":[1],\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"管理员用户不能修改\",\"code\":500}', 0, NULL, '2024-03-10 19:54:41', 0);
INSERT INTO `sys_oper_log` VALUES (541, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"浙江省台1州市1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2024-03-10 15:15:09\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"roleid\":[1],\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"管理员用户不能修改\",\"code\":500}', 0, NULL, '2024-03-10 19:54:54', 0);
INSERT INTO `sys_oper_log` VALUES (542, '用户管理', 2, 'com.wusuowei.system.controller.SysUserController.edit()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"浙江省台1州市1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2024-03-10 15:15:09\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"roleid\":[1],\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"管理员用户不能修改\",\"code\":500}', 0, NULL, '2024-03-10 19:57:48', 0);
INSERT INTO `sys_oper_log` VALUES (543, '修改个人信息', 2, 'com.wusuowei.system.controller.SysUserController.updateUser()', 'PUT', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user/updateUser', '192.168.131.137', '内网', '{\"sysUser\":{\"address\":\"浙江省台1州市1\",\"avatarUrl\":\"http://47.96.133.5:9000/image/lgy.jpg\",\"createTime\":\"2022-01-22 21:10:27\",\"email\":\"2673152463@qq.com\",\"id\":\"1\",\"isDelete\":false,\"loginTime\":\"2024-03-10 15:15:09\",\"nickname\":\"无所谓^_^\",\"phonenumber\":\"18337572656\",\"status\":\"0\",\"updateTime\":\"2023-04-22 00:11:30\",\"username\":\"admin\"}}', '{\"msg\":\"管理员用户不能修改\",\"code\":500}', 0, NULL, '2024-03-10 19:57:55', 0);
INSERT INTO `sys_oper_log` VALUES (544, '用户管理', 3, 'com.wusuowei.system.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user/1', '192.168.131.137', '内网', '{\"userIds\":[\"1\"]}', '{\"msg\":\"管理员用户不能删除\",\"code\":500}', 0, NULL, '2024-03-10 19:58:00', 0);
INSERT INTO `sys_oper_log` VALUES (545, '角色管理', 1, 'com.wusuowei.system.controller.SysRoleController.roleMenu()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/role/roleMenu/1', '192.168.131.137', '内网', '{\"roleId\":1,\"menuIds\":[3,4,5,68,69,70,71,104,6,72,73,74,75,7,76,77,78,79,65,80,81,82,83,47,84,85,86,87,57,59,95,88,58,96,89,63,64,90,97,105,61,62,103,98,99,100,101,102,94]}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 19:58:22', 206);
INSERT INTO `sys_oper_log` VALUES (546, '用户管理', 3, 'com.wusuowei.system.controller.SysUserController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/user/1', '192.168.131.137', '内网', '{\"userIds\":[\"1\"]}', '{\"msg\":\"管理员用户不能删除\",\"code\":500}', 0, NULL, '2024-03-10 19:59:39', 0);
INSERT INTO `sys_oper_log` VALUES (547, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 19:59:54\",\"enable\":true,\"fileMd5\":\"dc616931d7f291806536901a83ee4037\",\"fileName\":\"屏幕截图 2023-11-16 210739.png\",\"fileSize\":14089,\"fileType\":\"image\",\"id\":308,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/dc616931d7f291806536901a83ee4037.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:00:39', 24);
INSERT INTO `sys_oper_log` VALUES (548, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:02:13\",\"enable\":true,\"fileMd5\":\"2bd51e0ec643b207cd40270b6b2b37c8\",\"fileName\":\"屏幕截图 2023-11-09 203431.png\",\"fileSize\":479141,\"fileType\":\"image\",\"id\":309,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/2bd51e0ec643b207cd40270b6b2b37c8.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:02:41', 21);
INSERT INTO `sys_oper_log` VALUES (549, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:07:20\",\"enable\":true,\"fileMd5\":\"ef5f45b70a4d6942ea49dda792bd316c\",\"fileName\":\"屏幕截图 2023-11-09 203211.png\",\"fileSize\":130703,\"fileType\":\"image\",\"id\":311,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/ef5f45b70a4d6942ea49dda792bd316c.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:08:13', 17);
INSERT INTO `sys_oper_log` VALUES (550, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:08:21\",\"enable\":true,\"fileMd5\":\"ef5f45b70a4d6942ea49dda792bd316c\",\"fileName\":\"屏幕截图 2023-11-09 203211.png\",\"fileSize\":130703,\"fileType\":\"image\",\"id\":312,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/ef5f45b70a4d6942ea49dda792bd316c.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:09:05', 19);
INSERT INTO `sys_oper_log` VALUES (551, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:09:32\",\"enable\":true,\"fileMd5\":\"ef5f45b70a4d6942ea49dda792bd316c\",\"fileName\":\"屏幕截图 2023-11-09 203211.png\",\"fileSize\":130703,\"fileType\":\"image\",\"id\":313,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/ef5f45b70a4d6942ea49dda792bd316c.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:10:17', 17);
INSERT INTO `sys_oper_log` VALUES (552, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:10:27\",\"enable\":true,\"fileMd5\":\"ef5f45b70a4d6942ea49dda792bd316c\",\"fileName\":\"屏幕截图 2023-11-09 203211.png\",\"fileSize\":130703,\"fileType\":\"image\",\"id\":314,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/ef5f45b70a4d6942ea49dda792bd316c.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:10:52', 24);
INSERT INTO `sys_oper_log` VALUES (553, '文件管理', 3, 'com.wusuowei.system.controller.SysFileController.delete()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/system/file/', '192.168.131.137', '内网', '{\"sysFile\":{\"bucketName\":\"image\",\"chunkNum\":1,\"chunkSize\":10485760,\"createBy\":\"1\",\"createTime\":\"2024-03-10 20:11:01\",\"enable\":true,\"fileMd5\":\"7ce70f96017028c63309848beaf63fba\",\"fileName\":\"屏幕截图 2023-11-09 203238.png\",\"fileSize\":91774,\"fileType\":\"image\",\"id\":315,\"isDelete\":false,\"url\":\"http://127.0.0.1:9000/image/admin/7ce70f96017028c63309848beaf63fba.png\"}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 20:11:17', 25);
INSERT INTO `sys_oper_log` VALUES (554, '菜单管理`', 1, 'com.wusuowei.system.controller.SysMenuController.add()', 'POST', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu', '192.168.131.137', '内网', '{\"menu\":{\"icon\":\"el-icon-chat-dot-round\",\"menuType\":\"M\",\"name\":\"测试\",\"path\":\"/aa\",\"sortNum\":1}}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 23:20:29', 8);
INSERT INTO `sys_oper_log` VALUES (555, '菜单管理', 3, 'com.wusuowei.system.controller.SysMenuController.deleteBatch()', 'DELETE', 0, 'admin', '超级管理员', 'http://localhost:8160/auth/menu/106', '192.168.131.137', '内网', '{\"menuId\":106}', '{\"msg\":\"success\",\"code\":200}', 0, NULL, '2024-03-10 23:20:32', 20);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_key` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '唯一标识',
  `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin2', '超级管理员', '最高权限', '2023-05-03 18:31:29', '2023-04-20 09:32:12', 0);
INSERT INTO `sys_role` VALUES (2, 'user4', '普通用户', NULL, '2023-05-04 18:31:33', '2024-03-10 15:37:54', 0);
INSERT INTO `sys_role` VALUES (3, 'vip', 'Vip用户', 'sss334', '2023-05-07 18:31:45', '2024-03-10 15:39:40', 0);
INSERT INTO `sys_role` VALUES (10, 'aa', 'aaa', 'aaa', '2023-05-16 14:57:40', '2023-05-16 17:44:14', 1);
INSERT INTO `sys_role` VALUES (11, 'aaa', 'aaa', 'aaaa', '2023-05-16 15:39:18', '2023-05-16 17:44:14', 1);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '角色id',
  `menu_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '菜单id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '角色-菜单-关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '100', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '101', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '102', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '103', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '104', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '105', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '3', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '4', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '47', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '5', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '57', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '58', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '59', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '6', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '61', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '62', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '63', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '64', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '65', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '68', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '69', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '7', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '70', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '71', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '72', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '73', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '74', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '75', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '76', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '77', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '78', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '79', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '80', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '81', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '82', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '83', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '84', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '85', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '86', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '87', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '88', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '89', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '90', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '94', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '95', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '96', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '97', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '98', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('1', '99', '2024-03-10 19:58:22', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '100', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '102', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '103', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '104', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '3', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '4', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '47', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '5', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '57', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '58', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '59', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '6', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '61', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '62', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '63', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '64', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '65', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '68', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '7', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '72', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '76', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '80', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '84', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '94', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '95', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '96', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '97', '2023-05-26 16:48:28', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '98', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('2', '99', '2023-05-26 16:48:29', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '100', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '102', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '103', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '3', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '4', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '47', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '5', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '57', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '58', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '59', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '6', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '61', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '62', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '63', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '64', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '65', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '68', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '7', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '72', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '73', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '76', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '77', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '80', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '81', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '84', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '85', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '86', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '94', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '95', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '96', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '97', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '98', '2023-05-24 17:52:25', NULL);
INSERT INTO `sys_role_menu` VALUES ('3', '99', '2023-05-24 17:52:25', NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(0) NULL DEFAULT 0 COMMENT '性别（0男 1女）',
  `email` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phonenumber` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '地址',
  `unionID` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '微信开放平台unionID',
  `avatar_url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'https://lgy-edu.oss-cn-beijing.aliyuncs.com/wusuowei/image/lgy.jpg' COMMENT '头像',
  `is_delete` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$fn8sAJzvVrF2xv7OQWpcxuz.EmHFTJoQamraD9dxP1ELXng5/E0ee', '无所谓^_^', NULL, '2673152463@qq.com', '18337572656', '浙江省台1州市1', NULL, 'http://47.96.133.5:9000/image/lgy.jpg', 0, '0', '2024-03-10 23:14:16', '2022-01-22 21:10:27', '2023-04-22 00:11:30');
INSERT INTO `sys_user` VALUES (16, 'vip', '$2a$10$Ifx96BpAuDUByb9T.gKUIO0NwqDb/DKpAXBcz3nEcQ34rEzXbowHm', '小黑子', NULL, '2553229176@qq.com', '18337572656', '河南', NULL, NULL, 0, '0', '2023-06-18 16:30:26', '2022-02-26 22:10:14', '2023-05-23 21:53:48');
INSERT INTO `sys_user` VALUES (17, '333', '202cb962ac59075b964b07152d234b70', '我是三三哦豁', NULL, '3', '2673152463', '3', NULL, 'http://localhost:9090/files/20230409082108000000936.jpg', 1, '0', NULL, '2022-02-26 22:10:18', '2023-05-26 16:52:00');
INSERT INTO `sys_user` VALUES (18, 'nzz', '202cb962ac59075b964b07152d234b70', '哪吒', NULL, '2', '2', '2', NULL, '', 1, '0', NULL, '2022-03-29 16:59:44', '2023-05-26 16:52:00');
INSERT INTO `sys_user` VALUES (25, 'sir', '202cb962ac59075b964b07152d234b70', '安琪拉', NULL, NULL, NULL, NULL, NULL, NULL, 1, '0', NULL, '2022-06-08 17:00:47', '2023-05-26 16:52:00');
INSERT INTO `sys_user` VALUES (26, 'err', '202cb962ac59075b964b07152d234b70', '妲己', NULL, '11', '1', '1', NULL, NULL, 1, '0', NULL, '2022-07-08 17:20:01', '2023-05-26 16:51:43');
INSERT INTO `sys_user` VALUES (28, 'ddd', '202cb962ac59075b964b07152d234b70', 'ddd', NULL, '', '', '', NULL, 'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg', 1, '0', NULL, '2022-11-09 10:41:07', '2023-05-26 16:51:49');
INSERT INTO `sys_user` VALUES (29, 'ffff', '202cb962ac59075b964b07152d234b70', 'ffff', NULL, NULL, NULL, NULL, NULL, NULL, 1, '0', NULL, '2022-12-10 11:53:31', '2023-05-26 16:51:49');
INSERT INTO `sys_user` VALUES (36, 'aaa', '47bce5c74f589f4867dbd57e9ca9f808', NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost:9090/files/20230409082108000000936.jpg', 0, '0', NULL, '2023-04-21 22:45:25', '2023-05-18 10:55:13');
INSERT INTO `sys_user` VALUES (37, 'fff', '343d9040a671c45832ee5381860e2996', NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost:9090/files/20230409082108000000936.jpg', 0, '0', NULL, '2023-04-21 23:02:56', '2023-05-18 10:31:02');
INSERT INTO `sys_user` VALUES (39, '2553229176@qq.com', '$2a$10$Ifx96BpAuDUByb9T.gKUIO0NwqDb/DKpAXBcz3nEcQ34rEzXbowHm', '2553229176@qq.com', NULL, NULL, NULL, NULL, NULL, 'https://lgy-edu.oss-cn-beijing.aliyuncs.com/wusuowei/image/lgy.jpg', 0, '0', '2023-05-24 17:13:54', '2023-05-03 19:14:41', '2023-05-03 19:15:17');
INSERT INTO `sys_user` VALUES (41, '无所谓ㅍ_ㅍ', 'o3_SC5-qtX6miSBpx1RmnLWTGDQU', '无所谓ㅍ_ㅍ', 0, NULL, NULL, NULL, 'o3_SC5-qtX6miSBpx1RmnLWTGDQU', 'https://lgy-edu.oss-cn-beijing.aliyuncs.com/wusuowei/image/lgy.jpg', 0, '0', '2023-06-18 16:14:59', '2023-05-03 19:33:36', '2023-05-03 22:41:49');
INSERT INTO `sys_user` VALUES (42, 'wusuowei', '10095008', 'LGY', 0, '2553229176@qq.com', '18337572656', '河南', '10095008', 'https://foruda.gitee.com/avatar/1677168816178868832/10095008_wusupweilgy_1677168816.png', 0, '0', '2023-06-18 16:19:24', '2023-05-03 22:49:37', '2023-05-03 23:11:02');
INSERT INTO `sys_user` VALUES (43, 'LGY', '$2a$10$fd.CFEnZNkaGQkJFFCyMMuPCZ0dFXkDhJjFCUlbk4a3u47s9cFkWC', 'LGY', 0, NULL, NULL, NULL, NULL, 'https://lgy-edu.oss-cn-beijing.aliyuncs.com/wusuowei/image/lgy.jpg', 0, '0', '2023-05-17 10:47:08', '2023-05-11 12:43:42', '2023-05-11 12:43:49');
INSERT INTO `sys_user` VALUES (46, '是强子啊', 'o3_SC5xQxsl3w9vEmHi0UYpw6o1E', '是强子啊', 0, NULL, NULL, NULL, 'o3_SC5xQxsl3w9vEmHi0UYpw6o1E', 'https://thirdwx.qlogo.cn/mmopen/vi_32/xRYuVOluJxeuRtmKPjwwkXFzIhfiaujQZpdLlcicK2ias8VRtia3FBpesticHTFDkcAz2GOowL3Y1hvdmDI2Z2EogPA/132', 0, '0', '2023-05-18 08:49:33', '2023-05-18 08:49:33', NULL);
INSERT INTO `sys_user` VALUES (49, 'wusuowei', '$2a$10$fd.CFEnZNkaGQkJFFCyMMuPCZ0dFXkDhJjFCUlbk4a3u47s9cFkWC', 'wusuowei', 0, NULL, NULL, NULL, NULL, 'https://lgy-edu.oss-cn-beijing.aliyuncs.com/wusuowei/image/lgy.jpg', 0, '0', NULL, '2023-05-22 17:58:48', NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '角色id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '用户-角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '2024-03-10 15:43:15', NULL);
INSERT INTO `sys_user_role` VALUES ('16', '3', '2023-05-23 23:10:17', NULL);
INSERT INTO `sys_user_role` VALUES ('39', '2', '2023-05-03 19:14:41', NULL);
INSERT INTO `sys_user_role` VALUES ('41', '2', '2023-05-03 19:33:36', NULL);
INSERT INTO `sys_user_role` VALUES ('42', '2', '2024-03-10 16:28:28', NULL);
INSERT INTO `sys_user_role` VALUES ('43', '2', '2023-05-11 12:43:42', NULL);
INSERT INTO `sys_user_role` VALUES ('46', '2', '2023-05-18 08:49:33', NULL);
INSERT INTO `sys_user_role` VALUES ('47', '2', '2023-05-22 16:42:40', NULL);
INSERT INTO `sys_user_role` VALUES ('48', '2', '2023-05-22 17:47:48', NULL);
INSERT INTO `sys_user_role` VALUES ('49', '2', '2023-05-22 17:58:48', NULL);
INSERT INTO `sys_user_role` VALUES ('50', '3', '2024-03-10 15:45:23', NULL);

SET FOREIGN_KEY_CHECKS = 1;
