package com.wusuowei.common.web.page;

import com.wusuowei.common.web.domain.BaseEntity;

/**
 * 分页数据
 *
 * @author zsincere
 */
public class ConditionDomain extends BaseEntity
{
    /** 字段所属表 */
    private String tableName;

    /** 属性名称 */
    private String fieldName;

    /** 条件 */
    private String conditionType;

    /** 文本框 */
    private String text;

    /** 属性开始值 */
    private String startValue;

    /** 属性结束值 */
    private String endValue;

    /** 属性类型 */
    private String type;

    /** order */
    private String orderType;

    /** 多选框值 */
    private String[] checkbox;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getConditionType() {
        return conditionType;
    }

    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getStartValue() {
        return startValue;
    }

    public void setStartValue(String startValue) {
        this.startValue = startValue;
    }

    public String getEndValue() {
        return endValue;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(String[] checkbox) {
        this.checkbox = checkbox;
    }


}
