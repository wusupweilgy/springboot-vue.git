package com.wusuowei.common.utils.mybatis;

/**
 * @author liguangyao
 * @date 2023/9/7
 */

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlSelectQueryBlock;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.alibaba.druid.sql.parser.ParserException;
import com.alibaba.druid.sql.parser.SQLParserUtils;
import com.alibaba.druid.sql.parser.SQLStatementParser;
import com.alibaba.druid.util.JdbcConstants;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * druid的sql解析工具类，简单封装
 *
 * @author liguangyao
 * @date 2023/9/7
 */
public class MySqlUtil {

    private static final Logger log = LoggerFactory.getLogger(MySqlUtil.class);

    private static final String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
            + "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";

    /**
     * 表示忽略大小写
     */
    private static final Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);

    /**
     * 获取表名（获取别名，别名获取不到获取表名）
     *
     * @param sql
     * @param databaseTable
     * @return
     */
    public static String getTableName(String sql, String databaseTable) {

        Map<String, String> map = new HashMap<>();
        try {
            Select select = (Select) CCJSqlParserUtil.parse(sql);
            SelectBody selectBody = select.getSelectBody();
            PlainSelect plainSelect = (PlainSelect) selectBody;
            Table table = (Table) plainSelect.getFromItem();
            if (ObjectUtil.isNotNull(table.getAlias())) {
                map.put(table.getName(), table.getAlias().getName());
            }else if(StrUtil.isNotBlank(table.getName())){
                map.put(table.getName(), table.getName());
            }

            List<Join> joins = plainSelect.getJoins();
            if(CollUtil.isEmpty(joins)){
                return map.get(databaseTable);
            }

            for (Join join : joins) {
                Table joinTable = (Table) join.getRightItem();
                if(ObjectUtil.isNull(joinTable)){
                    continue;
                }
                if (joinTable.getAlias() != null) {
                    map.put(joinTable.getName(), joinTable.getAlias().getName());
                }else if(StrUtil.isNotBlank(joinTable.getName())){
                    map.put(joinTable.getName(), joinTable.getName());
                }
            }
        } catch (Exception e) {
            log.error("获取主表名失败：{}",e.getMessage());
            return null;
        }

        return map.get(databaseTable);
    }


    /**
     * 获取主表名
     *
     * @param sql
     * @return
     */
    public static String getOriginalTableName(String sql) {

        Select select = null;
        try {
            select = (Select) CCJSqlParserUtil.parse(sql);
            SelectBody selectBody = select.getSelectBody();

            if (selectBody instanceof PlainSelect) {
                PlainSelect plainSelect = (PlainSelect) selectBody;
                FromItem fromItem = plainSelect.getFromItem();

                if (fromItem != null && fromItem instanceof Table) {
                    Table mainTable = (Table) fromItem;
                    String mainTableName = mainTable.getName();
                    return mainTableName;
                }
            }
            return null;
        } catch (JSQLParserException e) {
            log.error("获取主表名失败：{}",e.getMessage());
            return null;
        }

    }


    /**
     * 根据表名获取表别名，获取不到返回表名
     *
     * @param tableName
     * @param sql
     * @return
     */
    public static String getTableAlias(String tableName, String sql) {
        Map<String,String> map = new HashMap<>();
        try {
            Select select = (Select)CCJSqlParserUtil.parse(sql);
            SelectBody selectBody = select.getSelectBody();
            PlainSelect plainSelect = (PlainSelect)selectBody;
            Table table = (Table)plainSelect.getFromItem();
            if(table.getAlias() != null){
                map.put(table.getName(), table.getAlias().getName());
            }

            for(Join join : plainSelect.getJoins()){
                Table table1 = (Table)join.getRightItem();
                if(table1.getAlias()!=null){
                    map.put(table1.getName(), table1.getAlias().getName());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ObjectUtil.isNotEmpty(map.get(tableName)) ? map.get(tableName) : tableName;
    }
    /**
     * 判断sql是否正确
     *
     * @param sql
     * @return
     */
    public static boolean isSqlValid(String sql) {
        try {
            SQLStatementParser parser = SQLParserUtils.createSQLStatementParser(sql, JdbcConstants.MYSQL);
            parser.parseStatement();
            return true;
        } catch (ParserException e) {
            // 解析异常，说明SQL语句不正确
            return false;
        }
    }


    static class CustomerMySqlASTVisitorAdapter extends MySqlASTVisitorAdapter {

        /**
         * 主表别名
         */
        private String tableAlias;
        /**
         * 主表名
         */
        private String tableName;


        @Override
        public boolean visit(SQLExprTableSource x) {
            // 获取到主表就不再继续查询表名
            if (ObjectUtil.isNotEmpty(tableAlias) || ObjectUtil.isNotEmpty(tableName)) {
                return false;
            }
            tableAlias = x.getAlias();
            tableName = x.getTableName();
            return true;
        }

        @Override
        public boolean visit(MySqlSelectQueryBlock x) {
            return true;
        }

        public String getTableName() {
            if (ObjectUtil.isNotEmpty(tableAlias)) {
                return tableAlias;
            }
            return tableName;
        }

        public String getOriginalTableName() {
            return tableName;
        }
    }

    /**
     * sql注入校验
     *
     * @param str ep: "or 1=1"
     */
    public static boolean sqlInjectionVerification(String str) {
        if(StrUtil.isBlank(str)){
            return false;
        }
        Matcher matcher = sqlPattern.matcher(str);
        if (matcher.find()) {
            //获取非法字符：or
            log.info("参数存在非法字符，请确认：" + matcher.group());
            return true;
        }
        return false;
    }
}
