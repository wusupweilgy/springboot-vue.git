package com.wusuowei.common.web.page;

import java.util.List;
import java.util.Map;

/**
 * 分页数据
 *
 * @author lishuzhen
 * @createTime 2023年10月19日 13:35:00
 */
public class PageData<T> {
    /**
     * 当前页
     */

    private Integer pageIndex;
    /**
     * 总记录数
     */
    private Long total;

    private Long totalPage;
    /**
     * 数据
     */
    private List<T> list;

    /**
     * 最后一条记录的排序ID
     */
    private String lastSortValues;
    /**
     * 其他数据
     */
    private Map<String, Object> extData;

    public PageData() {
    }

    public PageData(Integer pageIndex, Long total, List<T> list) {
        this.pageIndex = pageIndex;
        this.total = total;
        this.list = list;
    }

    public PageData(Integer pageIndex, Long total, List<T> list, Map<String, Object> extData) {
        this.pageIndex = pageIndex;
        this.total = total;
        this.list = list;
        this.extData = extData;
    }

    public PageData(Integer pageIndex, Integer pageSize, Long total, List<T> list) {
        this.pageIndex = pageIndex;
        this.total = total;
        this.totalPage = (total + pageSize - 1) / pageSize;
        this.list = list;
    }

    public static <T> PageData<T> noData() {
        return new PageData<>(0, 0L, null);
    }

    public static <T> PageData<T> data(List<T> data) {
        return new PageData<>(0, data == null ? 0L : data.size(), data);
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Long getTotal() {
        return total == null ? 0L : total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Map<String, Object> getExtData() {
        return extData;
    }

    public void setExtData(Map<String, Object> extData) {
        this.extData = extData;
    }

    public String getLastSortValues() {
        return lastSortValues;
    }

    public void setLastSortValues(String lastSortValues) {
        this.lastSortValues = lastSortValues;
    }

    public Long getTotalPage() {
        return totalPage == null ? 0L : totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public String toString() {
        return "PageData{" +
                "pageIndex=" + pageIndex +
                ", total=" + total +
                ", totalPage=" + totalPage +
                ", list.size=" + (list == null ? 0 : list.size()) +
                ", lastSortValues='" + lastSortValues + '\'' +
                ", extData=" + extData +
                '}';
    }
}