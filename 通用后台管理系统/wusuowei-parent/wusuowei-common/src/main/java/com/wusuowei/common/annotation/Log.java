package com.wusuowei.common.annotation;


import com.wusuowei.common.constant.BusinessTypeEnum;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 * @author wangzhenjun
 * @date 2022/10/26 15:37
 */
@Target(ElementType.METHOD) // 注解只能用于方法
@Retention(RetentionPolicy.RUNTIME) // 修饰注解的生命周期
@Documented
public @interface Log {

    String value() default "";
    /**
     * 模块
     */
    String title() default "测试模块";

    /**
     * 功能
     */
    BusinessTypeEnum businessType() default BusinessTypeEnum.OTHER;
}
