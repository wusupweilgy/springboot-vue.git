package com.wusuowei.common.model.po;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author LGY
 */
@Data
public class ComRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String roleKey;

    private String name;

}
