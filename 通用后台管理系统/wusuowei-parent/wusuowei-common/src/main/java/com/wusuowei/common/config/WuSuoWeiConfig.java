package com.wusuowei.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 * 
 * @author ruoyi
 */
@Data
@RefreshScope
@Component
public class WuSuoWeiConfig
{
    /*refresh-expire续期过期时间*/
    @Value("${jwt.refreshtoken-expire}")
    private Integer refreshTokenexpire;

    /*access_token过期时间*/
    @Value("${jwt.expire}")
    private Integer expire;

}
