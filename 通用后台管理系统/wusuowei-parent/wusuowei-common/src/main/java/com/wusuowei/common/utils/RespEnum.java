package com.wusuowei.common.utils;

public enum RespEnum {


    //ucenter
    UNAUTHENTICATEDERROR(401, "你没有认证"),
    UNAUTHORIZEDEDERROR(401, "你没有权限访问"),
    AUTHENTICATIONERROR(401,"身份验证失败"),

    //auth
    CODE_IS_EMPTY(8001,"验证码为空"),
    CODE_ERROR(8002,"验证码错误"),
    USER_NOT_EXISTS(8003,"账号不存在"),
    ACCOUNT_OR_PASSWORD_ERROR(8004,"账号或密码错误"),

    //system
    UPLOADSUCCESSFUL(1, "上传成功"),
    UPLOADING(2, "上传中"),
    NOT_UPLOADED(3, "未上传"),
    ACCESS_PARAMETER_INVALID(1001,"访问参数无效"),
    UPLOAD_FILE_FAILED(1002,"文件上传失败"),
    DATA_NOT_EXISTS(1003,"数据不存在"),

    SUBMENU_EXISTS(1010,"存在子菜单,不允许删除"),
    MENU_ASSIGNED(1011,"菜单已分配,不允许删除"),

    //gateway
    TOKEN_EXPIRATION(4001,"token过期，请重新登陆"),
    TOKEN_AUTHENTICATION_FAILURE(4001,"token认证失效，token错误或者过期，请重新登陆"),
    TOKEN_ILLEGAL(4001,"token非法，不是规范的token，可能被篡改了"),
    TOKEN_NOT_EXIST(4001,"token不存在"),
    MULTIPLE_BROWSER_LOGINS(4001,"多人登录或强退，请重新登录"),
    REFRESH_TOKEN_EXPIRATION(4001,"刷新令牌过期，请重新登录"),
    ;

    private final Integer code;
    private final String message;

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    RespEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
