package com.wusuowei.common.web.page;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;

import com.wusuowei.common.utils.ServletUtils;
import com.wusuowei.common.web.domain.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;

/**
 * 表格数据处理
 * 
 * @author zsincere
 */
public class TableSupport
{
    private static final Logger log = LoggerFactory.getLogger(TableSupport.class);
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 条件查询和筛选条件
     */
    public static final String PARAMS_PLUS = "paramsPlus";

    /**
     * 数据库表名
     */
    public static final String DATABASE_TABLE = "databaseTable";

    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain()
    {
        ServletRequestAttributes servletRequestAttributes =  (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        Integer pageNum = null,pageSize = null;
        if(HttpMethod.GET.name().equals(request.getMethod())){
            pageNum = Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1);
            pageSize = Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10);
        }else if(HttpMethod.POST.name().equals(request.getMethod())){
            BaseEntity baseEntity = getParamsPlusFromRequest(request);
            pageNum = ObjectUtil.isNull(baseEntity) ? 1 : Integer.valueOf(baseEntity.getPageNum());
            pageSize = ObjectUtil.isNull(baseEntity) ? 10 : Integer.valueOf(baseEntity.getPageSize());
        }
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(pageNum);
        pageDomain.setPageSize(pageSize);
        pageDomain.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(IS_ASC));
        pageDomain.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return pageDomain;
    }

    public static PageDomain buildPageRequest()
    {
        return getPageDomain();
    }

    private static BaseEntity getParamsPlusFromRequest(HttpServletRequest request) {
        BaseEntity result = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = request.getReader();) {
            char[] buff = new char[1024];
            int len;
            while ((len = reader.read(buff)) != -1) {
                sb.append(buff, 0, len);
            }
            if(StrUtil.isBlank(sb)){
                return null;
            }
            // 判断是否是分页请求
            if(!sb.toString().contains(TableSupport.PAGE_NUM) && !sb.toString().contains(TableSupport.PAGE_SIZE)){
                return null;
            }
            result = JSON.parseObject(sb.toString(), BaseEntity.class);
        } catch (Exception e) {
            log.error("分页参数JSON转换异常：{}", e);
        }
        if(ObjectUtil.isNull(result)){
            return null;
        }
        return result;
    }
}
