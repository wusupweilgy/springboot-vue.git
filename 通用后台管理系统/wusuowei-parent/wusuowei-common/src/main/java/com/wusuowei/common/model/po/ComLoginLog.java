package com.wusuowei.common.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统访问记录
 * </p>
 *
 * @author LGY
 */
@Data
public class ComLoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

   // @ApiModelProperty(value = "访问ID")
    private Long id;

 //   @ApiModelProperty(value = "用户账号")
    private String userName;

   // @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;

  //  @ApiModelProperty(value = "登录地点")
    private String loginLocation;

 //   @ApiModelProperty(value = "浏览器类型")
    @TableField("browser")
    private String browser;

  //  @ApiModelProperty(value = "操作系统")
    @TableField("os")
    private String os;

  //  @ApiModelProperty(value = "登录状态（0成功 1失败）")
    @TableField("status")
    private String status;

  //  @ApiModelProperty(value = "提示消息")
    @TableField("msg")
    private String msg;

   // @ApiModelProperty(value = "访问时间")
    @TableField("login_time")
    private LocalDateTime loginTime;

    @TableField(exist = false)
    private String uid;
}
