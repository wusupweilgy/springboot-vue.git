package com.wusuowei.common.utils;

import com.alibaba.fastjson2.JSON;
import com.wusuowei.common.model.po.ComUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUserUtil {

    public static ComUser getSecurityUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userJson = (String) authentication.getPrincipal();
        return JSON.parseObject(userJson, ComUser.class);
    }

}
