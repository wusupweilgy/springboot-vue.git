package com.wusuowei.common.filter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * post请求过滤器（解决post请求体只能读取一次问题）
 *
 * @author liguangyao
 * @createTime 2024/01/25
 */
@Component
@WebFilter(urlPatterns = {"/*"})
public class PostRequestFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(PostRequestFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
//        String token = request.getHeader("token");
//        response.setHeader("token", token);
        String contentType = servletRequest.getContentType();
        String method = "multipart/form-data";

        if (contentType != null && contentType.contains(method)) {
            // 通过spring的轮子，实现request的转换，
            MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
            MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
            // 将转化后的 request 放入过滤链中
            request = multipartRequest;

        }

        ServletRequest requestWrapper = new BodyReaderHttpServletRequestWrapper(request);

        filterChain.doFilter(requestWrapper, servletResponse);

    }

    @Override
    public void destroy() {

    }
}