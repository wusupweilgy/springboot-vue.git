package com.wusuowei.common.constant;

/**
 * 缓存的key 常量
 * 
 * @author ruoyi
 */
public class CacheConstants
{
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_online:";

    /**
     * 用户访问令牌 redis key
     */
    public static final String ACCESS_TOKEN_KEY = "access_token:";

    /**
     * 用户刷新令牌 redis key
     */
    public static final String REFRESH_TOKEN_KEY = "refresh_token";


    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";
}
