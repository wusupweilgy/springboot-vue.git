package com.wusuowei.common.utils.mybatis;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.StrUtil;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author zhuguoqing
 * @description 用于Mybatis中varchar字段向list类型转换
 * @date 2023/11/16
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes({List.class})
public class Varchar2ListTypeHandler implements TypeHandler<List<String>> {

    /**
     * 逗号
     */
    public static final String COMMA = StrPool.COMMA;

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, List<String> integerList, JdbcType jdbcType) throws SQLException {
        if (CollUtil.isEmpty(integerList)) {
            preparedStatement.setString(i, null);
            return;
        }
        String items = StrUtil.join(COMMA, integerList);
        preparedStatement.setString(i, items);
    }

    @Override
    public List<String> getResult(ResultSet resultSet, String i) throws SQLException {
        String str = resultSet.getString(i);
        if (StrUtil.isBlank(str)) {
            return null;
        }
        return CollUtil.newArrayList(str.split(COMMA));
    }

    @Override
    public List<String> getResult(ResultSet resultSet, int i) throws SQLException {
        String str = resultSet.getString(i);
        if (StrUtil.isBlank(str)) {
            return null;
        }
        return CollUtil.newArrayList(str.split(COMMA));
    }

    @Override
    public List<String> getResult(CallableStatement callableStatement, int i) throws SQLException {
        String str = callableStatement.getString(i);
        if (StrUtil.isBlank(str)) {
            return null;
        }
        return CollUtil.newArrayList(str.split(COMMA));
    }
}

