package com.wusuowei.common.model.dto;

import lombok.Data;

@Data
public class ClientMsg {
    private String clientType;
    private String systemType;
    private String browserType;
    private String ip;
    private String address;
}
