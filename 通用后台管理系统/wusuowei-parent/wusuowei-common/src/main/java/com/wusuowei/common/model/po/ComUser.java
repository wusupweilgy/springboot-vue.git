package com.wusuowei.common.model.po;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author LGY
 */
@Data
public class ComUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String username;

    //@JsonIgnore
    private String password;

    private String nickname;

    private Integer sex;

    private String email;

    private String phonenumber;

    private String address;

    private String unionid;

    private String avatarUrl;

    private Boolean isDelete;

    private String status;

    private LocalDateTime loginTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private List<ComRole> role;

    private List<Integer> roleid;

}
