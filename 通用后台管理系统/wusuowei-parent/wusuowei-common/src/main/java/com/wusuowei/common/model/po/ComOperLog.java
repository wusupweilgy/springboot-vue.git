package com.wusuowei.common.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 操作日志记录
 * </p>
 *
 * @author LGY
 */
@Data
public class ComOperLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    //@ApiModelProperty(value = "模块标题")
    private String title;

    //@ApiModelProperty(value = "业务类型（0其它 1新增 2修改 3删除）")
    private Integer businessType;

   // @ApiModelProperty(value = "方法名称")
    private String method;

   // @ApiModelProperty(value = "请求方式")
    private String requestMethod;

    //@ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;

   // @ApiModelProperty(value = "操作人员")
    private String operName;

   // @ApiModelProperty(value = "部门名称")
    private String deptName;

   // @ApiModelProperty(value = "请求URL")
    private String operUrl;

    //@ApiModelProperty(value = "主机地址")
    private String operIp;

   // @ApiModelProperty(value = "操作地点")
    private String operLocation;

    //@ApiModelProperty(value = "请求参数")
    private String operParam;

    //@ApiModelProperty(value = "返回参数")
    private String jsonResult;

    //@ApiModelProperty(value = "操作状态（0正常 1异常）")
    private Integer status;

    //@ApiModelProperty(value = "错误消息")
    private String errorMsg;

   // @ApiModelProperty(value = "操作时间")
    private LocalDateTime operTime;

    //@ApiModelProperty(value = "消耗时间")
    private Long costTime;


}
