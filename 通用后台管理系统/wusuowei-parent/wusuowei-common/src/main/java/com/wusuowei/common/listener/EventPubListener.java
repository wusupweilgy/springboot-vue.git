package com.wusuowei.common.listener;

import com.wusuowei.common.model.po.ComOperLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author wangzhenjun
 * @date 2022/10/26 16:38
 */
@Component
public class EventPubListener {
    @Autowired
    private ApplicationContext applicationContext;

    // 事件发布方法
    public void pushListener(ComOperLog sysLogEvent) {
        applicationContext.publishEvent(sysLogEvent);
    }
}
