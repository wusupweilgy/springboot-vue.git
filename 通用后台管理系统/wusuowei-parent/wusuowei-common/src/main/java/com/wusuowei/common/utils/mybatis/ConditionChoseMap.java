package com.wusuowei.common.utils.mybatis;

import cn.hutool.core.util.ObjectUtil;
import com.wusuowei.common.utils.StringUtils;
import com.wusuowei.common.web.page.ConditionDomain;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 存储所有筛选条件
 *
 * @author liguangyao
 * @date 2023/9/6
 */
public class ConditionChoseMap {

    private static final Map<String, Map<String, String>> CONDITION_CHOSE_MAP = new ConcurrentHashMap<>();

    // 提前创建好所有条件筛选策略类，缓存到 Map 中
    static {
        Map<String, String> textConditionChose = new HashMap<>();
        Map<String, String> numberConditionChose = new HashMap<>();
        Map<String, String> dateConditionChose = new HashMap<>();
        Map<String, String> checkboxConditionChose = new HashMap<>();

        textConditionChose.put("eq", " = '#' "); // 等于
        textConditionChose.put("ne", " <> '#' "); // 不等于
        textConditionChose.put("contain", " LIKE '%#%' "); // 包含
        textConditionChose.put("excluding", " NOT LIKE '%#%' "); // 不包含
        textConditionChose.put("startis", " LIKE '#%' "); // 开始是
        textConditionChose.put("startisnot", " NOT LIKE '#%' "); // 开始不是
        textConditionChose.put("endis", " LIKE '%#' "); // 结束是
        textConditionChose.put("endisnot", " NOT LIKE '%#' "); // 结束不是

        numberConditionChose.put("eq", " = # "); // 等于
        numberConditionChose.put("ne", " <> # "); // 不等于
        numberConditionChose.put("ge", " >= # "); // 大于等于
        numberConditionChose.put("le", " <= # "); // 小于等于
        numberConditionChose.put("gt", " > # "); // 大于
        numberConditionChose.put("lt", " < # "); // 小于
        numberConditionChose.put("bw", " BETWEEN # AND # "); // 数字区间

        dateConditionChose.put("bw", " BETWEEN '# 00:00:00' AND '# 23:59:59' "); // 时间区间
        dateConditionChose.put("after", " >= CURDATE() "); // 晚于（包含当天）
        dateConditionChose.put("before", " <= CURDATE() + INTERVAL 1 DAY "); // 早于（包含当天）
        dateConditionChose.put("today", " BETWEEN CONCAT(CURDATE(), ' 00:00:00') AND CONCAT(CURDATE(), ' 23:59:59') "); // 今天
        dateConditionChose.put("yesterday", " BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND DATE_SUB(CURDATE(), INTERVAL 1 DAY) "); // 昨天
        dateConditionChose.put("tomorrow", " BETWEEN CURDATE() + INTERVAL 1 DAY AND CURDATE() + INTERVAL 2 DAY "); // 明天
        dateConditionChose.put("thisweek", " BETWEEN CURDATE() - INTERVAL WEEKDAY(CURDATE()) DAY AND CURDATE() + INTERVAL (7 - WEEKDAY(CURDATE())) DAY "); // 本周
        dateConditionChose.put("thismonth", " BETWEEN DATE_FORMAT(CURDATE(), '%Y-%m-01') AND  DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 1 MONTH), '%Y-%m-01') "); // 本月

        checkboxConditionChose.put("in", " in(#) "); // 时间区间

        CONDITION_CHOSE_MAP.put("text", textConditionChose);
        CONDITION_CHOSE_MAP.put("number", numberConditionChose);
        CONDITION_CHOSE_MAP.put("date", dateConditionChose);
        CONDITION_CHOSE_MAP.put("checkbox", checkboxConditionChose);
    }

    /**
     * 获取筛选条件 （name = '李四' 的格式）
     *
     * @param conditionDomain
     * @return
     */
    public static String getCodition(ConditionDomain conditionDomain) {
        String type = conditionDomain.getType();
        // 判断类型是否为空
        if (type == null || type.isEmpty()) {
            return null;
        }
        // 判断条件是否为空
        String condition = conditionDomain.getConditionType();
        if (condition == null || condition.isEmpty()) {
            return null;
        }
        // 判断值是否为空
        if (valueIsNull(conditionDomain)) {
            return null;
        }

        // 获取sql拼接条件
        String reslut = null;
        switch (type) {
            case "text":
                reslut = singleValue(conditionDomain);
                break;
            case "number":
                reslut = multivalue(conditionDomain);
                break;
            case "date":
                reslut = multiDatevalue(conditionDomain);
                break;
            case "checkbox":
                conditionDomain.setText( "'" + String.join("','", conditionDomain.getCheckbox()) + "'");
                reslut = singleValue(conditionDomain);
                break;
            // 添加更多的case分支
            default:
                reslut = null;
                break;
        }
        return reslut;

    }


    /**
     * 判断有条件但是值为空的情况
     *
     * @param conditionDomain
     * @return
     */
    private static boolean valueIsNull(ConditionDomain conditionDomain) {
        switch (conditionDomain.getType()) {
            case "text":
                return ObjectUtil.isEmpty(conditionDomain.getText());
            case "number":
                // 开始结束值都为空的情况
                return (ObjectUtil.isEmpty(conditionDomain.getStartValue()) && ObjectUtil.isEmpty(conditionDomain.getEndValue()));
            case "date":
                // 条件是区间，并且开始结束值都为空的情况
                return ("bw".equals(conditionDomain.getConditionType()) && ObjectUtil.isEmpty(conditionDomain.getStartValue()) && ObjectUtil.isEmpty(conditionDomain.getEndValue()));
            case "checkbox":
                return ObjectUtil.isEmpty(conditionDomain.getCheckbox());
            default:
                return true;
        }
    }

    /**
     * 单值和复选框进行拼接
     *
     * @param conditionDomain
     * @return
     */
    private static String singleValue(ConditionDomain conditionDomain) {
        String s = CONDITION_CHOSE_MAP.get(conditionDomain.getType()).get(conditionDomain.getConditionType());
        return s.replaceAll("#", conditionDomain.getText());
    }


    /**
     * 多值进行拼接
     *
     * @param conditionDomain
     * @return
     */
    private static String multivalue(ConditionDomain conditionDomain) {
        // 多个井号，多个值
        String s = CONDITION_CHOSE_MAP.get(conditionDomain.getType()).get(conditionDomain.getConditionType());

        // 默认多值进行替换，如果不是多值，用开始值进行替换
        if (ObjectUtil.isNotEmpty(conditionDomain.getStartValue()) && ObjectUtil.isNotEmpty(conditionDomain.getEndValue())) {
            String s1 = s.replaceFirst("#", String.valueOf(conditionDomain.getStartValue()));
            return s1.replaceFirst("#", String.valueOf(conditionDomain.getEndValue()));
        } else if(ObjectUtil.isNotEmpty(conditionDomain.getStartValue())){
            return s.replaceAll("#", conditionDomain.getStartValue());
        }else{
            return s.replaceAll("#", conditionDomain.getEndValue());
        }
    }
    private static String multiDatevalue(ConditionDomain conditionDomain) {
        // 多个井号，多个值
        String s = CONDITION_CHOSE_MAP.get(conditionDomain.getType()).get(conditionDomain.getConditionType());
        if("bw".equals(conditionDomain.getConditionType())){
            String s1 = s.replaceFirst("#", String.valueOf(conditionDomain.getStartValue()));
            return s1.replaceFirst("#", String.valueOf(conditionDomain.getEndValue()));
        }
        return s.replaceAll("#", StringUtils.toUnderScoreCase(conditionDomain.getFieldName()));
    }


}
