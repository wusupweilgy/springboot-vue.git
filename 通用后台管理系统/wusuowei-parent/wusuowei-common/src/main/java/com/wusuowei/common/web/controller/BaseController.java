package com.wusuowei.common.web.controller;

import com.alibaba.fastjson2.util.DateUtils;
import com.github.pagehelper.PageInfo;
import com.wusuowei.common.constant.HttpStatus;
import com.wusuowei.common.utils.PageUtils;
import com.wusuowei.common.web.page.PageData;
import com.wusuowei.common.web.page.PageDomain;
import com.wusuowei.common.web.page.TableDataInfo;
import com.wusuowei.common.web.page.TableSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 * 
 * @author zsincere
 */
public class BaseController
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
        PageUtils.startPage();
    }

    /**
     * 清理分页的线程变量
     */
    protected void clearPage()
    {
        PageUtils.clearPage();
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setRows(list);
        rspData.setMsg("查询成功");
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected PageData getPageData(List<?> list)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        PageData rspData = new PageData();
        rspData.setPageIndex(pageDomain.getPageNum());
        rspData.setList(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

}
