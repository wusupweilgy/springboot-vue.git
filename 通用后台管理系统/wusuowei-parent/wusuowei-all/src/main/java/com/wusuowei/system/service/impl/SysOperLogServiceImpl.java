package com.wusuowei.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysOperLogMapper;
import com.wusuowei.system.model.po.SysOperLog;
import com.wusuowei.system.model.vo.PageOperLogVo;
import com.wusuowei.system.service.ISysOperLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements ISysOperLogService {
    @Autowired
    private SysOperLogMapper operLogMapper;

    @Override
    public int deleteAll() {
        return operLogMapper.deleteAll();
    }

    @Override
    public Page<SysOperLog> findPage(PageOperLogVo pagequery) {
        LambdaQueryWrapper<SysOperLog> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.orderByAsc(SysOperLog::getId);
        //系统模块
        queryWrapper.like(StringUtils.isNotBlank(pagequery.getTitle()),SysOperLog::getTitle,pagequery.getTitle());
        //操作人员
        queryWrapper.like(StringUtils.isNotBlank(pagequery.getOperName()),SysOperLog::getOperName,pagequery.getOperName());
        //操作类型
        queryWrapper.eq(StringUtils.isNotBlank(pagequery.getBusinessType()),SysOperLog::getBusinessType,pagequery.getBusinessType());
        //状态
        queryWrapper.eq(StringUtils.isNotBlank(pagequery.getStatus()),SysOperLog::getStatus,pagequery.getStatus());
        //时间
        queryWrapper.ge(StringUtils.isNotBlank(pagequery.getStartTime()),SysOperLog::getOperTime,pagequery.getStartTime());
        queryWrapper.le(StringUtils.isNotBlank(pagequery.getEndTime()),SysOperLog::getOperTime,pagequery.getEndTime());
        queryWrapper.orderByDesc(SysOperLog::getId);
        Page<SysOperLog> page = new Page<>(pagequery.getPageNum(), pagequery.getPageSize());
        page.addOrder(OrderItem.desc("id"));
        return operLogMapper.selectPage(page, queryWrapper);
    }
}
