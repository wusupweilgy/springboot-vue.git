package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysRoleMenu;

/**
 * <p>
 * 角色-菜单-关联表 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-17
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
