package com.wusuowei.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysHeaderFilter;
import org.springframework.stereotype.Repository;

/**
 * 用户默认头筛选Mapper接口
 * 
 * @author zsincere
 * @date 2023-09-16
 */
@Repository
public interface SysHeaderFilterMapper extends BaseMapper<SysHeaderFilter>
{
}
