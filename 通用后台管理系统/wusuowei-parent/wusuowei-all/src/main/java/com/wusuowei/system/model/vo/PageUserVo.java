package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageUserVo {
    private Integer pageNum;
    private Integer pageSize;
    private String userName;
    private String email;
    private String startTime;
    private String endTime;
}
