package com.wusuowei.checkcode.service;

import com.wusuowei.checkcode.model.CheckCodeParamsDto;
import com.wusuowei.checkcode.model.CheckCodeResultDto;


/**
 * @description 验证码接口
 * @author LGY
 * @date 2023/04/04 13:27
 * @version 1.0.0
 */
public interface CheckCodeService {


    /**
     * @description 生成验证码
     * @param checkCodeParamsDto 校验码参数dto
     * @return {@link CheckCodeResultDto }
     * @author LGY
     * @date 2023/04/04 13:28
     */
    CheckCodeResultDto generate(CheckCodeParamsDto checkCodeParamsDto);


    /**
     * @description 校验
     * @param key key
     * @param code 验证码
     * @return boolean
     * @author LGY
     * @date 2023/04/04 13:28
     */
    public boolean verify(String key, String code);


    /**
     * @description 验证码生成器
     * @author LGY
     * @date 2023/04/04 13:30
     * @version 1.0.0
     */
    public interface CheckCodeGenerator{
        /**
         * 验证码生成
         * @return 验证码
         */
        String generate(int length);
        

    }


    /**
     * @description key生成器
     * @author LGY
     * @date 2023/04/04 13:30
     * @version 1.0.0
     */
    public interface KeyGenerator{

        /**
         * key生成
         * @return 验证码
         */
        String generate(String prefix);
    }


    /**
     * @description 验证码存储
     * @author LGY
     * @date 2023/04/04 13:30
     * @version 1.0.0
     */
    public interface CheckCodeStore{


        /**
         * @description 向缓存设置key
         * @param key
         * @param value
         * @param expire 过期时间
         * @author LGY
         * @date 2023/04/04 13:29
         */
        void set(String key, String value, Integer expire);

        String get(String key);

        void remove(String key);
    }
}
