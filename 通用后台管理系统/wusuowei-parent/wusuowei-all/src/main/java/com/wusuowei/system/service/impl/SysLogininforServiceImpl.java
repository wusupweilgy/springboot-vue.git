package com.wusuowei.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysLogininforMapper;
import com.wusuowei.system.model.po.SysLoginLog;
import com.wusuowei.system.model.vo.PageLogininforVo;
import com.wusuowei.system.service.ISysLogininforService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统访问记录 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysLogininforServiceImpl extends ServiceImpl<SysLogininforMapper, SysLoginLog> implements ISysLogininforService {

    @Autowired
    private SysLogininforMapper sysLogininforMapper;
    @Override
    public int deleteAll() {
        return sysLogininforMapper.deleteAll();
    }

    @Override
    public Page<SysLoginLog> findPage(PageLogininforVo pagequery) {
        LambdaQueryWrapper<SysLoginLog> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.orderByAsc(SysLoginLog::getId);
        //系统模块
        queryWrapper.like(StringUtils.isNotBlank(pagequery.getLocaltion()), SysLoginLog::getLoginLocation,pagequery.getLocaltion());
        //操作人员
        queryWrapper.like(StringUtils.isNotBlank(pagequery.getUserName()), SysLoginLog::getUserName,pagequery.getUserName());
        //操作类型
        queryWrapper.eq(StringUtils.isNotBlank(pagequery.getStatus()), SysLoginLog::getStatus,pagequery.getStatus());
        //状态
        queryWrapper.eq(StringUtils.isNotBlank(pagequery.getStatus()), SysLoginLog::getStatus,pagequery.getStatus());
        //时间
        queryWrapper.ge(StringUtils.isNotBlank(pagequery.getStartTime()), SysLoginLog::getLoginTime,pagequery.getStartTime());
        queryWrapper.le(StringUtils.isNotBlank(pagequery.getEndTime()), SysLoginLog::getLoginTime,pagequery.getEndTime());
        Page<SysLoginLog> page = new Page<>(pagequery.getPageNum(), pagequery.getPageSize());
        page.addOrder(OrderItem.desc("id"));

        return sysLogininforMapper.selectPage(page, queryWrapper);
    }
}
