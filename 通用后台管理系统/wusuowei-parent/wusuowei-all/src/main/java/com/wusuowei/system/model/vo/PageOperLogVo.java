package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageOperLogVo {

    private Integer pageNum;
    private Integer pageSize;
    private String title;
    private String operName;
    private String businessType;
    private String status;
    private String startTime;
    private String endTime;
}
