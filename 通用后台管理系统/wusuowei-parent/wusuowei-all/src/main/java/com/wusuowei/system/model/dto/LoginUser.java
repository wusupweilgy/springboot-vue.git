package com.wusuowei.system.model.dto;


import com.wusuowei.auth.model.dto.UserExt;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUser implements UserDetails {

    private Collection<GrantedAuthority> authorities;
    private UserExt userExt;


    public LoginUser() {
    }

    public LoginUser(UserExt user){
        this.userExt = user;
    }

    public LoginUser(Collection<GrantedAuthority> authorities, UserExt user) {
        this.authorities = authorities;
        this.userExt = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return userExt.getPassword();
    }

    @Override
    public String getUsername() {
        return userExt.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
