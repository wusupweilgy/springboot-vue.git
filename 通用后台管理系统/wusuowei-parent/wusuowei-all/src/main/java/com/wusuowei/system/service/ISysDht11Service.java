package com.wusuowei.system.service;

import com.wusuowei.system.model.po.SysDht11;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-06-18
 */
public interface ISysDht11Service extends IService<SysDht11> {

}
