package com.wusuowei.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysMenu;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    @Select("SELECT * FROM sys_menu WHERE id in (SELECT menu_id FROM sys_role_menu WHERE role_id in(SELECT role_id FROM sys_user_role where user_id = #{id})) ORDER BY sort_num ASC")
    List<SysMenu> getUserMenus(String id);

    @Select("SELECT * FROM sys_menu WHERE id in(SELECT menu_id from sys_role_menu WHERE role_id IN (SELECT role_id FROM sys_user_role WHERE user_id = #{uid}))")
    List<SysMenu> getMenusByUid(String id);

    @Select("SELECT permission FROM sys_menu WHERE id IN (SELECT menu_id FROM sys_role_menu WHERE role_id in (SELECT role_id FROM sys_user_role WHERE user_id = #{uid}))")
    List<String> getPermission(String uid);
}
