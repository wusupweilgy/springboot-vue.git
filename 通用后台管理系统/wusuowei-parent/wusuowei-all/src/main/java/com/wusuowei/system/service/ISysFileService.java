package com.wusuowei.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysFile;
import com.wusuowei.system.model.vo.PageFileVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-27
 */
public interface ISysFileService extends IService<SysFile> {
    public List<SysFile> selectSysFileList(SysFile sysFile);
}
