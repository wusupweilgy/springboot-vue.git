package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_user")
public class SysUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("username")
    private String username;

    @JsonIgnore
    @TableField("password")
    private String password;

    @TableField("nickname")
    private String nickname;

    @TableField("sex")
    private Integer sex;

    @TableField("email")
    private String email;

    @TableField("phonenumber")
    private String phonenumber;

    @TableField("address")
    private String address;

    @TableField("unionID")
    private String unionid;

    @TableField("avatar_url")
    private String avatarUrl;

    @TableLogic(value = "0",delval = "1")
    @TableField("is_delete")
    private Boolean isDelete;

    @TableField("status")
    private String status;

    @TableField("login_time")
    private LocalDateTime loginTime;

    @TableField(exist = false)
    private List<SysRole> role;

    @TableField(exist = false)
    private List<Integer> roleid;

}
