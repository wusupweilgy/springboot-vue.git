package com.wusuowei.generator.common.properties;//package com.wusuowei.generator.common.properties;
//
//import lombok.Data;
//import lombok.ToString;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
///**
// * @author kdyzm
// */
//
//@Data
//@ToString
//@Component
//public class DbProperties {
//
//    @Value("${mysql.username}")
//    private String userName;
//
//    @Value("${mysql.password}")
//    private String password;
//
//    @Value("${mysql.connectionUrl}")
//    private String connectionUrl;
//
//    @Value("${mysql.driverClass}")
//    private String driverClassName;
//}
