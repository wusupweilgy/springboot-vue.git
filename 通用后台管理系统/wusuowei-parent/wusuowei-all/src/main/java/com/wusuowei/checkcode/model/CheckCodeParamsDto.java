package com.wusuowei.checkcode.model;

import lombok.Data;


/**
 * @description 验证码生成参数类
 * @author LGY
 * @date 2023/04/04 14:29
 * @version 1.0.0
 */
@Data
public class CheckCodeParamsDto {

    /**
     * 验证码类型:pic、sms、email等
     */
    private String checkCodeType;

    /**
     * 业务携带参数
     */
    private String param1;
    private String param2;
    private String param3;
}
