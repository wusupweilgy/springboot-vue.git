package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageDictVo {

    private Integer pageNum;
    private Integer pageSize;
    private String dictName;
    private String dictType;
    private String status;
    private String startTime;
    private String endTime;
}
