package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * <p>
 * 字典类型表
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_dict_type")
//@ApiModel(value="SysDictType", description="字典类型表")
public class SysDictType extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //@ApiModelProperty(value = "字典主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    //@ApiModelProperty(value = "字典名称")
    @TableField("dict_name")
    private String dictName;

    //@ApiModelProperty(value = "字典类型")
    @TableField("dict_type")
    private String dictType;

    //@ApiModelProperty(value = "状态（0正常 1停用）")
    @TableField("status")
    private String status;

    //@ApiModelProperty(value = "创建者")
    @TableField("create_by")
    private String createBy;


    //@ApiModelProperty(value = "更新者")
    @TableField("update_by")
    private String updateBy;


    //@ApiModelProperty(value = "备注")
    @TableField("remark")
    private String remark;

    @TableField(exist = false)
    private Map<String, Object> params;

}
