package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色-菜单-关联表
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_role_menu")
public class SysRoleMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId("role_id")
    private Integer roleId;


    @TableField("menu_id")
    private Integer menuId;


}
