package com.wusuowei.generator.model.vo;

import lombok.Data;

@Data
public class PageGenTableVo {

    private Integer pageNum;
    private Integer pageSize;
    private String tableName;
    private String tableComment;
    private String beginTime;
    private String endTime;
}
