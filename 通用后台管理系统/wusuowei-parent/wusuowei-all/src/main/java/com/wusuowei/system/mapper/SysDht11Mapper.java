package com.wusuowei.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysDht11;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysDht11Mapper extends BaseMapper<SysDht11> {

}
