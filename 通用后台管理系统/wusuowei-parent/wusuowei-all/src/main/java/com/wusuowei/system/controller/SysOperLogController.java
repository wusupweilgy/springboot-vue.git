package com.wusuowei.system.controller;

import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.system.model.vo.PageOperLogVo;
import com.wusuowei.system.service.ISysOperLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 操作日志记录 前端控制器
 * </p>
 *
 * @author LGY
 */
@Slf4j
@RestController
@RequestMapping("system/log/operLog")
public class SysOperLogController {

    @Autowired
    private ISysOperLogService sysOperLogService;

    @PreAuthorize("hasAuthority('system:operlog:remove')")
    @Log(title = "操作日志", businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping("/{operlogIds}")
    public R deleteBatch(@PathVariable List<String> operlogIds) {
        sysOperLogService.removeByIds(operlogIds);
        return R.ok();
    }

    @PreAuthorize("hasAuthority('system:operlog:remove')")
    @Log(title = "操作日志", businessType = BusinessTypeEnum.CLEAN)
    @DeleteMapping("/del/all")
    public R deleteAll() {
        return R.ok().setData(sysOperLogService.deleteAll());
    }


    @PreAuthorize("hasAnyAuthority('system:operlog:list','system:operlog:query')")
    @GetMapping("/page")
    public R findPage(PageOperLogVo pagequery) {
        return R.ok().setData(sysOperLogService.findPage(pagequery));
    }


}
