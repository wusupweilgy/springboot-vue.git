package com.wusuowei.config.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wusuowei.common.constant.CacheConstants;
import com.wusuowei.common.core.RedisCache;
import com.wusuowei.common.domain.R;
import com.wusuowei.common.utils.RespEnum;
import com.wusuowei.common.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * @description 网关身份验证筛选器
 * @author LGY
 * @date 2023/04/10 21:44
 * @version 1.0.0
 */
@RefreshScope
@Slf4j
//@Component
public class GatewayAuthFilter implements Filter {


    //白名单
    private static List<String> whitelist = null;

    static {
        //加载白名单
        try (
                InputStream resourceAsStream = GatewayAuthFilter.class.getResourceAsStream("/security-whitelist.properties");
        ) {
            Properties properties = new Properties();
            properties.load(resourceAsStream);
            Set<String> strings = properties.stringPropertyNames();
            whitelist = new ArrayList<>(strings);

        } catch (Exception e) {
            log.error("加载/security-whitelist.properties出错:{}", e.getMessage());
            e.printStackTrace();
        }

    }

//    @Autowired
//    TokenStore tokenStore;

    /**
     * 将非法请求返回
     * @return
     */
    private void responseError(HttpServletRequest req, HttpServletResponse resp, RespEnum respEnum, String uid) throws IOException {
        if (uid != null) {
            RedisCache redisCache = SpringContextUtils.getBean(RedisCache.class);
            redisCache.deleteObject(CacheConstants.LOGIN_TOKEN_KEY + uid);
        }
        // 设置响应头
        resp.setContentType("application/json");
        // 设置响应数据
        R responseData = R.error(respEnum.getCode(), respEnum.getMessage());
        byte[] bytes = new byte[0];
        try {
            bytes = new ObjectMapper().writeValueAsBytes(responseData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        // 将响应对象写入响应输出流
//        PrintWriter writer = resp.getWriter();
//        writer.write(JSON.toJSONString(responseData));
//        writer.close();
    }

    /**
     * 获取token
     */
    private String getToken(HttpServletRequest req) {
        String tokenStr = req.getHeader("Authorization");
        if (StringUtils.isBlank(tokenStr)) {
            return null;
        }
        String token = tokenStr.split(" ")[1];
        if (StringUtils.isBlank(token)) {
            return null;
        }
        return token;
    }

    public OAuth2AccessToken getNewToken(String refreshToken) {
        MultiValueMap<String, Object> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.set("grant_type", "refresh_token");
        paramsMap.set("refresh_token", refreshToken);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor("client", "112233"));
        OAuth2AccessToken token = restTemplate.postForObject("http://localhost:8160/auth/oauth/token", paramsMap, OAuth2AccessToken.class);
        return token;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getMethod().equals(RequestMethod.OPTIONS.name())) {
            chain.doFilter(request, resp);
        }
        String requestUrl = req.getRequestURI();
        AntPathMatcher pathMatcher = new AntPathMatcher();
        //白名单放行
        for (String url : whitelist) {
            if (pathMatcher.match(url, requestUrl)) {
                chain.doFilter(request, resp);
                return;
            }
        }

        //检查token是否存在
        String token = getToken(req);
        if (StringUtils.isBlank(token)) {
            responseError(req, resp, RespEnum.UNAUTHENTICATEDERROR, null);
        }
        //判断是否是有效的token
        OAuth2AccessToken oAuth2AccessToken = null;
        String uid = null;
        TokenStore tokenStore = SpringContextUtils.getBean(TokenStore.class);
        try {
            oAuth2AccessToken = tokenStore.readAccessToken(token);
            uid = (String) oAuth2AccessToken.getAdditionalInformation().get("uid");
        } catch (Exception e) {
            log.info("认证令牌无效: {}", token);
            responseError(req, resp, RespEnum.TOKEN_ILLEGAL, uid);
        }

        //判断是否多次登录，多个浏览器的情况
        RedisCache redisCache = SpringContextUtils.getBean(RedisCache.class);
        String accessToken = redisCache.getCacheObject(CacheConstants.ACCESS_TOKEN_KEY + uid);
        if (!token.equals(accessToken)) {
            responseError(req, resp, RespEnum.TOKEN_EXPIRATION, uid);
        }
        boolean expired = oAuth2AccessToken.isExpired();

        //token续期  如果请求中没有refreshToken，直接返回过期，否则续期
        if (expired) {
            String refreshToken = req.getHeader("refresh_token");
            if (StringUtils.isNotBlank(refreshToken)) {
                OAuth2AccessToken newToken = null;
                //生成新token
                try {
                    newToken = getNewToken(refreshToken);
                    redisCache.setCacheObject(CacheConstants.ACCESS_TOKEN_KEY + uid, newToken.getValue(), 3600, TimeUnit.SECONDS);
                } catch (Exception e) {
                    responseError(req, resp, RespEnum.REFRESH_TOKEN_EXPIRATION, uid);
                }
                log.info("token续期成功:" + newToken.getValue());
                resp.setHeader("refreshToken", String.valueOf(newToken.getRefreshToken()));
                resp.setHeader("accessToken", newToken.getValue());
                resp.setHeader("Access-Control-Expose-Headers", "refreshToken");
                resp.setHeader("Access-Control-Expose-Headers", "accessToken");
            } else {
                responseError(req,resp, RespEnum.TOKEN_EXPIRATION, uid);
            }
        }

        chain.doFilter(request, resp);
    }
}
