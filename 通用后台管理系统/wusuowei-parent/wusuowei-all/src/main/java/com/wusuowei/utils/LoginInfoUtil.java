package com.wusuowei.utils;

import com.wusuowei.common.model.dto.ClientMsg;
import com.wusuowei.common.model.po.ComLoginLog;
import com.wusuowei.common.model.po.ComUser;
import com.wusuowei.common.utils.IpUtil;
import com.wusuowei.common.utils.SpringContextUtils;
import com.wusuowei.config.rabbitmq.RabbitmqConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @description 记录登录操作日志工具类
 * @author LGY
 * @date 2023/05/11 12:38
 * @version 1.0.0
 */
public class LoginInfoUtil {

    public static ComLoginLog recordSuccessLoginInfo(ComUser comUser, HttpServletRequest request, String message) {
        RabbitTemplate rabbitTemplate = SpringContextUtils.getBean(RabbitTemplate.class);
        ClientMsg clientMsg = IpUtil.getClientMsg(request);
        ComLoginLog logininfor = new ComLoginLog();
        logininfor.setUserName(comUser.getUsername());
        logininfor.setIpaddr(clientMsg.getIp());
        logininfor.setLoginLocation(clientMsg.getAddress());
        logininfor.setBrowser(clientMsg.getBrowserType());
        logininfor.setOs(clientMsg.getSystemType());
        logininfor.setStatus("0");
        logininfor.setMsg(message); //登录信息
        logininfor.setLoginTime(LocalDateTime.now());

        rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE, RabbitmqConfig.ROUTINGKEY, logininfor);
        return logininfor;
    }

    public static ComLoginLog recordErrorLoginInfo(ComUser comUser, HttpServletRequest request, String message) {
        RabbitTemplate rabbitTemplate = SpringContextUtils.getBean(RabbitTemplate.class);
        ClientMsg clientMsg = IpUtil.getClientMsg(request);
        ComLoginLog logininfor = new ComLoginLog();
        logininfor.setUserName(comUser.getUsername());
        logininfor.setIpaddr(clientMsg.getIp());
        logininfor.setLoginLocation(clientMsg.getAddress());
        logininfor.setBrowser(clientMsg.getBrowserType());
        logininfor.setOs(clientMsg.getSystemType());
        logininfor.setStatus("1");
        logininfor.setMsg(message); //登录信息
        logininfor.setLoginTime(LocalDateTime.now());

        rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE, RabbitmqConfig.ROUTINGKEY, logininfor);
        return logininfor;
    }
}
