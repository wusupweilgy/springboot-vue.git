package com.wusuowei.checkcode.service.impl;

import com.wusuowei.checkcode.service.CheckCodeService;
import com.wusuowei.common.core.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @description redis校验码存储
 * @author LGY
 * @date 2023/04/04 14:28
 * @version 1.0.0
 */
@Service("RedisCheckCodeService")
public class RedisCheckCodeStore implements CheckCodeService.CheckCodeStore {

    @Autowired
    private RedisCache redisCache;

    @Override
    public void set(String key, String value, Integer expire) {
        redisCache.setCacheObject(key, value,expire,TimeUnit.SECONDS);
    }

    @Override
    public String get(String key) {
        return (String) redisCache.getCacheObject(key);
    }

    @Override
    public void remove(String key) {
        redisCache.deleteObject(key);
    }
}
