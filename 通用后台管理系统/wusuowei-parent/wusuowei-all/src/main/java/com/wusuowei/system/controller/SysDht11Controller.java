package com.wusuowei.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wusuowei.common.domain.R;
import com.wusuowei.system.model.po.SysDht11;
import com.wusuowei.system.service.ISysDht11Service;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LGY
 */
@Slf4j
@RestController
@Api(value = "管理接口",tags = "管理接口")
@RequestMapping("system/monitor/dht11")
public class SysDht11Controller {

    @Autowired
    private ISysDht11Service  iSysDht11Service;

    // 新增或者更新
    @PostMapping
    public boolean save(@RequestBody SysDht11 sysDht11) {
        return iSysDht11Service.saveOrUpdate(sysDht11);
    }


    @GetMapping("/page")
    public R findPage() {
            QueryWrapper<SysDht11> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByDesc("create_time");
            queryWrapper.last("limit 0,50");
        List<SysDht11> list = iSysDht11Service.list(queryWrapper);
        List<Double> temp = list.stream().map(SysDht11::getTemp).collect(Collectors.toList());
        List<Double> humi = list.stream().map(SysDht11::getHumi).collect(Collectors.toList());
        List<String> createTime = list.stream().map(item -> {
            return item.getCreateTime().toString().replaceAll("T"," ");
        }).collect(Collectors.toList());
        Collections.reverse(createTime);
        Map<String, Object> map = new HashMap<>();
        map.put("temp",temp);
        map.put("humi",humi);
        map.put("createTime",createTime);
        return R.ok().setData(map);
    }




}
