package com.wusuowei.system.service.impl;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.common.model.po.ComUser;
import com.wusuowei.common.utils.SecurityUserUtil;
import com.wusuowei.common.utils.mybatis.WrapperUtils;
import com.wusuowei.system.mapper.SysFileMapper;
import com.wusuowei.system.model.po.SysFile;
import com.wusuowei.system.service.ISysFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements ISysFileService {

    @Autowired
    private SysFileMapper sysFileMapper;
    @Override
    public List<SysFile> selectSysFileList(SysFile sysFile)
    {
        ComUser securityUser = SecurityUserUtil.getSecurityUser();
        if(!"admin".equals(securityUser.getUsername())){
            sysFile.setCreateBy(securityUser.getId());
        }
        Map map = JSON.parseObject(JSON.toJSONString(sysFile), Map.class);
        QueryWrapper<SysFile> sysFileQueryWrapper = new QueryWrapper<>();
        return sysFileMapper.selectList(WrapperUtils.MapAddEqualToQueryWrapper(sysFileQueryWrapper, map));
    }

}
