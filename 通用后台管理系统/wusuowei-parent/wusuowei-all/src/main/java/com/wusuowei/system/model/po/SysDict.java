package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;


@Data
@TableName("sys_dict")
public class SysDict extends BaseEntity {

    private String name;
    private String value;
    private String type;

}
