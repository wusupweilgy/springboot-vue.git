package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class UserPasswordVo {
    private Integer id;
    private String username;
    private String password;
    private String newPassword;
}
