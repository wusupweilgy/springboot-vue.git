package com.wusuowei.system.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.common.utils.SecurityUserUtil;
import com.wusuowei.common.web.controller.BaseController;
import com.wusuowei.common.web.page.TableDataInfo;
import com.wusuowei.system.mapper.SysFileMapper;
import com.wusuowei.system.model.po.SysFile;
import com.wusuowei.system.model.vo.PageFileVo;
import com.wusuowei.system.service.ISysFileService;
import com.wusuowei.utils.MinioUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lgy
 */
@Slf4j
@PreAuthorize("isAuthenticated()")
@RestController
@RequestMapping("system/file")
public class SysFileController extends BaseController {

    @Autowired
    private SysFileMapper fileMapper;
    @Autowired
    private MinioUtils minioUtils;
    @Autowired
    private ISysFileService sysFileService;


    @PreAuthorize("hasAuthority('system:file:list')")
    @GetMapping("/page")
    public TableDataInfo findPage(SysFile sysFile) {
        startPage();
        List<SysFile> list = sysFileService.selectSysFileList(sysFile);
        return getDataTable(list);
    }

    @PreAuthorize("hasAuthority('system:file:edit')")
    @Log(title = "文件管理",businessType = BusinessTypeEnum.UPDATE)
    @PostMapping("/update")
    public R update(@RequestBody SysFile files) {
        return R.ok().setData(fileMapper.updateById(files));
    }

    @PreAuthorize("hasAuthority('system:file:remove')")
    @Log(title = "文件管理",businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping()
    public R delete(@RequestBody SysFile sysFile) {
        try {
            String fileNanme = SecurityUserUtil.getSecurityUser().getUsername() + sysFile.getUrl().substring(sysFile.getUrl().lastIndexOf("/"));
            minioUtils.deleteObject(sysFile.getFileType(), fileNanme);
        } catch (Exception e) {
            return R.error("文件删除失败");
        }
        fileMapper.deleteById(sysFile.getId());
        return R.ok();
    }

    @PreAuthorize("hasAuthority('system:file:remove')")
    @Log(title = "文件管理",businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping("/del/batch")
    public R deleteBatch(@RequestBody List<SysFile> sysFiles) {
        List<Integer> collect = sysFiles.stream().map(SysFile::getId).collect(Collectors.toList());
        for (SysFile sysFile : sysFiles) {
            try {
                String fileNanme = SecurityUserUtil.getSecurityUser().getUsername() + sysFile.getUrl().substring(sysFile.getUrl().lastIndexOf("/"));
                minioUtils.deleteObject(sysFile.getFileType(), fileNanme);
            } catch (Exception e) {
                log.error("文件删除失败{}",e.getMessage());
            }
        }
        fileMapper.deleteBatchIds(collect);
        return R.ok();
    }


}
