package com.wusuowei.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysUserRole;

/**
 * <p>
 * 用户-角色关联表 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
