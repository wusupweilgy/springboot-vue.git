package com.wusuowei.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wusuowei.auth.model.dto.UserExt;
import com.wusuowei.auth.model.vo.AuthParamsVo;
import com.wusuowei.auth.service.AuthService;
import com.wusuowei.checkcode.service.impl.PicCheckCodeServiceImpl;
import com.wusuowei.common.model.po.ComRole;
import com.wusuowei.common.utils.RespEnum;
import com.wusuowei.common.utils.StringUtils;
import com.wusuowei.config.exception.ServiceException;
import com.wusuowei.system.mapper.SysUserMapper;
import com.wusuowei.system.model.po.SysRole;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.service.ISysRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 用户名密码认证
 * @author LGY
 * @date 2023/04/11 18:58
 * @version 1.0.0
 */
@Service("password_authservice")
public class PasswordServiceImpl implements AuthService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    ISysRoleService roleService;

    @Autowired
    PicCheckCodeServiceImpl picCheckCodeService;

    @Override
    public UserExt execute(AuthParamsVo authParamsVo) {
        //得到验证码
        String checkcode = authParamsVo.getCheckcode();
        String checkcodekey = authParamsVo.getCheckcodekey();
        if(StringUtils.isBlank(checkcodekey) || StringUtils.isBlank(checkcode)){
            throw new ServiceException(RespEnum.CODE_IS_EMPTY);
        }
        //校验验证码,请求验证码服务进行校验
        Boolean result = picCheckCodeService.verify(checkcodekey, checkcode);
        if(result==null || !result){
            throw new ServiceException(RespEnum.CODE_ERROR);
        }

        //账号
        String username = authParamsVo.getUsername();
        //从数据库查询用户信息
        SysUser sysUser = sysUserMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username));
        if (sysUser == null) {
            //账号不存在
            throw new ServiceException(RespEnum.USER_NOT_EXISTS);
        }
        //比对密码
        String passwordDB = sysUser.getPassword();//正确的密码(加密后)
        String passwordInput = authParamsVo.getPassword();//输入的密码
      //  String passwordEncrypt = RSAUtils.decryptBase64(passwordInput);//对加密的密码解密
        boolean matches = passwordEncoder.matches(passwordInput, passwordDB);
        if(!matches){
            throw new RuntimeException("账号或密码错误");
        }
        sysUser.setLoginTime(LocalDateTime.now());
        sysUserMapper.updateById(sysUser);
        List<SysRole> roles = roleService.getRoles(sysUser.getId());
        List<ComRole> comRoles = new ArrayList<>();
        roles.stream().forEach(item->{
            ComRole comRole = new ComRole();
            BeanUtils.copyProperties(item,comRole);
            comRoles.add(comRole);
        });

        UserExt userExt = new UserExt();
        BeanUtils.copyProperties(sysUser,userExt);
        userExt.setRolesExt(comRoles);
        return userExt;
    }

}
