package com.wusuowei.auth.service;


import com.wusuowei.auth.model.dto.UserExt;
import com.wusuowei.auth.model.vo.AuthParamsVo;
import com.wusuowei.system.model.po.SysUser;


/**
 * @description 身份认证接口
 * @author LGY
 * @date 2023/03/31 17:20
 * @version 1.0.0
 */
public interface AuthService {


  /**
   * @description 认证方法
   * @param authParamsVo 身份验证参数
   * @return {@link SysUser }
   * @author LGY
   * @date 2023/03/31 17:20
   */
  UserExt execute(AuthParamsVo authParamsVo);


}
