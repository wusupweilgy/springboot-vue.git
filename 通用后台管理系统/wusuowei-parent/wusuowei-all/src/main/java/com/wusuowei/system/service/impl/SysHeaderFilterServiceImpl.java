package com.wusuowei.system.service.impl;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.common.utils.RedisUtils;
import com.wusuowei.common.utils.ServletUtils;
import com.wusuowei.common.utils.StringUtils;
import com.wusuowei.common.utils.mybatis.WrapperUtils;
import com.wusuowei.generator.common.utils.DateUtils;
import com.wusuowei.system.mapper.SysHeaderFilterMapper;
import com.wusuowei.system.model.po.SysHeaderFilter;
import com.wusuowei.system.model.po.SysLoginLog;
import com.wusuowei.system.model.vo.PageLogininforVo;
import com.wusuowei.system.service.ISysHeaderFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户默认头筛选Service业务层处理
 * 
 * @author zsincere
 * @date 2023-09-16
 */
@Service
public class SysHeaderFilterServiceImpl extends ServiceImpl<SysHeaderFilterMapper, SysHeaderFilter> implements ISysHeaderFilterService
{
    private static final String KEY = "DEFAULT_HEADER_FILTER:";
    @Autowired
    private SysHeaderFilterMapper sysHeaderFilterMapper;

    @Autowired
    private ApplicationContext applicationContext;


    @Autowired
    protected Validator validator;

    @Autowired
    private RedisUtils redisService;

    @Override
    public Map<Object, Object> getDefaultHeader(String userName) {
        // 从缓存中读取
        Map<Object, Object> map = redisService.hmget(KEY + userName);
        if(ObjectUtil.isEmpty(map)){
            return null;
        }
        return map;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean uploadHeaderFilter(String pageName, String userName, List<SysHeaderFilter> list) {
        // 删除原有表头筛选条件
        this.deleteByPageNameAndUserName(pageName,userName);

        if(ObjectUtil.isEmpty(list)){
            redisService.hdel(KEY + userName,pageName);
            return true;
        }

        // 封装实体类
        List<SysHeaderFilter> collect = list.stream().map(item -> {
            item.setPageName(pageName);
            item.setUserName(userName);
            return item;
        }).collect(Collectors.toList());
        // 保存到数据库
        this.saveBatch(collect);
        // 写入缓存
        return redisService.hset(KEY + userName, pageName, collect);
    }

    @Override
    public int deleteByPageNameAndUserName(String pageName, String userName) {
        return sysHeaderFilterMapper.delete(Wrappers.lambdaQuery(SysHeaderFilter.class)
                .eq(SysHeaderFilter::getPageName, pageName)
                .eq(SysHeaderFilter::getUserName, userName)
        );
    }

    /**
     * 查询用户默认头筛选
     * 
     * @param id 用户默认头筛选主键
     * @return 用户默认头筛选
     */
    @Override
    public SysHeaderFilter selectSysHeaderFilterById(Long id)
    {
        return sysHeaderFilterMapper.selectById(id);
    }

    /**
     * 查询用户默认头筛选列表
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 用户默认头筛选
     */
    @Override
    public List<SysHeaderFilter> selectSysHeaderFilterList(SysHeaderFilter sysHeaderFilter)
    {
        Map map = JSON.parseObject(JSON.toJSONString(sysHeaderFilter), Map.class);
        QueryWrapper<SysHeaderFilter> sysHeaderFilterQueryWrapper = new QueryWrapper<>();
        return sysHeaderFilterMapper.selectList(WrapperUtils.MapAddEqualToQueryWrapper(sysHeaderFilterQueryWrapper, map));
    }

    /**
     * 新增用户默认头筛选
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 结果
     */
    @Override
    public int insertSysHeaderFilter(SysHeaderFilter sysHeaderFilter)
    {
        sysHeaderFilter.setCreateTime(DateUtils.getNowDate());
        return sysHeaderFilterMapper.insert(sysHeaderFilter);
    }

    /**
     * 修改用户默认头筛选
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 结果
     */
    @Override
    public int updateSysHeaderFilter(SysHeaderFilter sysHeaderFilter)
    {
        sysHeaderFilter.setUpdateTime(DateUtils.getNowDate());
        return sysHeaderFilterMapper.updateById(sysHeaderFilter);
    }

    /**
     * 批量删除用户默认头筛选
     * 
     * @param ids 需要删除的用户默认头筛选主键
     * @return 结果
     */
    @Override
    public int deleteSysHeaderFilterByIds(Long[] ids)
    {
        return sysHeaderFilterMapper.deleteBatchIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    /**
     * 删除用户默认头筛选信息
     * 
     * @param id 用户默认头筛选主键
     * @return 结果
     */
    @Override
    public int deleteSysHeaderFilterById(Long id)
    {
        return sysHeaderFilterMapper.deleteById(id);
    }
}
