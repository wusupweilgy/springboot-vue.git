package com.wusuowei.system.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysMenu;

import java.util.List;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-17
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<String> getPermission(String uid);

    List<SysMenu> getUserMenus(String id);

    Object findMenus(String name);

    List<SysMenu> getMenusByUid(String uid);

    boolean hasChildByMenuId(Integer menuIds);

    boolean checkMenuExistRole(Integer menuIds);
}
