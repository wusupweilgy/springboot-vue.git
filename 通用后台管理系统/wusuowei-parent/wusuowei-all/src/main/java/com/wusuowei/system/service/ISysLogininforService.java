package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysLoginLog;
import com.wusuowei.system.model.vo.PageLogininforVo;

/**
 * <p>
 * 系统访问记录 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-05-08
 */
public interface ISysLogininforService extends IService<SysLoginLog> {

    int deleteAll();

    Page<SysLoginLog> findPage(PageLogininforVo pagequery);

}
