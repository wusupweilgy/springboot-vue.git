package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysFile;
import com.wusuowei.system.model.po.SysHeaderFilter;
import com.wusuowei.system.model.po.SysLoginLog;
import com.wusuowei.system.model.vo.PageLogininforVo;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 用户默认头筛选Service接口
 * 
 * @author zsincere
 * @date 2023-09-16
 */
public interface ISysHeaderFilterService extends IService<SysHeaderFilter>
{
    /**
     * 查询用户默认头筛选
     * 
     * @param id 用户默认头筛选主键
     * @return 用户默认头筛选
     */
    public SysHeaderFilter selectSysHeaderFilterById(Long id);

    /**
     * 查询用户默认头筛选列表
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 用户默认头筛选集合
     */
    public List<SysHeaderFilter> selectSysHeaderFilterList(SysHeaderFilter sysHeaderFilter);

    /**
     * 新增用户默认头筛选
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 结果
     */
    public int insertSysHeaderFilter(SysHeaderFilter sysHeaderFilter);

    /**
     * 修改用户默认头筛选
     * 
     * @param sysHeaderFilter 用户默认头筛选
     * @return 结果
     */
    public int updateSysHeaderFilter(SysHeaderFilter sysHeaderFilter);

    /**
     * 批量删除用户默认头筛选
     * 
     * @param ids 需要删除的用户默认头筛选主键集合
     * @return 结果
     */
    public int deleteSysHeaderFilterByIds(Long[] ids);

    /**
     * 删除用户默认头筛选信息
     * 
     * @param id 用户默认头筛选主键
     * @return 结果
     */
    public int deleteSysHeaderFilterById(Long id);

    Boolean uploadHeaderFilter(String pageName, String userName, List<SysHeaderFilter> list);

    int deleteByPageNameAndUserName(String pageName, String userName);

    Map<Object, Object> getDefaultHeader(String userName);
}
