package com.wusuowei.system.controller;

import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.system.model.vo.PageLogininforVo;
import com.wusuowei.system.service.ISysLogininforService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * <p>
 * 系统访问记录 前端控制器
 * </p>
 *
 * @author LGY
 */
@Slf4j
@RestController
//@Api(value = "管理接口",tags = "管理接口")
@RequestMapping("system/log/logininfor")
public class SysLoginLogController {

    @Autowired
    private ISysLogininforService sysLogininforService;

    @PreAuthorize("hasAuthority('system:loginlog:remove')")
    @Log(title = "登录日志", businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping("/del/batch")
    public R deleteBatch(@PathVariable List<Integer> ids) {
        sysLogininforService.removeByIds(ids);
        return R.ok();
    }

    @PreAuthorize("hasAuthority('system:loginlog:remove')")
    @Log(title = "登录日志", businessType = BusinessTypeEnum.CLEAN)
    @DeleteMapping("/del/all")
    public R deleteAll() {
        return R.ok().setData(sysLogininforService.deleteAll());
    }

    @PreAuthorize("hasAnyAuthority('system:loginlog:list','system:loginlog:query')")
    @GetMapping("/page")
    public R findPage(PageLogininforVo pagequery) {
        return R.ok().setData(sysLogininforService.findPage(pagequery));
    }


}
