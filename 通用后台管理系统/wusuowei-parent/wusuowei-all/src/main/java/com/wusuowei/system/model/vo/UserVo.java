package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class UserVo {

    private String username;
    private String password;
    private String checkPass;
}
