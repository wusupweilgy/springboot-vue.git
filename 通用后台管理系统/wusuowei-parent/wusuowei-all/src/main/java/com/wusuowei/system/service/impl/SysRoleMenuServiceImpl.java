package com.wusuowei.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysRoleMenuMapper;
import com.wusuowei.system.model.po.SysRoleMenu;
import com.wusuowei.system.service.ISysRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-菜单-关联表 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
