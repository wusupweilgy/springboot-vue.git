package com.wusuowei.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysDict;


public interface SysDictMapper extends BaseMapper<SysDict> {
}
