package com.wusuowei.system.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysMenuMapper;
import com.wusuowei.system.mapper.SysRoleMenuMapper;
import com.wusuowei.system.model.po.SysMenu;
import com.wusuowei.system.model.po.SysRoleMenu;
import com.wusuowei.system.service.ISysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Autowired
    private SysMenuMapper menuMapper;
    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    /**
     * @description 模糊查找菜单
     * @param name 名称
     * @return {@link List }<{@link SysMenu }>
     * @author LGY
     * @date 2023/04/01 10:39
     */
    @Override
    public List<SysMenu> findMenus(String name) {
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("sort_num");
        if (StrUtil.isNotBlank(name)) {
            queryWrapper.like("name", name);
        }
        // 查询所有数据
        List<SysMenu> list = list(queryWrapper);
        // 找出pid为null的一级菜单
        List<SysMenu> collect = list.stream().filter(menu -> menu.getPid() == null)
                .map(menu -> {
                    menu.setChildren(getChildren2(menu, list));
                    return menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getSortNum() == null ? 0 : menu1.getSortNum()) - (menu2.getSortNum() == null ? 0 : menu2.getSortNum());
                })
                .collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<SysMenu> getMenusByUid(String uid) {
        return menuMapper.getMenusByUid(uid);
    }

    /**
     * @description 获取用户菜单
     * @param id
     * @return {@link List }<{@link SysMenu }>
     * @author LGY
     * @date 2023/04/01 10:43
     */
    @Override
    public List<SysMenu> getUserMenus(String id) {
        List<SysMenu> userSysMenus = menuMapper.getUserMenus(id);
        List<SysMenu> collect = userSysMenus.stream().filter(item -> {
            return item.getPid() == null;
        }).map(menu -> {
            menu.setChildren(getChildren(menu, userSysMenus));
            return menu;
        }).collect(Collectors.toList());
        return collect;
    }


    @Override
    public List<String> getPermission(String uid) {

        return menuMapper.getPermission(uid);
    }

    //前端路由渲染用
    private List<SysMenu> getChildren(SysMenu sysMenu, List<SysMenu> userSysMenus) {
        List<SysMenu> collect = userSysMenus.stream()
                .filter(item -> {
                    return item.getPid() != null && item.getPid().equals(sysMenu.getId()) && item.getMenuType()!='F';
                }).map(children -> {
                    children.setPath((sysMenu.getPath()==null? "" : sysMenu.getPath())+(children.getPath()==null?"":children.getPath()));
                    children.setPagePath((sysMenu.getPagePath()==null? "" : sysMenu.getPagePath())+(children.getPagePath()==null?"":children.getPagePath()));
                    children.setChildren(getChildren(children, userSysMenus));
                    return children;
                }).collect(Collectors.toList());
        return collect;
    }

    //前端菜单显示
    private List<SysMenu> getChildren2(SysMenu sysMenu, List<SysMenu> userSysMenus) {
        List<SysMenu> collect = userSysMenus.stream()
                .filter(item -> {
                    return item.getPid() != null && item.getPid().equals(sysMenu.getId());
                }).map(children -> {
                   // children.setPagePath((sysMenu.getPagePath()==null? "" : sysMenu.getPagePath())+(children.getPagePath()==null?"":children.getPagePath()));
                    children.setChildren(getChildren2(children, userSysMenus));
                    return children;
                }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public boolean hasChildByMenuId(Integer menuId) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysMenu::getPid,menuId);
        return menuMapper.exists(wrapper);
    }

    @Override
    public boolean checkMenuExistRole(Integer menuId) {
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRoleMenu::getMenuId,menuId);
        return roleMenuMapper.exists(wrapper);
    }

}
