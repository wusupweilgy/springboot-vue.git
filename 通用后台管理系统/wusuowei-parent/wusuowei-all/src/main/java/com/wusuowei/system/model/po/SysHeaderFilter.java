package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wusuowei.common.utils.mybatis.typehandler.StringArrayToStringTypeHandler;
import com.wusuowei.common.web.page.ConditionDomain;

/**
 * 用户默认头筛选对象 sys_header_filter
 * 
 * @author zsincere
 * @date 2023-09-16
 */
@TableName(autoResultMap = true, excludeProperty={"params", "searchValue", "remark"})
public class SysHeaderFilter  extends ConditionDomain
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @JsonIgnore
    private Long id;

    /** 用户名 */
    @JsonIgnore
    private String userName;

    /** 当前页面的路由名 */
    @JsonIgnore
    private String pageName;

    /** 多选框值 */
    @TableField(value = "checkbox", typeHandler = StringArrayToStringTypeHandler.class)
    private String[] checkbox;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    @Override
    public String[] getCheckbox() {
        return checkbox;
    }

    @Override
    public void setCheckbox(String[] checkbox) {
        this.checkbox = checkbox;
    }
}
