package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 菜单
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_menu")
public class SysMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private String path;

    private String icon;

    private String description;

    private String permission;

    private Integer pid;

    private String pagePath;

    private Integer sortNum;

    private Character menuType;
    //菜单类型（M目录 C菜单 F按钮）
    @TableLogic(value = "0",delval = "1")
    @TableField("is_delete")
    private Boolean isDelete;

    @TableField(exist = false)
    private List<SysMenu> children;



}
