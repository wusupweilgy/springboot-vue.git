package com.wusuowei.generator.controller;

import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.generator.common.core.text.Convert;
import com.wusuowei.generator.model.po.GenTable;
import com.wusuowei.generator.model.vo.PageGenTableVo;
import com.wusuowei.generator.service.IGenTableColumnService;
import com.wusuowei.generator.service.IGenTableService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("generator/gen")
public class GenController
{
    @Autowired
    private IGenTableService genTableService;

    @Autowired
    private IGenTableColumnService genTableColumnService;

    /**
     * 查询数据库列表
     */
    @GetMapping("/db/list")
    public R dataList(PageGenTableVo queryParams)
    {
        Map<String, Object> map = new HashMap<>();
        List<GenTable> list = genTableService.selectDbTableList(queryParams);
        map.put("total",list.size());
        map.put("records",list);
        return R.ok().setData(map);
    }

    /**
     * 查询代码生成列表
     */
    @PreAuthorize("hasAnyAuthority('tool:gen:list','tool:gen:query')")
    @GetMapping("/list")
    public R genList(GenTable genTable)
    {
        List<GenTable> list = genTableService.selectGenTableList(genTable);
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("records",list);
        return R.ok().setData(map);
    }

    /**
     * 导入表结构（保存）
     */
    @PreAuthorize("hasAuthority('tool:gen:import')")
    @Log(title = "代码生成", businessType = BusinessTypeEnum.IMPORT)
    @PostMapping("/importTable")
    public R importTableSave(String tables)
    {
        String[] tableNames = Convert.toStrArray(tables);
        // 查询表信息
        List<GenTable> tableList = genTableService.selectDbTableListByNames(tableNames);
        genTableService.importGenTable(tableList);
        return R.ok();
    }

    /**
     * 预览代码
     */
    @PreAuthorize("hasAuthority('tool:gen:preview')")
    @GetMapping("/preview/{tableId}")
    public R preview(@PathVariable("tableId") Long tableId) throws IOException
    {
        Map<String, String> dataMap = genTableService.previewCode(tableId);
        return R.ok().setData(dataMap);
    }

    /**
     * 删除代码生成
     */
    @PreAuthorize("hasAuthority('tool:gen:remove')")
    @Log(title = "代码生成", businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping("/{tableIds}")
    public R remove(@PathVariable Long[] tableIds)
    {
        genTableService.deleteGenTableByIds(tableIds);
        return R.ok();
    }
    /**
     * 批量生成代码
     */
    @PreAuthorize("hasAuthority('tool:gen:code')")
    @Log(title = "代码生成", businessType = BusinessTypeEnum.GENCODE)
    @GetMapping("/batchGenCode")
    public void batchGenCode(HttpServletResponse response, String tables) throws IOException
    {
        String[] tableNames = Convert.toStrArray(tables);
        byte[] data = genTableService.downloadCode(tableNames);
        genCode(response, data);
    }
    /**
     * 生成代码（下载方式）
     */
    @PreAuthorize("hasAuthority('tool:gen:code')")
    @Log(title = "代码生成", businessType = BusinessTypeEnum.GENCODE)
    @GetMapping("/download/{tableName}")
    public void download(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException
    {
        byte[] data = genTableService.downloadCode(tableName);
        genCode(response, data);
    }

    /**
     * 生成zip文件
     */
    private void genCode(HttpServletResponse response, byte[] data) throws IOException
    {
        response.reset();
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }
}