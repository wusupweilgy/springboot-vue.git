package com.wusuowei.config.exception;


import com.wusuowei.common.utils.RespEnum;
import lombok.Getter;

/**
 * 自定义异常
 */
@Getter
public class ServiceException extends RuntimeException {
    private int code;

    public ServiceException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public ServiceException(RespEnum respEnum){
        super(respEnum.getMessage());
        this.code = respEnum.getCode();
    }

    public ServiceException(String format) {
        super(format);
        this.code = 500;
    }
}