package com.wusuowei.auth.model.dto;

import com.wusuowei.common.model.po.ComRole;

import com.wusuowei.system.model.po.SysUser;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 用户扩展类
 * @author LGY
 * @date 2023/03/31 17:58
 * @version 1.0.0
 */
@Data
public class UserExt extends SysUser {
    //用户权限
    List<String> permissions = new ArrayList<>();

    List<ComRole> rolesExt;
}
