package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageLogininforVo {

    private Integer pageNum;
    private Integer pageSize;
    private String localtion;
    private String userName;
    private String status;
    private String startTime;
    private String endTime;
}
