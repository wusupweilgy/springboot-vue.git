package com.wusuowei.system.service.impl;

import com.wusuowei.system.model.po.SysDht11;
import com.wusuowei.system.mapper.SysDht11Mapper;
import com.wusuowei.system.service.ISysDht11Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysDht11ServiceImpl extends ServiceImpl<SysDht11Mapper, SysDht11> implements ISysDht11Service {

}
