package com.wusuowei.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysOperLog;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

    int deleteAll();
}
