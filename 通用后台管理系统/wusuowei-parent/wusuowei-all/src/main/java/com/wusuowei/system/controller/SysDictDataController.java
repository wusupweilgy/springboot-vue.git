package com.wusuowei.system.controller;

import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.common.utils.SecurityUserUtil;
import com.wusuowei.common.utils.StringUtils;
import com.wusuowei.system.model.po.SysDictData;
import com.wusuowei.system.service.ISysDictDataService;
import com.wusuowei.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据字典信息
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("system/dict/data")
public class SysDictDataController
{
    @Autowired
    private ISysDictDataService dictDataService;

    @Autowired
    private ISysDictTypeService dictTypeService;

    @PreAuthorize("hasAuthority('system:dict:list')")
    @GetMapping("/page")
    public R list(SysDictData dictData)
    {
        List<SysDictData> list = dictDataService.selectDictDataList(dictData);
        Map<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("records",list);
        return R.ok().setData(map);
    }

    /**
     * 查询字典数据详细
     */
    @PreAuthorize("hasAuthority('system:dict:query')")
    @GetMapping(value = "/{dictCode}")
    public R getInfo(@PathVariable Long dictCode)
    {
        return R.ok().setData(dictDataService.selectDictDataById(dictCode));
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    @PreAuthorize("hasAuthority('system:dict:query')")
    @GetMapping(value = "/type/{dictType}")
    public R dictType(@PathVariable String dictType)
    {
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (StringUtils.isEmpty(data))
        {
            data = new ArrayList<SysDictData>();
        }
        return R.ok().setData(data);
    }

    /**
     * 新增字典类型
     */
    @PreAuthorize("hasAuthority('system:dict:add')")
    @Log(title = "字典数据", businessType = BusinessTypeEnum.INSERT)
    @PostMapping
    public R add(@RequestBody SysDictData dict)
    {
        dict.setCreateBy(SecurityUserUtil.getSecurityUser().getUsername());
        return R.ok().setData(dictDataService.insertDictData(dict));
    }

    /**
     * 修改保存字典类型
     */
    @PreAuthorize("hasAuthority('system:dict:edit')")
    @Log(title = "字典数据", businessType = BusinessTypeEnum.UPDATE)
    @PutMapping
    public R edit(@RequestBody SysDictData dict)
    {
        dict.setUpdateBy(SecurityUserUtil.getSecurityUser().getUsername());
        return R.ok().setData(dictDataService.updateDictData(dict));
    }

    /**
     * 删除字典类型
     */
    @PreAuthorize("hasAuthority('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessTypeEnum.DELETE)
    @DeleteMapping("/{dictCodes}")
    public R remove(@PathVariable Long[] dictCodes)
    {
        dictDataService.deleteDictDataByIds(dictCodes);
        return R.ok();
    }
}
