package com.wusuowei.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysOperLog;
import com.wusuowei.system.model.vo.PageOperLogVo;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-05-06
 */
public interface ISysOperLogService extends IService<SysOperLog> {

    int deleteAll();

    Page<SysOperLog> findPage(PageOperLogVo pagequery);

}
