package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户-角色关联表
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_user_role")

public class SysUserRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("user_id")
    private String userId;


    @TableField("role_id")
    private Integer roleId;


}
