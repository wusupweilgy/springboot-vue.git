package com.wusuowei.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysFile;
import com.wusuowei.system.model.vo.PageFileVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

    List<SysFile> findPage(PageFileVo pagequery);
}
