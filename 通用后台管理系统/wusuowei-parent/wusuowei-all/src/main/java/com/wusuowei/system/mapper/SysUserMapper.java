package com.wusuowei.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.model.vo.PageUserVo;
import com.wusuowei.system.model.vo.UserPasswordVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    int updatePassword(UserPasswordVo userPasswordVo);

    List<SysUser> findPage(PageUserVo pagequery);
}
