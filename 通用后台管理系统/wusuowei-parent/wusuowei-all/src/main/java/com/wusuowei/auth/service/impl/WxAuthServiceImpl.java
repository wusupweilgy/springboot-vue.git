package com.wusuowei.auth.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wusuowei.auth.model.dto.UserExt;
import com.wusuowei.auth.model.vo.AuthParamsVo;
import com.wusuowei.auth.service.AuthService;
import com.wusuowei.common.utils.RespEnum;
import com.wusuowei.config.exception.ServiceException;
import com.wusuowei.system.mapper.SysUserMapper;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.service.ISysRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description 微信认证
 * @author LGY
 * @date 2023/04/11 18:58
 * @version 1.0.0
 */
@Service("wechat_open_authservice")
public class WxAuthServiceImpl implements AuthService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    ISysRoleService roleService;

    //微信认证方法
    @Override
    public UserExt execute(AuthParamsVo authParamsVo) {
        //获取账号
        String unionId = authParamsVo.getUnionId();
        SysUser sysUser = sysUserMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUnionid,unionId));
        if(sysUser ==null){
            throw new ServiceException(RespEnum.USER_NOT_EXISTS);
        }
        UserExt userExt = new UserExt();
        sysUser.setRole(roleService.getRoles(String.valueOf(sysUser.getId())));
        BeanUtils.copyProperties(sysUser, userExt);

        return userExt;
    }
}
