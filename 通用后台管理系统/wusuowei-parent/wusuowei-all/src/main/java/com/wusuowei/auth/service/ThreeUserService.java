package com.wusuowei.auth.service;

import com.wusuowei.system.model.po.SysUser;
import me.zhyd.oauth.model.AuthUser;


/**
 * @description 身份认证接口
 * @author LGY
 * @date 2023/03/31 17:20
 * @version 1.0.0
 */
public interface ThreeUserService {


  /**
   * @description 添加第三方应用的用户
   * @param authUser 身份验证用户
   * @return {@link SysUser }
   * @author LGY
   * @date 2023/04/02 23:01
   */
  SysUser addUser(AuthUser authUser);


}
