package com.wusuowei.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysLoginLog;


/**
 * <p>
 * 系统访问记录 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysLogininforMapper extends BaseMapper<SysLoginLog> {

    int deleteAll();
}
