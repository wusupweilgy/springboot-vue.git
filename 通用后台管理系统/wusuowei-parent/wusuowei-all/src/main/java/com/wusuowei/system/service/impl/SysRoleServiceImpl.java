package com.wusuowei.system.service.impl;


import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysRoleMapper;
import com.wusuowei.system.mapper.SysRoleMenuMapper;
import com.wusuowei.system.model.po.SysMenu;
import com.wusuowei.system.model.po.SysRole;
import com.wusuowei.system.model.po.SysRoleMenu;
import com.wusuowei.system.service.ISysMenuService;
import com.wusuowei.system.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private SysRoleMenuMapper roleMenuMapper;

    @Resource
    private ISysMenuService menuService;


    @Override
    public List<SysRole> getRoles(String uid) {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<SysRole>().inSql("id", "SELECT role_id FROM sys_user_role WHERE user_id=" + uid);
        List<SysRole> list = this.list(wrapper);
        return list;
    }

    @Transactional
    @Override
    public void setRoleMenu(Integer roleId, List<Integer> menuIds) {

        // 先删除当前角色id所有的绑定关系
        roleMenuMapper.deleteByRoleId(roleId);

        // 再把前端传过来的菜单id数组绑定到当前的这个角色id上去
        List<Integer> menuIdsCopy = CollUtil.newArrayList(menuIds);
        for (Integer menuId : menuIds) {
            SysMenu sysMenu = menuService.getById(menuId);
            if (sysMenu.getPid() != null && !menuIdsCopy.contains(sysMenu.getPid())) { // 二级菜单 并且传过来的menuId数组里面没有它的父级id
                // 那么我们就得补上这个父级id
                SysRoleMenu roleMenu = new SysRoleMenu();
                roleMenu.setRoleId(roleId);
                roleMenu.setMenuId(sysMenu.getPid());
                roleMenuMapper.insert(roleMenu);
                menuIdsCopy.add(sysMenu.getPid());
            }
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(roleId);
            sysRoleMenu.setMenuId(menuId);
            roleMenuMapper.insert(sysRoleMenu);
        }
    }

    @Override
    public List<Integer> getRoleMenu(Integer roleId) {

        return roleMenuMapper.selectByRoleId(roleId);
    }


}
