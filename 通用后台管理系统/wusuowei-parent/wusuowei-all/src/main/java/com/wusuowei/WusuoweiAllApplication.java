package com.wusuowei;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WusuoweiAllApplication {

    private static final Logger logger = LoggerFactory.getLogger(WusuoweiAllApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WusuoweiAllApplication.class, args);

        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
