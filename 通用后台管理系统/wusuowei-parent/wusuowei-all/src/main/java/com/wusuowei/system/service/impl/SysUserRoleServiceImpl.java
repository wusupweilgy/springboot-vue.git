package com.wusuowei.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wusuowei.system.mapper.SysUserRoleMapper;
import com.wusuowei.system.model.po.SysUserRole;
import com.wusuowei.system.service.ISysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-角色关联表 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
