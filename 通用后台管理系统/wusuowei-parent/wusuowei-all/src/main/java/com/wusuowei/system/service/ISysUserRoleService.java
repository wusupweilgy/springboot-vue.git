package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysUserRole;

/**
 * <p>
 * 用户-角色关联表 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-17
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
