package com.wusuowei.auth.security;


import com.alibaba.fastjson.JSON;
import com.wusuowei.system.model.po.SysUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @description jwt令牌增强器(扩展jwt中存储的内容)
 * @author LGY
 * @date 2023/03/30 17:37
 * @version 1.0.0
 */
@Component
public class JwtTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        Map<String, Object> map = new HashMap<>();
        String userJson = (String) oAuth2Authentication.getName();
        SysUser sysUser = JSON.parseObject(userJson, SysUser.class);
        map.put("uid", sysUser.getId());
        ((DefaultOAuth2AccessToken)oAuth2AccessToken).setAdditionalInformation(map);
        return oAuth2AccessToken;
    }
}
