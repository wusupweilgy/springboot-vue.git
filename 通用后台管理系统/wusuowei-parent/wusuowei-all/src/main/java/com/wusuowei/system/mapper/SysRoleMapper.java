package com.wusuowei.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wusuowei.system.model.po.SysRole;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author LGY
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
