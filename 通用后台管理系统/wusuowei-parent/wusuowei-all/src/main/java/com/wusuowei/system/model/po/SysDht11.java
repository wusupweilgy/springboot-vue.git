package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_dht11")
public class SysDht11 extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "temp",type = IdType.AUTO)
    private Integer id;

    @TableField("temp")
    private Double temp;

    @TableField("humi")
    private Double humi;

}
