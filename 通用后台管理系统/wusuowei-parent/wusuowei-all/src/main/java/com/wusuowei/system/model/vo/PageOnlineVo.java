package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageOnlineVo {

    private Integer pageNum;
    private Integer pageSize;
    private String localtion;
    private String userName;

}
