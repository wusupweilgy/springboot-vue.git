package com.wusuowei.system.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Quarter;
import com.wusuowei.common.constant.CacheConstants;
import com.wusuowei.common.core.RedisCache;


import com.wusuowei.common.domain.R;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.service.ISysMenuService;
import com.wusuowei.system.service.ISysRoleService;
import com.wusuowei.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Slf4j
@PreAuthorize("isAuthenticated()")
@RestController
@RequestMapping("auth/echarts")
public class EchartsController {

    @Autowired
    private ISysUserService userService;
    @Autowired
    private ISysRoleService roleService;
    @Autowired
    private ISysMenuService sysMenuService;
    @Autowired
    private RedisCache redisCache;

    @GetMapping("/members")
    public R members() {
        List<SysUser> list = userService.list();
        int q1 = 0; // 第一季度
        int q2 = 0; // 第二季度
        int q3 = 0; // 第三季度
        int q4 = 0; // 第四季度
        for (SysUser sysUser : list) {
            Date createTime = sysUser.getCreateTime();

            Quarter quarter = DateUtil.quarterEnum(createTime);
            switch (quarter) {
                case Q1:
                    q1 += 1;
                    break;
                case Q2:
                    q2 += 1;
                    break;
                case Q3:
                    q3 += 1;
                    break;
                case Q4:
                    q4 += 1;
                    break;
                default:
                    break;
            }
        }
        return R.ok().setData(CollUtil.newArrayList(q1, q2, q3, q4));
    }

    @GetMapping("/allTotal")
    public R allTotal() {
        long utotal = userService.count();
        long rtotal = roleService.count();
        long mtotal = sysMenuService.count();
        long ototal = redisCache.keys(CacheConstants.ACCESS_TOKEN_KEY+"*").size();
        HashMap<String, Long> map = new HashMap<>();
        map.put("utotal",utotal);
        map.put("rtotal",rtotal);
        map.put("mtotal",mtotal);
        map.put("ototal",ototal);
        return R.ok().setData(map);
    }
}
