package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageFileVo {

    private Integer pageNum;
    private Integer pageSize;
    private String fileName;
    private String fileType;
    private String startTime;
    private String endTime;
}
