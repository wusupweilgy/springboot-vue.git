package com.wusuowei.system.model.vo;

import lombok.Data;

@Data
public class PageRoleVo {
    private Integer pageNum;
    private Integer pageSize;
    private String roleName;
    private String roleKey;
    private String startTime;
    private String endTime;
}
