package com.wusuowei.system.model.po;

import com.baomidou.mybatisplus.annotation.*;
import com.wusuowei.common.web.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author LGY
 */
@Data
@TableName("sys_role")

public class SysRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @TableField("role_key")
    private String roleKey;


    @TableField("name")
    private String name;

    @TableField("description")
    private String description;

    @TableLogic(value = "0",delval = "1")
    @TableField("is_delete")
    private Boolean isDelete;


}
