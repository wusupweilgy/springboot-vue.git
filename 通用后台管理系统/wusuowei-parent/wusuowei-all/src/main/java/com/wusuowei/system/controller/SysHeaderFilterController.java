package com.wusuowei.system.controller;

import com.wusuowei.common.annotation.Log;
import com.wusuowei.common.constant.BusinessTypeEnum;
import com.wusuowei.common.domain.R;
import com.wusuowei.common.utils.PageUtils;
import com.wusuowei.common.utils.SecurityUserUtil;
import com.wusuowei.common.web.controller.BaseController;
import com.wusuowei.common.web.page.TableDataInfo;
import com.wusuowei.system.model.po.SysHeaderFilter;
import com.wusuowei.system.service.ISysHeaderFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 用户默认头筛选Controller
 * 
 * @author zsincere
 * @date 2023-09-16
 */
@RestController
@RequestMapping("/system/headerFilter")
public class SysHeaderFilterController extends BaseController
{
    @Autowired
    private ISysHeaderFilterService sysHeaderFilterService;

    /**
     * 获取用户全部显隐列配置
     * @return
     */
    @GetMapping("/defaultHeader")
    public R getDefaultHeader(){
        Map<Object, Object> resultMap = sysHeaderFilterService.getDefaultHeader(SecurityUserUtil.getSecurityUser().getUsername());
        return R.ok().setData(resultMap);
    }

    /**
     * 保存用户当前页的显隐列配置
     * @param pageName
     * @param list
     * @return
     */
    @PostMapping("/defaultHeader")
    public R uploadDefaultHeader(@RequestParam("pageName") String pageName, @RequestBody(required = false) List<SysHeaderFilter> list){
        sysHeaderFilterService.uploadHeaderFilter(pageName, SecurityUserUtil.getSecurityUser().getUsername(), list);
        return R.ok().setData(sysHeaderFilterService.getDefaultHeader(SecurityUserUtil.getSecurityUser().getUsername()));
    }

    /**
     * 查询用户默认头筛选列表
     */
    @PreAuthorize("hasAuthority('system:headerFilter:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysHeaderFilter sysHeaderFilter)
    {
        startPage();
        List<SysHeaderFilter> list = sysHeaderFilterService.selectSysHeaderFilterList(sysHeaderFilter);
        return getDataTable(list);
    }


    /**
     * 获取用户默认头筛选详细信息
     */
    @PreAuthorize("hasAuthority('system:headerFilter:query')")
    @GetMapping(value = "/{id}")
    public R getInfo(@PathVariable("id") Long id)
    {
        return R.ok().setData(sysHeaderFilterService.selectSysHeaderFilterById(id));
    }

    /**
     * 新增用户默认头筛选
     */
    @PreAuthorize("hasAuthority('system:headerFilter:add')")
    @Log(title = "用户默认头筛选", businessType = BusinessTypeEnum.INSERT)
    @PostMapping
    public R add(@RequestBody SysHeaderFilter sysHeaderFilter)
    {
        return R.ok().setData(sysHeaderFilterService.insertSysHeaderFilter(sysHeaderFilter));
    }

    /**
     * 修改用户默认头筛选
     */
    @PreAuthorize("hasAuthority('system:headerFilter:edit')")
    @Log(title = "用户默认头筛选", businessType = BusinessTypeEnum.UPDATE)
    @PutMapping
    public R edit(@RequestBody SysHeaderFilter sysHeaderFilter)
    {
        return R.ok().setData(sysHeaderFilterService.updateSysHeaderFilter(sysHeaderFilter));
    }

    /**
     * 删除用户默认头筛选
     */
    @PreAuthorize("hasAuthority('system:headerFilter:remove')")
    @Log(title = "用户默认头筛选", businessType = BusinessTypeEnum.DELETE)
	@DeleteMapping("/{ids}")
    public R remove(@PathVariable Long[] ids)
    {
        return R.ok().setData(sysHeaderFilterService.deleteSysHeaderFilterByIds(ids));
    }
}
