package com.wusuowei.auth.service.impl;



import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wusuowei.auth.model.dto.UserExt;
import com.wusuowei.auth.model.vo.AuthParamsVo;
import com.wusuowei.auth.service.AuthService;
import com.wusuowei.common.model.po.ComRole;
import com.wusuowei.common.utils.RespEnum;
import com.wusuowei.config.exception.ServiceException;

import com.wusuowei.system.mapper.SysUserMapper;
import com.wusuowei.system.model.po.SysRole;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.service.ISysRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @description gitee认证
 * @author LGY
 * @date 2023/04/11 18:57
 * @version 1.0.0
 */
@Service("gitee_authservice")
public class GiteeAuthServiceImpl implements AuthService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    GiteeAuthServiceImpl currentProxy;

    @Autowired
    ISysRoleService roleService;

    //微信认证方法
    @Override
    public UserExt execute(AuthParamsVo authParamsVo) {
        //获取账号
        String unionId = authParamsVo.getUnionId();
        SysUser sysUser = sysUserMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUnionid,unionId));
        if(sysUser ==null){
            throw new ServiceException(RespEnum.USER_NOT_EXISTS);
        }

        List<SysRole> roles = roleService.getRoles(sysUser.getId());
        sysUser.setRole(roles);

        List<ComRole> comRoles = new ArrayList<>();
        roles.stream().forEach(item->{
            ComRole comRole = new ComRole();
            BeanUtils.copyProperties(item,comRole);
            comRoles.add(comRole);
        });
        UserExt userExt = new UserExt();
        BeanUtils.copyProperties(sysUser, userExt);
        userExt.setRolesExt(comRoles);
        return userExt;
    }
}
