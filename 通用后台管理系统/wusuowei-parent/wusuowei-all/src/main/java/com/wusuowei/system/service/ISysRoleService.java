package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysRole;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-17
 */
public interface ISysRoleService extends IService<SysRole> {

    List<SysRole> getRoles(String uid);

    void setRoleMenu(Integer roleId, List<Integer> menuIds);

    List<Integer> getRoleMenu(Integer roleId);

}
