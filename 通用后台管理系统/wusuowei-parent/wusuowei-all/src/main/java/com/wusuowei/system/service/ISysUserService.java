package com.wusuowei.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wusuowei.system.model.po.SysUser;
import com.wusuowei.system.model.vo.PageUserVo;
import com.wusuowei.system.model.vo.UserPasswordVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-04-19
 */
public interface ISysUserService extends IService<SysUser> {

    SysUser getUserByPass(String username, String md5Password);

    void updatePassword(UserPasswordVo userPasswordVo);

    List<SysUser> findPage(PageUserVo pagequery);
}
