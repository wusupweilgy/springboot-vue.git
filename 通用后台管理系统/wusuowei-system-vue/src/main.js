import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import directive from '@/directive'
import request from "@/utils/request";
import './assets/gloable.css';
import '@/assets/ruoyi.scss' // ruoyi css
import Loading from "@/utils/loading.js";
import  "./assets/fonts/iconfont.css";
import { getDicts } from "@/api/system/dict/data";
// 自定义表格工具组件
import RightToolbar from "@/components/RightToolbar"
// 字典标签组件
import DictTag from '@/components/DictTag'
// 字典数据组件
import DictData from '@/components/DictData'
// 表头筛选组件
import FilterHeader from '@/components/FilterHeader'
// 全局方法挂载
Vue.prototype.getDicts = getDicts
// 全局组件挂载
Vue.component('DictTag', DictTag)
Vue.component('RightToolbar', RightToolbar)
Vue.component('FilterHeader', FilterHeader)
DictData.install()

Vue.prototype.$Loading = Loading;
Vue.config.productionTip = false
Vue.prototype.request = request
Vue.use(ElementUI,{size:"medium"});
Vue.use(directive)
new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate(){
    Vue.prototype.$bus = this	//安装全局事件总线
  }
}).$mount('#app')
