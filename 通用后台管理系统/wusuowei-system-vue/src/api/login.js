import request from "@/utils/request";
import qs from "qs";
import router from '../router'

//登录(只有这种方式，securityoauth默认可以接收到参数)
export const login = () => {
    return request({
        url: "/auth/login",
        method: "post"
    })
}

// //获取token
export const getToken = usernamejson => {
    localStorage.clear()
    return request({
        method: "post",
        url: "/oauth/token?" + qs.stringify(usernamejson),
    })
}

export const getUser = () => {
    return request({
        method: "get",
        //url: "/auth/user/getUser"
        url: "/product/list"
    })
}
/*获取图片验证码*/
export const getCheckCodeSubmit = () => {
    return request({
        method: "post",
        //url:"http://localhost:4000/checkcode/pic"
        url: "/checkcode/pic"
    });
}

/*获取邮箱验证码*/
export const getEmailCodeSubmit = (emailReceiver) => {
    return request.get("/checkcode/email/sendEmail/" + emailReceiver)
}

/*退出*/
export const logout = () => {
    return request({
        method: "post",
        url: "/auth/logOut"
    }).then(()=>{
        localStorage.clear()
        router.replace("/login")
    })
}

export const register = (data) =>{
    return request({
        method: "post",
        url: "/auth/register",
        data
    })
}
export const getPublicKey =()=>{
    return request({
        method: "post",
        url: "/auth/getPublicKey"
    })
}


