import request from '@/utils/request'
const BASE_URL = 'auth/menu'

// 查询菜单数据详细
export function getMenu(menuId) {
    return request({
        url: `${BASE_URL}/` + menuId,
        method: 'get'
    })
}
// 新增
export function addMenu(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'post',
        data
    })
};
// 修改
export function updateMenu(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'put',
        data: data
    })
}

// 获取菜单
export function getMenus(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'get',
        params:{
            name:data
        }
    })
};

// 请求图标的数据
export function getMenusIcon() {
    return request({
        url: `${BASE_URL}/icons`,
        method: 'get',
    })
};

export function delMenuById(data){
    return request({
        url: `${BASE_URL}/`+data,
        method: 'delete',
    })
}

// 获取全部菜单
export function getAllMenusId() {
    return request({
        url: `${BASE_URL}/ids`,
        method: 'get',
    })
};
