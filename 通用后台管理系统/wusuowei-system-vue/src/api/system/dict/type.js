import request from '@/utils/request'

const BASE_URL = 'system/dict/type'

// 查询字典类型列表
export function listType(query) {
  return request({
    url: `${BASE_URL}/page`,
    method: 'get',
    params: query
  })
}

// 查询字典类型详细
export function getType(dictId) {
  return request({
    url: `${BASE_URL}/` + dictId,
    method: 'get'
  })
}

// 新增字典类型
export function addType(data) {
  return request({
    url: `${BASE_URL}`,
    method: 'post',
    data: data
  })
}

// 修改字典类型
export function updateType(data) {
  return request({
    url: `${BASE_URL}`,
    method: 'put',
    data: data
  })
}

// 删除字典类型
export function delType(dictId) {
  return request({
    url: `${BASE_URL}/` + dictId,
    method: 'delete'
  })
}

// 刷新字典缓存
export function refreshCache() {
  return request({
    url: `${BASE_URL}/refreshCache`,
    method: 'delete'
  })
}

// 获取字典选择框列表
export function optionselect() {
  return request({
    url: `${BASE_URL}/optionselect`,
    method: 'get'
  })
}
