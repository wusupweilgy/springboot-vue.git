import request from '@/utils/request'
import qs from "qs";

const BASE_URL = 'system/log'

// 新增或更新角色
export function saveOperlog(data) {
    return request({
        url: `${BASE_URL}/operLog`,
        method: 'post',
        data
    })
};


// 获取操作日志分页
export function getOperlogsPage(data) {
    return request({
        url: `${BASE_URL}/operLog/page`,
        method: 'get',
        params:data
    })
};

export function delOperlogById(data) {
    return request({
        url: `${BASE_URL}/operLog/` + data,
        method: 'delete',
    })
}

// 批量删除角色
export function delOperlogsByIds(data) {
    return request({
        url: `${BASE_URL}/operLog/del/batch`,
        method: 'delete',
        data
    })
}
// 清空操作日志
export function delAllOperlog() {
    return request({
        url: `${BASE_URL}/operLog/del/all`,
        method: 'delete',
    })
}




