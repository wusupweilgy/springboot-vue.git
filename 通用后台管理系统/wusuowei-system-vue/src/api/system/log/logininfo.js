import request from '@/utils/request'
import qs from "qs";

const BASE_URL = 'system/log'

// 获取操作日志分页
export function getLoginlogsPage(data) {
    return request({
        url: `${BASE_URL}/logininfor/page`,
        method: 'get',
        params:data
    })
};

export function delLoginlogById(data) {
    return request({
        url: `${BASE_URL}/logininfor/` + data,
        method: 'delete',
    })
}

// 批量删除登录日志
export function delLoginlogsByIds(data) {
    return request({
        url: `${BASE_URL}/logininfor/del/batch`,
        method: 'delete',
        data
    })
}

// 清空登录日志
export function delAllLoginlogs() {
    return request({
        url: `${BASE_URL}/logininfor/del/all`,
        method: 'delete',
    })
}



