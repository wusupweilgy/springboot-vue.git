import request from '@/utils/request'
import qs from "qs";
const BASE_URL = 'system/file'

// 更新文件
export function updateFile(data) {
    return request({
        url: `${BASE_URL}/update`,
        method: 'post',
        data
    })
};

// 获取文件分页
export function getFilesPage(data) {
    return request({
        url: `${BASE_URL}/page`,
        method: 'get',
        params:data
    })
};


export function delFileById(data){
    return request({
        url: `${BASE_URL}/`,
        method: 'delete',
        data
    })
}

// 批量删除文件
export function delFilesByIds(data){
    return request({
        url: `${BASE_URL}/del/batch`,
        method: 'delete',
        data
    })
}




