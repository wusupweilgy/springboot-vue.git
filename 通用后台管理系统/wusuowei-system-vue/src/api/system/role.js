import request from '@/utils/request'
import qs from "qs";

const BASE_URL = 'auth/role'

// 查询角色数据详细
export function getRole(roleId) {
    return request({
        url: `${BASE_URL}/` + roleId,
        method: 'get'
    })
}
// 新增
export function addRole(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'post',
        data
    })
};
// 修改
export function updateRole(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'put',
        data: data
    })
}
// 获取全部角色
export function getAllRoles() {
    return request({
        url: `${BASE_URL}`,
        method: 'get',
    })
};

// 获取角色分页
export function getRolesPage(data) {
    return request({
        url: `${BASE_URL}/page`,
        method: 'get',
        params:data
    })
};

export function delRoleById(data) {
    return request({
        url: `${BASE_URL}/` + data,
        method: 'delete',
    })
}

// 批量删除角色
export function delRolesByIds(data) {
    return request({
        url: `${BASE_URL}/del/batch`,
        method: 'delete',
        data
    })
}


// 绑定角色和菜单关系
export function saveRoleAndMenu(roleId, menusIds) {
    return request({
        url: `${BASE_URL}/roleMenu/${roleId}`,
        method: 'post',
        data:menusIds
    })
}

// 根据角色获取菜单
export function getMenusByRoleId(data) {
    return request({
        url: `${BASE_URL}/roleMenu/${data}`,
        method: 'get',
    })
}
