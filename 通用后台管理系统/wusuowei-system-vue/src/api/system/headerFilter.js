import request from '@/utils/request'

// 查询用户默认头筛选列表
export function listHeaderFilter(query) {
  return request({
    url: '/system/headerFilter/list',
    method: 'get',
    params: query
  })
}

// 查询用户默认头筛选详细
export function getHeaderFilter(id) {
  return request({
    url: '/system/headerFilter/' + id,
    method: 'get'
  })
}

// 新增用户默认头筛选
export function addHeaderFilter(data) {
  return request({
    url: '/system/headerFilter',
    method: 'post',
    data: data
  })
}

// 修改用户默认头筛选
export function updateHeaderFilter(data) {
  return request({
    url: '/system/headerFilter',
    method: 'put',
    data: data
  })
}

// 删除用户默认头筛选
export function delHeaderFilter(id) {
  return request({
    url: '/system/headerFilter/' + id,
    method: 'delete'
  })
}

// 获取表头筛选的map
export function getDefaultHeaderFilter() {
  return request({
    url: '/system/headerFilter/defaultHeader',
    method: 'get',
  })
}
// 上传表头筛选的map
export function uploadDefaultHeaderFilter(pageName, data) {
  let newArrVal = JSON.parse(data) //数组是引用类型, 深拷贝一下
  for (const key in newArrVal) {
    // // 删除id属性
    delete newArrVal[key].pageSize;
    delete newArrVal[key].pageNum;
  }
  return request({
    url: '/system/headerFilter/defaultHeader',
    method: 'post',
    params: {
      pageName: pageName,
    },
    data: newArrVal
  })
}
