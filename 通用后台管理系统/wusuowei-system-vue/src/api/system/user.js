import request from '@/utils/request'
import qs from "qs";
const BASE_URL = 'auth/user'

// 新增
export function addUser(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'post',
        data
    })
};
// 修改
export function updateUser(data) {
    return request({
        url: `${BASE_URL}`,
        method: 'put',
        data: data
    })
}

// 修改个人信息
export function updatePersonUser(data) {
    return request({
        url: `${BASE_URL}/updateUser`,
        method: 'put',
        data: data
    })
}
// 查询用户数据详细
export function getUser(userId) {
    return request({
        url: `${BASE_URL}/` + userId,
        method: 'get'
    })
}
// 获取用户分页
export function getUsersPage(data) {
    return request({
        url: `${BASE_URL}/page`,
        method: 'get',
        params:data
    })
};

// 根据用户id删除用户
export function delUserById(data){
    return request({
        url: `${BASE_URL}/`+data,
        method: 'delete',
    })
}

// 根据用户名返回用户
export function getUserByName(data){
    return request({
        url: `${BASE_URL}/username/${data}`,
        method: 'get',
    })
}

// 修改密码
export function editPassword(data){
    return request({
        url: `${BASE_URL}/password/`,
        method: 'post',
        data
    })
}
