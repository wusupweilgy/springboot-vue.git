import request from '@/utils/request'
import axios from 'axios'

let BASE_URL = 'system/monitor'

// 获取操作在线用户分页
export function getOnlinePage(data) {
    return request({
        url: `${BASE_URL}/online/page`,
        method: 'get',
        params:data
    })
};

export function getData(data) {
    return axios({
        url: 'https://api.bemfa.com/api/device/v1/data/1/get/', //状态
        params: {
            uid: data.uid,
            topic: data.zigbeetopic,
            num:1
        },
        header: {
            'content-type': "application/x-www-form-urlencoded"
        },
        method: 'get',
    })

}

export function saveData(data) {
    return request({
        url: `${BASE_URL}/dht11`,
        method: 'post',
        data
    })

}

export function getDBData() {
    return request({
        url: `${BASE_URL}/dht11/page`,
        method: 'get',
    })

}



