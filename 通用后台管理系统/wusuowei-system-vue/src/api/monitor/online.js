import request from '@/utils/request'
import qs from "qs";

let BASE_URL = 'system/monitor'

// 获取操作在线用户分页
export function getOnlinePage(data) {
    return request({
        url: `${BASE_URL}/online/page`,
        method: 'get',
        params:data
    })
};


export function delOnlineById(data) {
    return request({
        url: `${BASE_URL}/online/` + data,
        method: 'delete',
    })
}




