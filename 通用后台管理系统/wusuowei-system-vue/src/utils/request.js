import axios from 'axios'
import router from "@/router";
import ElementUI from "element-ui";
import {MessageBox} from 'element-ui'
const request = axios.create({
    baseURL: `/api`,
    timeout: 30000,
})

// request 拦截器
// 可以自请求发送前对请求做一些处理
// 比如统一加token，对请求参数统一加密
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
    let user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : ""
    let token = localStorage.getItem("token")
    let refresh_token = localStorage.getItem("refresh_token")
    if (token) {
        config.headers['Authorization'] ="bearer " + token;  // 设置请求头
        config.headers['refresh_token'] = refresh_token;  // 设置请求头
    }
    // if (user) {
    //     config.headers['token'] = token;  // 设置请求头
    // }
    return config
}, error => {
    return Promise.reject(error)
});

// response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        if (response.headers === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
            console.log(res)
        }
        let refreshtoken = response.headers.refreshtoken

        if(refreshtoken!==undefined&&refreshtoken!==""){
            console.log(response.headers.accesstoken)
            localStorage.removeItem("token")
            localStorage.removeItem("refresh_token")
            localStorage.setItem("token",response.headers.accesstoken)
            localStorage.setItem("refresh_token",response.headers.refreshtoken)
            console.log("token续期成功")
        }

        if(res.code===401){
            router.replace("/login")
        }
        if(res.code!==undefined && res.code!==200 && res.code >= 100){
            ElementUI.Message({
                message: res.msg,
                type: 'error'
            });
        }
        return res;
    },
    error => {
        let res = JSON.parse(error.request.response)
        if(res.code===401){
            MessageBox.confirm((res.msg===''?'登录状态已过期':res.msg)+'，您可以继续留在该页面，或者重新登录', '系统提示', { confirmButtonText: '重新登录', cancelButtonText: '取消', type: 'warning' }).then(() => {
                localStorage.clear()
                router.replace("/login")
            }).catch(() => {
                ElementUI.Message({
                    message: res.msg,
                    type: 'error'
                });
            });
        }else{
            ElementUI.Message({
                message: res.msg,
                type: 'error'
            });
        }
        return Promise.reject(error)
    }
)


export default request

