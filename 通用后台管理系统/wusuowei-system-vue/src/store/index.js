import Vue from 'vue'
import Vuex from 'vuex'
import router  from "@/router";
import request from "@/utils/request";
import ElementUI from "element-ui";
import dict from './modules/dict'
import tagsView from './modules/tags-view'
import getters from "@/store/getters";
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    hasRoutes:false,
    uploadHeaders:"bearer "+ localStorage.getItem("token"),
    avatarUrl: localStorage.getItem("user") === null ? "" : JSON.parse(localStorage.getItem("user" || "[]")).avatarUrl,
    routes:JSON.parse(localStorage.getItem("menus"))
  },
  mutations: {
    changeAvatarUrl() {
      let user = JSON.parse(localStorage.getItem("user"))
      this.state.avatarUrl = user.avatarUrl
    },
    SET_ROUTES_STATE:(state,hasRoutes)=>{
      state.hasRoutes=hasRoutes
    },
  },
  actions: {
  },
  modules: {
    tagsView,
    dict,
  },
  getters
})
