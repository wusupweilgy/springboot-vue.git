const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/',
  outputDir:'dist',
  assetsDir:'static',

  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8160',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
  }

})
