package com.wusuowei.oauthresource.controller;

import com.wusuowei.oauthresource.utils.R;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('product')")
    public R list(Authentication authentication) {
        List<String> list = new ArrayList<>();
        list.add("huawei");
        list.add("vivo");
        list.add("oppo");
        System.out.println(authentication.getPrincipal());
        return R.ok().setData(list);
    }
}