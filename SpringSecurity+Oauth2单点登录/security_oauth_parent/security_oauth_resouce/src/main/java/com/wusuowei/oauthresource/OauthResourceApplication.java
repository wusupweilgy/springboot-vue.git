package com.wusuowei.oauthresource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@EnableOAuth2Sso//启用单点登录（SSO）功能
@SpringBootApplication
public class OauthResourceApplication {
    private static final Logger logger = LoggerFactory.getLogger(OauthResourceApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(OauthResourceApplication.class, args);
        logger.info("\n\n\n博主博客地址：https://blog.csdn.net/weixin_51603038\n\n\n");
    }

}
